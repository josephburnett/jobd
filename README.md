# jobd

`jobd` is a GitLab CI/CD/workload Runner.

# Getting started

## Installation

### Binary packages

A package is available for each architecture. This is a zip file containing the manager and plugins binaries and will need to be extracted to a location together.

   - macos arm64: https://gitlab.com/jobd/jobd/-/jobs/2722486124/artifacts/download
   - macos amd64: https://gitlab.com/jobd/jobd/-/jobs/2722481967/artifacts/download
   - linux amd64: https://gitlab.com/jobd/jobd/-/jobs/2722485734/artifacts/download

### Docker container

The `manager` and plugins are also packaged as a docker container.

`manager` commands can be substituted for:

```shell
docker run --rm registry.gitlab.com/jobd/jobd/manager:v0.0.0-dev manager <command>
```

## Getting started

The `manager` uses `cuelang` for configuration. Manager can be pointed to a
configuration directory, and each `.cue` file present represents a runner
configuration. It is typical for a manager to have a single runner configuration,
but multiple are supported.

The following is a quick guide to getting up and running with the Docker executor
on Linux:

1. Installation
   ```shell
   # download binary package
   curl -L -o package.zip https://gitlab.com/jobd/jobd/-/jobs/2722485734/artifacts/download

   # unzip package to ~/jobd
   unzip -j package.zip 'linux-amd64/*' -d ~/jobd
   ```

1. Register runner:

   Register a new runner. You'll need a registration token.

   Configuration will be printed to your terminal, this can be copied and pasted
   and used in the next step.

   ```shell
   ~/jobd/manager register https://gitlab.com
   ```

1. Setup

   Create a new config file in `~/.jobd/my-runner.cue`.

   Paste the contents from the registration step, and configure the docker
   executor:

   **`~/.jobd/my-runner.cue`**:
   ```cue
   // token and url for gitlab instance (from register command)
   token: "token"
   url:   "https://gitlab.com"

   // default shell to use
   shell: "sh"

   // number of concurrent jobs this executor can execute
   limit: 1

   // configure docker executor
   executor: {
     type: "/home/<username>/docker-plugin"

     # containers are privileged (insecure)
     privileged: true
   }
   ```

   For now, the `executor`'s `type` field must be an absolute path to the docker-plugin.
   This can be found by running `realpath ~/jobd/docker-plugin`.

1. Run

   ```
   ~/jobd/manager run
   ```

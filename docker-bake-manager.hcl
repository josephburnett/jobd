variable "TAG" {}
variable "CI_REGISTRY_IMAGE" {}

target "manager-linux-amd64" {
  dockerfile = "cmd/manager/Dockerfile"
  args       = { IMAGE = "scratch" }
  platforms  = [ "linux/amd64" ]
  tags       = [ "${CI_REGISTRY_IMAGE}/manager:${TAG}-linux-amd64" ]
}

target "manager-linux-arm64" {
  dockerfile = "cmd/manager/Dockerfile"
  args       = { IMAGE = "scratch" }
  platforms  = [ "linux/arm64" ]
  tags       = [ "${CI_REGISTRY_IMAGE}/manager:${TAG}-linux-arm64" ]
}

target "manager-windows-20H2-amd64" {
  dockerfile = "cmd/manager/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:20H2" }
  tags       = [ "${CI_REGISTRY_IMAGE}/manager:${TAG}-windows-20H2-amd64" ]
  platforms  = [ "windows/amd64" ]
}

target "manager-windows-2004-amd64" {
  dockerfile = "cmd/manager/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:2004" }
  tags       = [ "${CI_REGISTRY_IMAGE}/manager:${TAG}-windows-2004-amd64" ]
  platforms  = [ "windows/amd64" ]
}

target "manager-windows-1909-amd64" {
  dockerfile = "cmd/manager/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:1909" }
  tags       = [ "${CI_REGISTRY_IMAGE}/manager:${TAG}-windows-1909-amd64" ]
  platforms  = [ "windows/amd64" ]
}

target "manager-windows-1809-amd64" {
  dockerfile = "cmd/manager/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:1809" }
  tags       = [ "${CI_REGISTRY_IMAGE}/manager:${TAG}-windows-1809-amd64" ]
  platforms  = [ "windows/amd64" ]
}

group "linux" {
    targets = [
        "manager-linux-amd64",
        "manager-linux-arm64",
    ]
}

group "windows" {
    targets = [
        "manager-windows-20H2-amd64",
        "manager-windows-2004-amd64",
        "manager-windows-1909-amd64",
        "manager-windows-1809-amd64"
    ]
}

group "default" {
  targets = [ "linux" ] # , "windows" ]
}
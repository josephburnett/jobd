package cli

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/signal"
)

type Command interface {
	Command() (fs *flag.FlagSet, usage string)
	Execute(ctx context.Context) error
}

type commands []Command

func (c commands) Usage() {
	for _, cmd := range c {
		fs, _ := cmd.Command()
		fs.Usage()
	}
	os.Exit(1)
}

// Run runs the specified commands based on user input. Command usage is
// displayed if a command returns flag.ErrHelp.
func Run(c ...Command) {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	cmds := commands(c)
	for _, cmd := range cmds {
		fs, usage := cmd.Command()
		fs.Usage = func() {
			fmt.Fprintln(fs.Output(), fs.Name(), usage)
			fs.PrintDefaults()
		}
	}

	if len(os.Args) < 2 {
		cmds.Usage()
	}

	for _, cmd := range cmds {
		fs, _ := cmd.Command()
		if os.Args[1] == fs.Name() {
			_ = fs.Parse(os.Args[2:])
			if err := cmd.Execute(ctx); err != nil {
				if errors.Is(err, flag.ErrHelp) {
					fs.Usage()
					os.Exit(1)
				}

				fmt.Fprintln(fs.Output(), err)
				os.Exit(1)
			}
			return
		}
	}

	cmds.Usage()
}

// CancelBlockWithContext allows a cli to perform a blocking operation (such as
// reading from stdin), but exit in the event of the context cancelation (such
// as from a signal).
func CancelBlockWithContext(ctx context.Context, fn func() error) error {
	errCh := make(chan error, 1)

	go func() {
		errCh <- fn()
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-errCh:
		return err
	}
}

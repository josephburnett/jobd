//go:build windows

package internal

const (
	DependencyBinDir      = DependencySystemPath + DependencySourcePath + "/cmd"
	DependencyGitExecPath = DependencySystemPath + DependencySourcePath + "/mingw64/libexec/git-core"
)
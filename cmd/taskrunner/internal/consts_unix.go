//go:build !windows

package internal

const (
	DependencyBinDir      = DependencySystemPath + DependencySourcePath + "/usr/bin/"
	DependencyGitExecPath = DependencySystemPath + DependencySourcePath + "/usr/lib/git-core/"
)
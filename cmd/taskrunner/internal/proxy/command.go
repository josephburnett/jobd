package proxy

import (
	"context"
	"flag"
	"os"

	"gitlab.com/jobd/jobd/pkg/taskrunner/agent"
)

type proxyCmd struct {
	fs *flag.FlagSet
}

func New() *proxyCmd {
	return &proxyCmd{
		fs: flag.NewFlagSet("proxy", flag.ExitOnError),
	}
}

func (cmd *proxyCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, ""
}

func (cmd *proxyCmd) Execute(ctx context.Context) error {
	agent.Proxy(1, os.Stdout, os.Stdin)

	return nil
}

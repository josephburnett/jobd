package install

import (
	"context"
	"flag"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/jobd/jobd/cmd/taskrunner/internal"
)

type installCmd struct {
	fs *flag.FlagSet
}

func New() *installCmd {
	return &installCmd{
		fs: flag.NewFlagSet("install", flag.ExitOnError),
	}
}

func (cmd *installCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, ""
}

func (cmd *installCmd) Execute(ctx context.Context) error {
	path, err := os.Executable()
	if err != nil {
		return err
	}

	// if taskrunner binary already exists, assume we've already performed the
	// installation.
	_, err = os.Stat(join(internal.DependencySystemPath, path))
	if !os.IsNotExist(err) {
		return nil
	}

	mkdir(filepath.Dir(path), 0777)
	if err := copy(path, 0777); err != nil {
		return err
	}

	err = filepath.Walk(internal.DependencySourcePath, func(path string, info fs.FileInfo, _ error) error {
		switch {
		case info.IsDir():
			return mkdir(path, info.Mode())

		case info.Mode()&os.ModeSymlink != 0:
			return symlink(path)

		default:
			return copy(path, info.Mode())
		}
	})
	if err != nil {
		return err
	}

	gitLfsName := "git-lfs"
	if runtime.GOOS == "windows" {
		gitLfsName += ".exe"
	}

	return copydst(
		join(internal.DependencyGitExecPath, gitLfsName),
		join(internal.DependencyBinDir, gitLfsName),
		0777,
	)
}

func mkdir(path string, perm os.FileMode) error {
	return os.MkdirAll(join(internal.DependencySystemPath, path), perm)
}

func copy(path string, perm os.FileMode) error {
	return copydst(join(internal.DependencySystemPath, path), path, perm)
}

func copydst(dstpath, path string, perm os.FileMode) error {
	src, err := os.Open(path)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.OpenFile(dstpath, os.O_CREATE|os.O_WRONLY, perm)
	if err != nil {
		return err
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)

	return err
}

func symlink(path string) error {
	mkdir(filepath.Dir(path), 0777)

	link, err := os.Readlink(path)
	if err != nil {
		return err
	}

	return os.Symlink(link, join(internal.DependencySystemPath, path))
}

func join(base, path string) string {
	path = strings.TrimPrefix(path, filepath.VolumeName(path))
	return filepath.Join(base, path)
}

package serve

import (
	"context"
	"flag"

	"gitlab.com/jobd/jobd/cmd/taskrunner/internal"
	"gitlab.com/jobd/jobd/pkg/taskrunner"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent"
)

type serveCmd struct {
	fs *flag.FlagSet
}

func New() *serveCmd {
	return &serveCmd{
		fs: flag.NewFlagSet("serve", flag.ExitOnError),
	}
}

func (cmd *serveCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, ""
}

func (cmd *serveCmd) Execute(ctx context.Context) error {
	r, err := taskrunner.NewRunner(taskrunner.WithInternalBinDir(internal.DependencyBinDir, internal.DependencyGitExecPath))
	if err != nil {
		return err
	}

	err = agent.Serve(ctx, 1, r)
	if err != nil {
		return err
	}

	return nil
}

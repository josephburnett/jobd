#!/bin/bash

additions=(
    /usr/bin/git
    /usr/bin/ssh
    /usr/lib/git-core
    /usr/bin/git-lfs
    /usr/lib
    /lib
    /lib64
)

subtractions=(
    # these are the only git commands that still require perl.
    # nothing of which we're likely to use
    /usr/lib/git-core/git-instaweb
    /usr/lib/git-core/git-add--interactive
    /usr/lib/git-core/git-request-pull
)

for d in "${subtractions[@]}"
do
    rm -f "${d}"
done

INSTALL_DIR=/taskrunner/deps

mkdir -p ${INSTALL_DIR}
for d in "${additions[@]}"
do
    for f in $(find ${d} -type f -or -type l)
    do
        # copy the file
        cp --verbose --parents -P "${f}" ${INSTALL_DIR}

        # if also a symlink, also copy over and correc the symlink
        if [ -L "${f}" ]; then
            cp --verbose --parents -P $(readlink -f "${f}") ${INSTALL_DIR}

            ln -s -f ${INSTALL_DIR}$(readlink -f "${f}") ${INSTALL_DIR}${f}
        fi
    done
done

libDirs=$(find ${INSTALL_DIR} -type d -printf '%p:')
for f in $(find ${INSTALL_DIR} -type f)
do
    if ldd "${f}"; then
        if ldd "${f}" | grep -v "statically linked"; then
            patchelf --set-rpath "${libDirs}" "${f}"

            # shrinking the path must remove some library location that is actually needed
            #patchelf --shrink-rpath "${f}"
        fi
    fi

    patchelf --set-interpreter /taskrunner/deps/lib/x86_64-linux-gnu/ld-2.31.so "${f}" || true
done
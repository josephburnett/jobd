package main

import (
	"gitlab.com/jobd/jobd/cmd/internal/cli"
	"gitlab.com/jobd/jobd/cmd/taskrunner/internal/install"
	"gitlab.com/jobd/jobd/cmd/taskrunner/internal/proxy"
	"gitlab.com/jobd/jobd/cmd/taskrunner/internal/serve"
)

func main() {
	cli.Run(
		install.New(),
		proxy.New(),
		serve.New(),
	)
}

package main

import (
	"gitlab.com/jobd/jobd/cmd/internal/cli"
	"gitlab.com/jobd/jobd/cmd/manager/internal/register"
	"gitlab.com/jobd/jobd/cmd/manager/internal/run"
)

func main() {
	cli.Run(
		register.New(),
		run.New(),
	)
}

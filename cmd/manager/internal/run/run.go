package run

import (
	"context"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/go-hclog"
	"golang.org/x/sync/errgroup"

	"gitlab.com/jobd/jobd/internal/manager"
	"gitlab.com/jobd/jobd/pkg/executor/plugin"
)

type runCmd struct {
	fs *flag.FlagSet

	configDir string
	limit     int
}

func New() *runCmd {
	c := &runCmd{}
	c.fs = flag.NewFlagSet("run", flag.ExitOnError)

	configDir, _ := os.UserHomeDir()
	configDir = filepath.Join(configDir, ".jobd")

	c.fs.StringVar(&c.configDir, "config-dir", configDir, "configuration directory")
	c.fs.IntVar(&c.limit, "limit", 0, "concurrent job limit (zero is unlimited)")

	return c
}

func (cmd *runCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, ""
}

func (cmd *runCmd) Execute(ctx context.Context) error {
	log := hclog.Default()
	log.SetLevel(hclog.Trace)

	matches, err := filepath.Glob(filepath.Join(cmd.configDir, "*.cue"))
	if err != nil {
		return fmt.Errorf("finding configs: %w", err)
	}

	limiter := manager.Limiter(cmd.limit, nil)
	opts := []manager.Option{
		manager.WithLogger(log),
		manager.WithLimiter(limiter),
	}

	var managers []*manager.Manager
	for _, conf := range matches {
		name := strings.TrimSuffix(filepath.Base(conf), filepath.Ext(conf))

		cfg, err := manager.LoadConfig(conf)
		if err != nil {
			return fmt.Errorf("loading config %q: %w", name, err)
		}

		ep, err := plugin.Run(cfg.Executor.Type)
		if err != nil {
			return fmt.Errorf("running plugin: %w", err)
		}

		m, err := manager.New(ctx, cfg, ep, opts...)
		if err != nil {
			return fmt.Errorf("creating manager for %q: %w", name, err)
		}
		defer m.Close()

		managers = append(managers, m)
	}

	wg, ctx := errgroup.WithContext(ctx)

	// todo: manager failure will cause all other managers to exit.
	// we need to determine what behaviour we might like here.
	for _, m := range managers {
		man := m
		wg.Go(func() error {
			return man.Start(ctx)
		})
	}

	return wg.Wait()
}

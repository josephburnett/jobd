package register

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jobd/jobd/cmd/internal/cli"
	"gitlab.com/jobd/jobd/pkg/ci/client"
)

type registerCmd struct {
	fs *flag.FlagSet

	desc     string
	paused   bool
	locked   bool
	untagged bool
	tags     string
	timeout  time.Duration
	access   string
	note     string
	token    string
}

func New() *registerCmd {
	hostname, _ := os.Hostname()

	c := &registerCmd{}
	c.fs = flag.NewFlagSet("register", flag.ExitOnError)

	c.fs.StringVar(&c.desc, "description", hostname, "description of runner")
	c.fs.BoolVar(&c.paused, "paused", false, "runner is paused")
	c.fs.BoolVar(&c.locked, "locked", false, "runner is locked")
	c.fs.BoolVar(&c.untagged, "untagged", false, "runner will run untagged jobs")
	c.fs.StringVar(&c.tags, "tags", "", "comma-separated list of tags")
	c.fs.DurationVar(&c.timeout, "timeout", time.Hour, "runner job timeout")
	c.fs.StringVar(&c.access, "access", string(client.NotProtected), "access level: not_protected or ref_protected")
	c.fs.StringVar(&c.note, "note", "", "maintenance note")
	c.fs.StringVar(&c.token, "token", "", "registration token")

	return c
}

func (cmd *registerCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, "<gitlab instance URL>"
}

func (cmd *registerCmd) Execute(ctx context.Context) error {

	if cmd.token == "" {
		err := cli.CancelBlockWithContext(ctx, func() error {
			fmt.Fprint(os.Stderr, "Registration token: ")

			r := bufio.NewReader(os.Stdin)
			token, err := r.ReadString('\n')
			if err != nil {
				return fmt.Errorf("reading token line: %w", err)
			}

			cmd.token = token

			return nil
		})
		if err != nil {
			return err
		}
	}

	cmd.token = strings.TrimSpace(cmd.token)

	c, err := client.New(cmd.token, cmd.fs.Arg(0))
	if err != nil {
		return fmt.Errorf("creating client: %w", err)
	}

	opts := []client.RegistrationOption{
		client.WithRegistrationDescription(cmd.desc),
		client.WithRegistrationMaximumTimeout(int(cmd.timeout.Seconds())),
		client.WithRegistrationAccessLevel(client.AccessLevel(cmd.access)),
		client.WithRegistrationMaintenanceNote(cmd.note),
	}

	if cmd.locked {
		opts = append(opts, client.WithRegistrationLocked())
	}

	if cmd.untagged {
		opts = append(opts, client.WithRegistrationRunUntagged())
	}

	if cmd.tags != "" {
		for _, tag := range strings.Split(cmd.tags, ",") {
			opts = append(opts, client.WithRegistrationTag(tag))
		}
	}

	if err := c.Register(ctx, opts...); err != nil {
		return err
	}

	// output configuration snippet:
	cfg := "url:   " + strconv.Quote(cmd.fs.Arg(0)) + "\n" +
		"token: " + strconv.Quote(c.Token()) + "\n"

	os.Stdout.Write([]byte(cfg))

	return nil
}

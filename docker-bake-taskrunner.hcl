variable "TAG" {}
variable "CI_REGISTRY_IMAGE" {}

target "taskrunner-linux-amd64" {
  dockerfile = "cmd/taskrunner/Dockerfile"
  args       = { IMAGE = "scratch" }
  platforms  = [ "linux/amd64" ]
  tags       = [ "${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-linux-amd64" ]
}

target "taskrunner-linux-arm64" {
  dockerfile = "cmd/taskrunner/Dockerfile"
  args       = { IMAGE = "scratch" }
  platforms  = [ "linux/arm64" ]
  tags       = [ "${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-linux-arm64" ]
}

target "taskrunner-windows-20H2-amd64" {
  dockerfile = "cmd/taskrunner/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:20H2" }
  tags       = [ "${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-windows-20H2-amd64" ]
  platforms  = [ "windows/amd64" ]
}

target "taskrunner-windows-2004-amd64" {
  dockerfile = "cmd/taskrunner/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:2004" }
  tags       = [ "${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-windows-2004-amd64" ]
  platforms  = [ "windows/amd64" ]
}

target "taskrunner-windows-1909-amd64" {
  dockerfile = "cmd/taskrunner/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:1909" }
  tags       = [ "${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-windows-1909-amd64" ]
  platforms  = [ "windows/amd64" ]
}

target "taskrunner-windows-1809-amd64" {
  dockerfile = "cmd/taskrunner/Dockerfile.windows"
  args       = { IMAGE = "mcr.microsoft.com/windows/nanoserver:1809" }
  tags       = [ "${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-windows-1809-amd64" ]
  platforms  = [ "windows/amd64" ]
}

group "linux" {
    targets = [
        "taskrunner-linux-amd64",
        "taskrunner-linux-arm64",
    ]
}

group "windows" {
    targets = [
        "taskrunner-windows-20H2-amd64",
        "taskrunner-windows-2004-amd64",
        "taskrunner-windows-1909-amd64",
        "taskrunner-windows-1809-amd64"
    ]
}

group "default" {
  targets = [ "linux" ] # , "windows" ]
}
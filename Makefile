TAG ?= v0.0.0-dev
OS_ARCHS ?= darwin/amd64 darwin/arm64 freebsd/amd64 freebsd/arm64 linux/amd64 linux/arm64 linux/s390x windows/amd64
LINUX_IMAGE_TAGS ?= linux-amd64 linux-arm64
WINDOWS_VERSIONS = 20H2 2004 1909 1909
WINDOWS_IMAGE_TAGS ?= $(foreach VERSION,$(WINDOWS_VERSIONS),windows-${VERSION}-amd64)
IMAGE_TAGS ?= ${LINUX_IMAGE_TAGS} # ${WINDOWS_IMAGE_TAGS}

CI_REGISTRY_IMAGE ?= docker.io/fiveturns

taskrunner:
	$(MAKE) $(foreach OSARCH,$(OS_ARCHS),.output/$(subst /,-,$(OSARCH))/taskrunner)
	mv .output/windows-amd64/taskrunner .output/windows-amd64/taskrunner.exe

.output/%/taskrunner:
	echo $@
	CGO_ENABLED=0 GOOS=$(subst -, GOARCH=,$(subst /taskrunner,,$(subst .output/,,$@))) go build -a -ldflags '-extldflags "-static"' -o .output/$(subst /taskrunner,,$(subst .output/,,$@))/taskrunner ./cmd/taskrunner

manager:
	$(MAKE) $(foreach OSARCH,$(OS_ARCHS),.output/$(subst /,-,$(OSARCH))/manager)
	mv .output/windows-amd64/manager .output/windows-amd64/manager.exe

.output/%/manager:
	echo $@
	CGO_ENABLED=0 GOOS=$(subst -, GOARCH=,$(subst /manager,,$(subst .output/,,$@))) go build -a -ldflags '-extldflags "-static"' -o .output/$(subst /manager,,$(subst .output/,,$@))/manager ./cmd/manager

plugin:
	$(MAKE) $(foreach OSARCH,$(OS_ARCHS),.output/$(subst /,-,$(OSARCH))/docker-plugin)
	mv .output/windows-amd64/docker-plugin .output/windows-amd64/docker-plugin.exe

.output/%/docker-plugin:
	echo $@
	CGO_ENABLED=0 GOOS=$(subst -, GOARCH=,$(subst /docker-plugin,,$(subst .output/,,$@))) go build -a -ldflags '-extldflags "-static"' -o .output/$(subst /docker-plugin,,$(subst .output/,,$@))/docker-plugin ./plugins/docker

taskrunner-docker: taskrunner
	CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} TAG=${TAG} docker buildx bake --file docker-bake-taskrunner.hcl --set *.output=type=registry
	docker manifest create --amend ${CI_REGISTRY_IMAGE}/taskrunner:${TAG} $(foreach IMAGE_TAG,${IMAGE_TAGS},${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-${IMAGE_TAG})
	# $(foreach VERSION,$(WINDOWS_VERSIONS),docker manifest annotate --os-version $(shell docker manifest inspect mcr.microsoft.com/windows/nanoserver:${VERSION} | jq -r '.manifests[0].platform["os.version"]' ) ${CI_REGISTRY_IMAGE}/taskrunner:${TAG} ${CI_REGISTRY_IMAGE}/taskrunner:${TAG}-windows-${VERSION}-amd64;)
	docker manifest push --purge ${CI_REGISTRY_IMAGE}/taskrunner:${TAG}

manager-docker: manager plugin
	CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} TAG=${TAG} docker buildx bake --file docker-bake-manager.hcl --set *.output=type=registry
	docker manifest create --amend ${CI_REGISTRY_IMAGE}/manager:${TAG} $(foreach IMAGE_TAG,${IMAGE_TAGS},${CI_REGISTRY_IMAGE}/manager:${TAG}-${IMAGE_TAG})
	# $(foreach VERSION,$(WINDOWS_VERSIONS),docker manifest annotate --os-version $(shell docker manifest inspect mcr.microsoft.com/windows/nanoserver:${VERSION} | jq -r '.manifests[0].platform["os.version"]' ) ${CI_REGISTRY_IMAGE}/manager:${TAG} ${CI_REGISTRY_IMAGE}/manager:${TAG}-windows-${VERSION}-amd64;)
	docker manifest push --purge ${CI_REGISTRY_IMAGE}/manager:${TAG}

docker: manager-docker taskrunner-docker
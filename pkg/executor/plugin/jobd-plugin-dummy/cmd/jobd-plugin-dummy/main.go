package main

import (
	"github.com/hashicorp/go-hclog"
	"gitlab.com/jobd/jobd/pkg/executor/plugin"
	dummy "gitlab.com/jobd/jobd/pkg/executor/plugin/jobd-plugin-dummy"
)

func main() {
	plugin.Serve(&dummy.Executor{}, hclog.Default())
}

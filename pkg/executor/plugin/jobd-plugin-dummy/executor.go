package dummy

import (
	"context"
	"io"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor"
	"gitlab.com/jobd/jobd/pkg/executor/clientstore"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

var _ executor.Interface = (*Executor)(nil)

type Executor struct {
	store clientstore.Store[any]
}

func (e *Executor) Init(ctx context.Context, log hclog.Logger, config []byte) error {
	return nil
}

func (e *Executor) Discover(ctx context.Context, cursor uint64, follow bool, fn func(ci.Credentials, uint64) error) error {
	return e.store.Next(ctx, cursor, follow, fn)
}

func (e *Executor) Create(ctx context.Context, meta spec.Spec, w io.Writer, script taskscript.Script) error {
	e.store.Add(meta.JobCredentials(), nil)

	_, err := w.Write([]byte("Created!"))

	return err
}

func (e *Executor) Delete(ctx context.Context, credentials ci.Credentials) error {
	e.store.Delete(credentials)

	return nil
}

func (e *Executor) Attach(ctx context.Context, credentials ci.Credentials, w io.Writer, offset int64) (*executor.Result, error) {
	_, err := w.Write([]byte("Attached!"))

	return &executor.Result{
		Finished: true,
		ExitCode: 0,
	}, err
}

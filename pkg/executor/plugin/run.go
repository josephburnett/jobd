package plugin

import (
	"os/exec"
	"path/filepath"
	"strings"

	hplugin "github.com/hashicorp/go-plugin"

	"gitlab.com/jobd/jobd/pkg/executor"
	"gitlab.com/jobd/jobd/pkg/executor/internal/plugin"
)

const Prefix = "jobd-plugin-"

type ExecutorPlugin struct {
	client *hplugin.Client
	iface  executor.Interface
}

func Run(name string) (*ExecutorPlugin, error) {
	if filepath.Base(name) == name && !strings.HasPrefix(name, Prefix) {
		name = Prefix + name
	}

	r := &ExecutorPlugin{}
	r.client = hplugin.NewClient(&hplugin.ClientConfig{
		HandshakeConfig: plugin.Handshake,
		VersionedPlugins: map[int]hplugin.PluginSet{
			0: {
				"executor": &plugin.GRPCExecutorPlugin{},
			},
		},
		Cmd:              exec.Command(name),
		AllowedProtocols: []hplugin.Protocol{hplugin.ProtocolGRPC},
	})

	if err := r.init(); err != nil {
		r.client.Kill()
		return nil, err
	}

	return r, nil
}

func (r *ExecutorPlugin) init() error {
	rpc, err := r.client.Client()
	if err != nil {
		return err
	}

	raw, err := rpc.Dispense("executor")
	if err != nil {
		return err
	}

	r.iface = raw.(executor.Interface)

	return nil
}

func (r *ExecutorPlugin) Executor() executor.Interface {
	return r.iface
}

func (r *ExecutorPlugin) Kill() {
	r.client.Kill()
}

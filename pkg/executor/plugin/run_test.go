package plugin

import (
	"bytes"
	"context"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"sync"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor"
	dummy "gitlab.com/jobd/jobd/pkg/executor/plugin/jobd-plugin-dummy"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

func TestPlugin(t *testing.T) {
	testExecutor(t, &dummy.Executor{})
}

func TestRunPlugin(t *testing.T) {
	dir := t.TempDir()

	binaryName := "jobd-plugin-dummy"
	if runtime.GOOS == "windows" {
		binaryName += ".exe"
	}
	binaryName = filepath.Join(dir, binaryName)

	target, _ := filepath.Abs("jobd-plugin-dummy/cmd/jobd-plugin-dummy")

	cmd := exec.Command("go", "build", "-o", binaryName, target)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	require.NoError(t, err)

	executor, err := Run(binaryName)
	require.NoError(t, err)
	defer executor.Kill()

	testExecutor(t, executor.Executor())
}

func testExecutor(t *testing.T, e executor.Interface) {
	t.Helper()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := e.Init(ctx, hclog.Default(), []byte(``))
	require.NoError(t, err)

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		err := e.Discover(ctx, 0, true, func(creds ci.Credentials, cursor uint64) error {
			defer cancel()
			defer wg.Done()

			buf := new(bytes.Buffer)

			result, err := e.Attach(ctx, creds, buf, 0)
			require.NoError(t, err)
			require.True(t, result.Finished)
			require.Equal(t, 0, result.ExitCode)
			require.Equal(t, "Attached!", buf.String())

			return err
		})
		require.Error(t, err)
	}()

	buf := new(bytes.Buffer)
	require.NoError(t, e.Create(ctx, spec.Spec{ID: 1}, buf, taskscript.Script{}))
	require.Equal(t, "Created!", buf.String())

	wg.Wait()
}

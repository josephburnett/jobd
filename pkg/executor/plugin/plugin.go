package plugin

import (
	"math"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"google.golang.org/grpc"

	"gitlab.com/jobd/jobd/pkg/executor"
	internal "gitlab.com/jobd/jobd/pkg/executor/internal/plugin"
)

func Serve(impl executor.Interface, logger hclog.Logger) {
	if logger == nil {
		logger = hclog.New(&hclog.LoggerOptions{
			JSONFormat: true,
			Level:      hclog.Trace,
		})
	}

	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: internal.Handshake,
		Logger:          logger,
		VersionedPlugins: map[int]plugin.PluginSet{
			0: {
				"executor": &internal.GRPCExecutorPlugin{
					Logger: logger,
					Impl:   impl,
				},
			},
		},
		GRPCServer: func(opts []grpc.ServerOption) *grpc.Server {
			opts = append(opts, grpc.MaxRecvMsgSize(math.MaxInt32))
			opts = append(opts, grpc.MaxSendMsgSize(math.MaxInt32))
			return plugin.DefaultGRPCServer(opts)
		},
	})
}

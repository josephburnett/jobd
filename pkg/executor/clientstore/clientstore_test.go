package clientstore

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci"
)

func TestStore(t *testing.T) {
	var credentials []ci.Credentials
	for i := 0; i < 10000; i++ {
		credentials = append(credentials, ci.Credentials{
			ID: int64(i),
		})
	}

	var in Store[any]
	for _, creds := range credentials {
		in.Add(creds, nil)
	}

	var wg sync.WaitGroup
	wg.Add(len(credentials) + 2)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		defer wg.Done()

		// iterate over items, but add an item to the list whilst doing so to
		// test that following works.
		err := in.Next(ctx, 0, true, func(c ci.Credentials, cursor uint64) error {
			if cursor == 1 {
				in.Add(ci.Credentials{
					ID: int64(0xffff),
				}, nil)
			}
			wg.Done()

			if cursor == uint64(len(credentials))+1 {
				cancel()
			}

			return nil
		})
		require.ErrorIs(t, err, context.Canceled)
	}()

	wg.Wait()
	cancel()

	for _, creds := range credentials {
		in.Delete(creds)
	}

	require.Equal(t, 1, len(in.List()))
}

func TestStoreCursor(t *testing.T) {
	var in Store[any]
	in.Add(ci.Credentials{ID: int64(1)}, nil)
	in.Add(ci.Credentials{ID: int64(2)}, nil)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var track uint64
	err := in.Next(ctx, track, true, func(c ci.Credentials, cursor uint64) error {
		track = cursor

		if track == 2 {
			cancel()
		}
		return nil
	})
	require.Error(t, err, context.Canceled)

	ctx, cancel = context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel()

	err = in.Next(ctx, track, false, func(c ci.Credentials, cursor uint64) error {
		require.True(t, false, "condition should never be executed")
		return nil
	})
	require.NoError(t, err)
}

package clientstore

import (
	"context"
	"math/rand"
	"sync"

	"golang.org/x/exp/slices"

	"gitlab.com/jobd/jobd/pkg/ci"
)

// Store is a key value store representing a workload, with ci.Credentials for
// the key. It allows subscribers to wait for new items to be added.
type Store[T any] struct {
	mu    sync.Mutex
	items []Item[T]
	index map[ci.Credentials]struct{}
	subs  map[int64]chan Item[T]
	next  uint64
}

type Item[T any] struct {
	ci.Credentials
	cursor uint64
	Value  T
}

func (in *Store[T]) Add(creds ci.Credentials, fn func() (T, error)) error {
	in.mu.Lock()
	defer in.mu.Unlock()

	if in.index == nil {
		in.index = make(map[ci.Credentials]struct{})
	}
	if _, ok := in.index[creds]; ok {
		return nil
	}

	in.next++
	it := Item[T]{
		Credentials: creds,
		cursor:      in.next,
	}

	idx, _ := slices.BinarySearchFunc(in.items, it, func(a Item[T], b Item[T]) int {
		if a.cursor == b.cursor {
			return 0
		}
		if a.cursor < b.cursor {
			return -1
		}
		return 1
	})

	if fn != nil {
		var err error
		it.Value, err = fn()
		if err != nil {
			return err
		}
	}
	in.items = slices.Insert(in.items, idx, it)
	in.index[creds] = struct{}{}

	// publish to subscribers
	for _, sub := range in.subs {
		sub <- it
	}

	return nil
}

func (in *Store[T]) Get(creds ci.Credentials) (item T) {
	in.mu.Lock()
	defer in.mu.Unlock()

	for _, item := range in.items {
		if item.Credentials == creds {
			return item.Value
		}
	}

	return
}

func (in *Store[T]) Delete(creds ci.Credentials) {
	in.mu.Lock()
	defer in.mu.Unlock()

	for idx, item := range in.items {
		if item.Credentials == creds {
			delete(in.index, creds)
			in.items = slices.Delete(in.items, idx, idx+1)
			return
		}
	}
}

func (in *Store[T]) List() []Item[T] {
	in.mu.Lock()
	defer in.mu.Unlock()

	items := make([]Item[T], len(in.items))
	copy(items, in.items)

	return items
}

func (in *Store[T]) Next(ctx context.Context, cursor uint64, follow bool, fn func(ci.Credentials, uint64) error) error {
	itemCh := make(chan Item[T], 64)
	var items []Item[T]

	in.mu.Lock()
	{
		items = in.items
		id := rand.Int63()

		if in.subs == nil {
			in.subs = make(map[int64]chan Item[T])
		}
		in.subs[id] = itemCh
		defer delete(in.subs, id)
	}
	in.mu.Unlock()

	for _, item := range items {
		if cursor >= item.cursor {
			continue
		}

		if err := fn(item.Credentials, item.cursor); err != nil {
			return err
		}
	}

	if !follow {
		return nil
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()

		case item := <-itemCh:
			if err := fn(item.Credentials, item.cursor); err != nil {
				return err
			}
		}
	}
}

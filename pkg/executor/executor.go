package executor

import (
	"context"
	"io"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

type Interface interface {
	// Init initializes the executor, validating the provided json encoded config.
	Init(ctx context.Context, log hclog.Logger, config []byte) error

	// Discover discovers work that has been assigned to the executor, this
	// could be recent work or previous unfinished work from the past. Iteration
	// is controlled with the cursor.
	Discover(ctx context.Context, cursor uint64, follow bool, fn func(ci.Credentials, uint64) error) error

	// Create gives the executor new work, providing both spec from a GitLab
	// instance and generated taskscript defining each task.
	Create(ctx context.Context, meta spec.Spec, w io.Writer, script taskscript.Script) error

	// Delete deletes work.
	Delete(ctx context.Context, credentials ci.Credentials) error

	// Attach attaches to previously created work and streams the work log.
	Attach(ctx context.Context, credentials ci.Credentials, w io.Writer, offset int64) (*Result, error)
}

type Result struct {
	Finished bool
	ExitCode int
}

package plugin

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/hashicorp/go-hclog"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor"
	"gitlab.com/jobd/jobd/pkg/executor/internal/plugin/proto"
)

var _ proto.ExecutorServer = (*executorGRPCServer)(nil)

var (
	empty = &emptypb.Empty{}
)

type executorGRPCServer struct {
	Logger hclog.Logger
	Impl   executor.Interface

	proto.UnimplementedExecutorServer
}

func (s *executorGRPCServer) Init(ctx context.Context, req *proto.InitRequest) (*emptypb.Empty, error) {
	return empty, s.Impl.Init(ctx, s.Logger, req.Config)
}

func (s *executorGRPCServer) Discover(req *proto.DiscoverRequest, srv proto.Executor_DiscoverServer) error {
	return s.Impl.Discover(srv.Context(), req.Cursor, req.Follow, func(credentials ci.Credentials, cursor uint64) error {
		return srv.Send(&proto.DiscoveryResponse{
			Credentials: &proto.Credentials{
				Id:    credentials.ID,
				Token: credentials.Token,
				Url:   credentials.BaseURL,
			},
			Cursor: cursor,
		})
	})
}

func (s *executorGRPCServer) Create(req *proto.CreateRequest, srv proto.Executor_CreateServer) error {
	var meta spec.Spec
	if err := json.Unmarshal(req.Spec, &meta); err != nil {
		return fmt.Errorf("unmarshaling spec: %w", err)
	}

	return s.Impl.Create(srv.Context(), meta, &createWriter{srv}, req.Script)
}

func (s *executorGRPCServer) Delete(ctx context.Context, req *proto.DeleteRequest) (*emptypb.Empty, error) {
	credentials := ci.Credentials{
		ID:      req.Credentials.Id,
		Token:   req.Credentials.Token,
		BaseURL: req.Credentials.Url,
	}

	return empty, s.Impl.Delete(ctx, credentials)
}

func (s *executorGRPCServer) Attach(req *proto.AttachRequest, srv proto.Executor_AttachServer) error {
	credentials := ci.Credentials{
		ID:      req.Credentials.Id,
		Token:   req.Credentials.Token,
		BaseURL: req.Credentials.Url,
	}

	result, err := s.Impl.Attach(srv.Context(), credentials, &attachWriter{srv}, req.Offset)
	if err != nil {
		return err
	}

	return srv.Send(&proto.AttachResponse{
		Finished: result.Finished,
		ExitCode: int32(result.ExitCode),
	})
}

type createWriter struct {
	stream proto.Executor_CreateServer
}

func (w *createWriter) Write(p []byte) (int, error) {
	err := w.stream.Send(&proto.CreateResponse{Data: p})
	if err != nil {
		return 0, err
	}

	return len(p), nil
}

type attachWriter struct {
	stream proto.Executor_AttachServer
}

func (w *attachWriter) Write(p []byte) (int, error) {
	err := w.stream.Send(&proto.AttachResponse{Data: p})
	if err != nil {
		return 0, err
	}

	return len(p), nil
}

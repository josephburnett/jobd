package plugin

import (
	"context"
	"encoding/json"
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor"
	"gitlab.com/jobd/jobd/pkg/executor/internal/plugin/proto"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

var (
	_ executor.Interface = (*executorGRPCClient)(nil)
)

type executorGRPCClient struct {
	client proto.ExecutorClient
}

func (c *executorGRPCClient) Init(ctx context.Context, log hclog.Logger, config []byte) error {
	_, err := c.client.Init(ctx, &proto.InitRequest{
		Config: config,
	})

	return err
}

func (c *executorGRPCClient) Discover(ctx context.Context, cursor uint64, follow bool, fn func(ci.Credentials, uint64) error) (err error) {
	srv, err := c.client.Discover(ctx, &proto.DiscoverRequest{
		Cursor: cursor,
		Follow: follow,
	})
	if err != nil {
		return err
	}

	for {
		resp, err := srv.Recv()
		switch {
		case err == io.EOF:
			return nil

		case err != nil:
			return err
		}

		creds := ci.Credentials{
			ID:      resp.Credentials.Id,
			Token:   resp.Credentials.Token,
			BaseURL: resp.Credentials.Url,
		}

		if err := fn(creds, resp.Cursor); err != nil {
			return err
		}
	}
}

func (c *executorGRPCClient) Create(ctx context.Context, meta spec.Spec, w io.Writer, script taskscript.Script) error {
	metaMarshaled, err := json.Marshal(meta)
	if err != nil {
		return fmt.Errorf("marshaling spec: %w", err)
	}

	srv, err := c.client.Create(ctx, &proto.CreateRequest{
		Spec:   metaMarshaled,
		Script: script,
	})
	if err != nil {
		return err
	}

	for {
		resp, err := srv.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}

		_, err = w.Write(resp.Data)
		if err != nil {
			return err
		}
	}
}

func (c *executorGRPCClient) Delete(ctx context.Context, credentials ci.Credentials) error {
	_, err := c.client.Delete(ctx, &proto.DeleteRequest{
		Credentials: &proto.Credentials{
			Id:    credentials.ID,
			Token: credentials.Token,
			Url:   credentials.BaseURL,
		},
	})

	return err
}

func (c *executorGRPCClient) Attach(ctx context.Context, credentials ci.Credentials, w io.Writer, offset int64) (*executor.Result, error) {
	srv, err := c.client.Attach(ctx, &proto.AttachRequest{
		Credentials: &proto.Credentials{
			Id:    credentials.ID,
			Token: credentials.Token,
			Url:   credentials.BaseURL,
		},
		Offset: offset,
	})
	if err != nil {
		return nil, err
	}

	result := new(executor.Result)
	for {
		resp, err := srv.Recv()
		if err == io.EOF {
			return result, nil
		}
		if err != nil {
			return nil, err
		}

		result.ExitCode = int(resp.ExitCode)
		result.Finished = resp.Finished

		if w != nil {
			_, err := w.Write(resp.Data)
			if err != nil {
				return nil, err
			}
		}
	}
}

package plugin

import (
	"context"

	"github.com/hashicorp/go-hclog"
	plugin "github.com/hashicorp/go-plugin"
	"google.golang.org/grpc"

	"gitlab.com/jobd/jobd/pkg/executor"
	"gitlab.com/jobd/jobd/pkg/executor/internal/plugin/proto"
)

//go:generate protoc -I ./ ./proto/executor.proto --go_out=./
//go:generate protoc -I ./ ./proto/executor.proto --go-grpc_out=./

var (
	_ plugin.Plugin     = (*GRPCExecutorPlugin)(nil)
	_ plugin.GRPCPlugin = (*GRPCExecutorPlugin)(nil)
)

var Handshake = plugin.HandshakeConfig{
	ProtocolVersion:  0,
	MagicCookieKey:   "JOBD_EXECUTOR_PLUGIN",
	MagicCookieValue: "52858c1e-9ddf-4b7c-a05c-a07205198e80",
}

type GRPCExecutorPlugin struct {
	plugin.NetRPCUnsupportedPlugin

	Impl   executor.Interface
	Logger hclog.Logger
}

func (b GRPCExecutorPlugin) GRPCServer(broker *plugin.GRPCBroker, s *grpc.Server) error {
	proto.RegisterExecutorServer(s, &executorGRPCServer{
		Logger: b.Logger,
		Impl:   b.Impl,
	})

	return nil
}

func (b *GRPCExecutorPlugin) GRPCClient(ctx context.Context, broker *plugin.GRPCBroker, c *grpc.ClientConn) (interface{}, error) {
	return &executorGRPCClient{
		client: proto.NewExecutorClient(c),
	}, nil
}

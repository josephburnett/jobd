package shell

import (
	"strconv"
	"strings"
)

type Shell interface {
	Generate([]string) string
	Args() []string
	Extension() string
}

type ShellConfig struct {
	Name string
	Cmd  []string
	Ext  string

	// Echo is the statement used for echoing the command to be executed.
	//
	// __COMMAND__ is replaced with a reference to the command. The reference
	// consists of alphanumerical characters and is itself replaced later to
	// prevent escaping issues.
	//
	// For example, with bash, you might use:
	//    echo __COMMAND__
	//
	// This would then be executed by the shell as:
	//    echo ref123456789
	//
	// For logging, ref123456789 would then be replaced by the command to be
	// executed.
	Echo string

	// ScriptPrefix is script that is added at the start of each script
	ScriptPrefix string

	// ScriptSuffix is script that is added at the end of each script
	ScriptSuffix string

	// LinePrefix is script that is added to the start of each line
	LinePrefix string

	// LineSuffix is script that is added to the end of each line
	LineSuffix string
}

const (
	fileReplacement = "__FILE__"
	echoReplacement = "__COMMAND__"

	commandReferenceMarker = '_'
	commandReferencePrefix = "#jobd#:"
)

func (s ShellConfig) Args() []string {
	return s.Cmd
}

func (s ShellConfig) Extension() string {
	return s.Ext
}

func (s ShellConfig) Generate(cmd []string) string {
	var sb strings.Builder

	sb.WriteString(s.ScriptPrefix)
	for idx, line := range cmd {
		ref := string(commandReferenceMarker) +
			commandReferencePrefix + strconv.Itoa(idx) +
			string(commandReferenceMarker)

		sb.WriteString(strings.ReplaceAll(s.Echo, echoReplacement, ref))
		sb.WriteString(s.LinePrefix)
		sb.WriteString(line)
		sb.WriteString(s.LineSuffix)
	}
	sb.WriteString(s.ScriptSuffix)

	return sb.String()
}

func Args(filename string, args []string) []string {
	rewrite := make([]string, len(args))
	copy(rewrite, args)

	for idx := range args {
		if args[idx] == fileReplacement {
			args[idx] = filename
		}
	}

	return args
}

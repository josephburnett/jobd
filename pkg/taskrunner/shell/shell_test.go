package shell

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShell(t *testing.T) {
	provider := NewShellProvider(ShellConfig{
		Name: "test",
		Cmd:  []string{"sh", fileReplacement},
		Ext:  ".sh",
		Echo: "echo __COMMAND__\n",
	})

	shell := provider("test")

	generated := shell.Generate([]string{
		"echo 'hello world'",
		"echo 'more commands'",
	})

	assert.Equal(t, "echo _#jobd#:0_\necho 'hello world'echo _#jobd#:1_\necho 'more commands'", generated)
	assert.Equal(t, []string([]string{"sh", "filename"}), Args("filename", shell.Args()))
	assert.Equal(t, ".sh", shell.Extension())
}

func TestCommandReplacement(t *testing.T) {
	buf := new(bytes.Buffer)
	w := Writer(buf, []string{
		"echo 'hello world'",
		"echo 'more commands'",
		"echo 'another'",
	})

	input := []string{
		"foobar\n_#jobd#:0_\n",
		"_#jobd#:1_\n",
		"out_put\n",
		"_#jobd#:2_\n",
	}
	for _, line := range input {
		n, err := w.Write([]byte(line))
		assert.NoError(t, err)
		assert.Equal(t, len(line), n)
	}

	assert.Equal(t, "foobar\n[command] echo 'hello world'\n[command] echo 'more commands'\nout_put\n[command] echo 'another'\n", buf.String())
}

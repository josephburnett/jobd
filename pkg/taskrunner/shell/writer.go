package shell

import (
	"bytes"
	"io"
	"strconv"
)

type writer struct {
	w      io.Writer
	script []string
	match  []byte
}

const scriptLinePrefix = "[command] "

func Writer(w io.Writer, script []string) io.WriteCloser {
	for idx := range script {
		script[idx] = scriptLinePrefix + script[idx]
	}

	return &writer{
		w:      w,
		match:  make([]byte, 0, 16),
		script: script,
	}
}

func (w *writer) Write(p []byte) (n int, err error) {
	var last int

	for n < len(p) {
		// if our match is at capacity (maximum token size), reset it and
		// continue looking for the next token.
		if len(w.match) == cap(w.match) {
			w.match = w.match[:0]
		}

		// fast path: if we're not matching any names, skip towards _
		// if none found, we can bail early
		if len(w.match) == 0 {
			off := bytes.IndexByte(p[n:], commandReferenceMarker)
			if off == -1 {
				n += len(p[n:])
				break
			} else {
				w.match = append(w.match, p[n+off])
				n += off + 1
			}
		}

		// all of p consumed, so break
		if n >= len(p) {
			break
		}

		// find end of name
		off := bytes.IndexByte(p[n:], commandReferenceMarker)

		// if not found, continue adding to key match
		if off == -1 {
			w.match = append(w.match, p[n])
			n++
			continue
		}

		// bail early if name would exceed our buffer size
		if off+len(w.match) > cap(w.match) {
			w.match = w.match[:0]
			n++
			continue
		}

		name := append(w.match, p[n:n+off]...)
		if bytes.HasPrefix(name[1:], []byte(commandReferencePrefix)) {
			n += off + 1

			ref, err := strconv.Atoi(string(name[1+len(commandReferencePrefix):]))
			if err == nil && ref < len(w.script) {
				_, err = w.w.Write(p[last : n-len(name)-1])
				if err != nil {
					return n, err
				}

				_, err = w.w.Write([]byte(w.script[ref]))
				if err != nil {
					return n, err
				}

				last = n
			}
		}

		// reset match
		w.match = w.match[:0]
	}

	if len(p[last:n]) > 0 {
		_, err = w.w.Write(p[last:n])
	}

	return n, err
}

func (w *writer) Close() error {
	_, err := w.w.Write(w.match)
	if err != nil {
		return err
	}
	w.match = w.match[:0]

	return nil
}

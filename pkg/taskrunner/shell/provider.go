package shell

func NewShellProvider(configs ...ShellConfig) func(string) Shell {
	builtin := []ShellConfig{
		{
			Name:         "sh",
			Cmd:          []string{"sh", fileReplacement},
			Ext:          ".sh",
			Echo:         "echo __COMMAND__\n",
			ScriptPrefix: "if set -o | grep -q pipefail; then set -o pipefail; fi\nset -o errexit\nset +o noclobber\n",
			LineSuffix:   "\n",
			//LineSuffix:   "\n_runner_exit_code=$?; if [ $_runner_exit_code -ne 0 ]; then exit $_runner_exit_code; fi\n",
		},
		{
			Name:         "bash",
			Cmd:          []string{"bash", fileReplacement},
			Ext:          ".sh",
			Echo:         "echo __COMMAND__\n",
			ScriptPrefix: "if set -o | grep -q pipefail; then set -o pipefail; fi\nset -o errexit\nset +o noclobber\n",
			LineSuffix:   "\n",
			//LineSuffix:   "\n_runner_exit_code=$?; if [ $_runner_exit_code -ne 0 ]; then exit $_runner_exit_code; fi\n",
		},
		{
			Name:         "cmd",
			Cmd:          []string{"cmd", fileReplacement},
			Ext:          ".bat",
			Echo:         "echo __COMMAND__\n",
			ScriptPrefix: "@echo off\r\nsetlocal enableextensions\r\nsetlocal enableDelayedExpansion\r\nset nl=^\r\n\r\n\r\n",
			LineSuffix:   "\r\n",
		},
		{
			Name:       "pwsh",
			Cmd:        []string{"pwsh", "-NoLogo", "-NonInteractive", "-ExecutionPolicy", "Bypass", fileReplacement},
			Ext:        ".ps1",
			Echo:       "echo __COMMAND__\n",
			LineSuffix: "\nif(!$?) { Exit &{if($LASTEXITCODE) {$LASTEXITCODE} else {1}} }\n",
		},
	}

	return shellProvider(append(builtin, configs...)...)
}

func shellProvider(configs ...ShellConfig) func(string) Shell {
	r := map[string]ShellConfig{}
	for _, config := range configs {
		r[config.Name] = config
	}

	return func(name string) Shell {
		config, ok := r[name]
		if !ok {
			return r["sh"]
		}
		return config
	}
}

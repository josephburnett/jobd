package agent

import (
	"bytes"
	"context"
	"io"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

type runner struct {
	t *testing.T
}

func (r *runner) Run(_ context.Context, script taskscript.Script, w io.WriteCloser) error {
	require.Equal(r.t, []byte("foobar"), []byte(script))

	w.Write([]byte("barfoo"))
	w.Close()

	return nil
}

func (r *runner) Status() internal.Status {
	return internal.Status{
		Finished: true,
		ExitCode: 6,
	}
}

func TestProtocol(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		err := Serve(ctx, 1, &runner{t})
		if err != nil {
			cancel()
		}
	}()

	client, err := New(ci.Credentials{}, func(stdin io.Reader, stdout io.Writer) (func(context.Context) error, error) {
		return func(ctx context.Context) error {
			Proxy(1, stdout, stdin)

			return nil
		}, nil
	})
	require.NoError(t, err)

	err = client.Run(ctx, taskscript.Script([]byte("foobar")))
	require.NoError(t, err)

	status, err := client.Status(ctx)
	require.NoError(t, err)
	require.True(t, status.Finished)
	require.Equal(t, 6, status.ExitCode)

	buf := new(bytes.Buffer)
	require.NoError(t, client.Follow(ctx, 0, buf))
	require.Equal(t, []byte("barfoo"), buf.Bytes())
}

package internal

//go:generate protoc -I ./ ./proto/taskrunner.proto --go_out=./
//go:generate protoc -I ./ ./proto/taskrunner.proto --go-grpc_out=./

import (
	"context"
	"io"

	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

type Runner interface {
	Run(context.Context, taskscript.Script, io.WriteCloser) error
	Status() Status
}

type Agent interface {
	Run(ctx context.Context, script taskscript.Script) error
	Follow(ctx context.Context, offset uint64, w io.Writer) error
	Status(ctx context.Context) (status Status, err error)
	Shutdown(ctx context.Context)
}

type Status struct {
	Finished bool
	ExitCode int
	Data     map[string][]string
}

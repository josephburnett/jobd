package internal

import (
	"context"
	"io"

	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal/proto"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

var _ Agent = (*Client)(nil)

type Client struct {
	Client proto.TaskrunnerClient
}

func (c *Client) Run(ctx context.Context, script taskscript.Script) error {
	_, err := c.Client.Run(ctx, &proto.RunRequest{
		Script: script,
	})

	return err
}

func (c *Client) Follow(ctx context.Context, offset uint64, w io.Writer) error {
	follow, err := c.Client.Follow(ctx, &proto.FollowRequest{Offset: offset})
	if err != nil {
		return err
	}

	resp := new(proto.FollowResponse)
	for {
		err := follow.RecvMsg(resp)
		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		}

		_, err = w.Write(resp.Data)
		if err != nil {
			return err
		}
	}
}

func (c *Client) Status(ctx context.Context) (status Status, err error) {
	resp, err := c.Client.Status(ctx, &proto.StatusRequest{})
	if err != nil {
		return Status{}, err
	}

	data := make(map[string][]string)
	for key, value := range resp.Data {
		if value == nil {
			continue
		}
		data[key] = value.Value
	}

	return Status{
		Finished: resp.Finished,
		ExitCode: int(resp.ExitCode),
		Data:     data,
	}, nil
}

func (c *Client) Shutdown(ctx context.Context) {
	c.Client.Shutdown(ctx, nil)
}

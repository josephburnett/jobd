package internal

import (
	"context"

	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal/proto"
)

var _ proto.TaskrunnerServer = (*Server)(nil)

type Server struct {
	Impl Agent

	proto.UnimplementedTaskrunnerServer
}

func (s *Server) Run(ctx context.Context, req *proto.RunRequest) (*proto.RunResponse, error) {
	err := s.Impl.Run(ctx, req.Script)
	if err != nil {
		return nil, err
	}

	return &proto.RunResponse{}, nil
}

func (s *Server) Follow(req *proto.FollowRequest, stream proto.Taskrunner_FollowServer) error {
	return s.Impl.Follow(stream.Context(), req.Offset, &writer{stream})
}

func (s *Server) Status(ctx context.Context, _ *proto.StatusRequest) (*proto.StatusResponse, error) {
	status, err := s.Impl.Status(ctx)

	data := make(map[string]*proto.StatusData)
	for key, value := range status.Data {
		data[key] = &proto.StatusData{
			Value: value,
		}
	}

	return &proto.StatusResponse{
		Finished: status.Finished,
		ExitCode: int32(status.ExitCode),
		Data:     data,
	}, err
}

type writer struct {
	stream proto.Taskrunner_FollowServer
}

func (w *writer) Write(p []byte) (int, error) {
	err := w.stream.Send(&proto.FollowResponse{Data: p})
	if err != nil {
		return 0, err
	}

	return len(p), nil
}

func (s *Server) Shutdown(ctx context.Context, _ *proto.ShutdownRequest) (*proto.ShutdownResponse, error) {
	s.Impl.Shutdown(ctx)

	return nil, nil
}

package nc

import (
	"context"
	"fmt"
	"io"
	"net"
	"time"
)

// Dialer dials and provides a io.Reader and io.Writer to dialerFn and returns
// a connected net.Conn implementation.
//
// It can be used to connect code expecting a net.Conn with code expecting an
// io.Reader and io.Writer.
func Dialer(ctx context.Context, dialerFn func(r io.Reader, w io.Writer) (func(context.Context) error, error)) (net.Conn, error) {
	connReader, w := io.Pipe()
	r, connWriter := io.Pipe()

	dial, err := dialerFn(r, w)
	if err != nil {
		return nil, err
	}

	go func() {
		err := dial(ctx)
		if err != nil {
			err = fmt.Errorf("taskrunner client dialer: %w", err)
			r.CloseWithError(err)
			w.CloseWithError(err)
		}
	}()

	return &rwConn{connWriter, connReader}, nil
}

// Proxy connects the read and writer with the dialed connection.
func Proxy(w io.Writer, r io.Reader, network, address string) error {
	conn, err := net.Dial(network, address)
	if err != nil {
		return fmt.Errorf("proxy dialing: %w", err)
	}

	errChan := make(chan error)
	go func() {
		_, err := io.Copy(conn, r)
		if err != nil {
			err = fmt.Errorf("proxy r to conn: %w", err)
		}
		errChan <- err
	}()

	go func() {
		_, err := io.Copy(w, conn)
		if err != nil {
			err = fmt.Errorf("proxy w to conn: %w", err)
		}
		errChan <- err
	}()

	err1 := <-errChan
	err2 := conn.Close()
	err3 := <-errChan

	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return err3
}

type rwConn struct {
	io.WriteCloser
	io.Reader
}

func (conn *rwConn) Close() error {
	conn.WriteCloser.Close()

	return nil
}

func (*rwConn) LocalAddr() net.Addr                { return addr{} }
func (*rwConn) RemoteAddr() net.Addr               { return addr{} }
func (*rwConn) SetDeadline(t time.Time) error      { return fmt.Errorf("unsupported") }
func (*rwConn) SetReadDeadline(t time.Time) error  { return fmt.Errorf("unsupported") }
func (*rwConn) SetWriteDeadline(t time.Time) error { return fmt.Errorf("unsupported") }

type addr struct{}

func (addr) Network() string { return "nc.Conn" }
func (addr) String() string  { return "nc.Conn" }

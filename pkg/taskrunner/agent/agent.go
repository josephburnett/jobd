package agent

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net"
	"os"
	"path/filepath"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal/follow"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal/nc"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent/internal/proto"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

type (
	Runner = internal.Runner
	Status = internal.Status

	Dialer func(stdin io.Reader, stdout io.Writer) (func(context.Context) error, error)
)

type Client interface {
	internal.Agent
	ci.JobCredentials
	io.Closer
}

type client struct {
	internal.Agent
	ci.JobCredentials
	closer func() error
}

func (c *client) Close() error {
	return c.closer()
}

func New(creds ci.JobCredentials, dialer Dialer) (Client, error) {
	conn, err := grpc.Dial("stdinstdout", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(func(c context.Context, s string) (net.Conn, error) {
		return nc.Dialer(c, dialer)
	}))

	if err != nil {
		return nil, fmt.Errorf("dialing: %w", err)
	}

	return &client{
		Agent: &internal.Client{
			Client: proto.NewTaskrunnerClient(conn),
		},
		JobCredentials: creds,
		closer:         conn.Close,
	}, nil
}

type server struct {
	runner      Runner
	logFile     *os.File
	logFollower *follow.ConcurrentReadWriter

	stop func()
}

func Serve(ctx context.Context, id int64, runner Runner) error {
	socketPath := getSocketPath(id)

	if err := os.MkdirAll(filepath.Dir(socketPath), 0777); err != nil {
		return err
	}

	if err := os.RemoveAll(socketPath); err != nil {
		return err
	}

	listener, err := net.Listen("unix", socketPath)
	if err != nil {
		return err
	}

	f, err := ioutil.TempFile("", "")
	if err != nil {
		return err
	}

	srv := grpc.NewServer()
	proto.RegisterTaskrunnerServer(srv, &internal.Server{Impl: &server{
		runner:      runner,
		logFile:     f,
		logFollower: follow.NewConcurrentReadWriter(f),
		stop:        srv.Stop,
	}})

	go func() {
		<-ctx.Done()
		srv.Stop()
	}()

	defer os.Remove(socketPath)

	return srv.Serve(listener)
}

func (s *server) Run(ctx context.Context, script taskscript.Script) error {
	return s.runner.Run(context.Background(), script, s.logFollower)
}

func (s *server) Follow(ctx context.Context, offset uint64, w io.Writer) error {
	rc := s.logFollower.Reader()
	if rc == nil {
		return fmt.Errorf("log is closed")
	}
	defer rc.Close()

	_, err := io.Copy(w, io.NewSectionReader(rc, int64(offset), math.MaxInt64))
	if err != nil {
		return fmt.Errorf("follow log: %w", err)
	}

	return nil
}

func (s *server) Status(ctx context.Context) (status Status, err error) {
	status = s.runner.Status()

	return status, err
}

func (s *server) Shutdown(ctx context.Context) {
	s.stop()
}

func Proxy(id int64, w io.Writer, r io.Reader) {
	nc.Proxy(w, r, "unix", getSocketPath(id))
}

func getSocketPath(id int64) string {
	return filepath.Join(filepath.Join(os.TempDir(), "jobd"), strconv.FormatInt(id, 10))
}

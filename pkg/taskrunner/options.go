package taskrunner

import (
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
	v1 "gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1"
)

type runnerOptions struct {
	TaskFactory         []func() taskscript.Task
	InternalBinDir      string
	InternalGitExecPath string
}

// RunnerOption is an option used when registering or updating a runner details.
type RunnerOption func(*runnerOptions) error

func DefaultTaskFactory() []func() taskscript.Task {
	return []func() taskscript.Task{
		func() taskscript.Task { return &v1.ArtifactsArchiveTask{} },
		func() taskscript.Task { return &v1.ArtifactsExtractTask{} },
		func() taskscript.Task { return &v1.CacheArchiveTask{} },
		func() taskscript.Task { return &v1.CacheExtractTask{} },
		func() taskscript.Task { return &v1.FetchSourcesTask{} },
		func() taskscript.Task { return &v1.InfoTask{} },
		func() taskscript.Task { return &v1.SetupTask{} },
		func() taskscript.Task { return &v1.StepTask{} },
	}
}

func newRunnerOptions(opts []RunnerOption) (runnerOptions, error) {
	var options runnerOptions

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if len(options.TaskFactory) == 0 {
		options.TaskFactory = DefaultTaskFactory()
	}

	return options, nil
}

// WithTaskFactory adds supported tasks and their factory methods.
func WithTaskFactory(factory ...func() taskscript.Task) RunnerOption {
	return func(o *runnerOptions) error {
		o.TaskFactory = append(o.TaskFactory, factory...)
		return nil
	}
}

func WithInternalBinDir(binDir, gitExecPath string) RunnerOption {
	return func(o *runnerOptions) error {
		o.InternalBinDir = binDir
		o.InternalGitExecPath = gitExecPath
		return nil
	}
}

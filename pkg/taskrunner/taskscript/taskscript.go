/*
Package taskscript contains the routines for marshaling and unmarshaling Tasks
as well as executing a script.

A taskscript script is a collection of global metadata and individual
tasks with their associated options and metadata.

Tasks are executed in order, but the execution flow is controlled via the Task's
When and OnFailure configurations.

Each Task has a return value which updates an exit code for the script.

The OnFailure setting controls when the exit code is set.

- OnFailure=break sets the exit code.
- OnFailure=continue does not set the exit code.
- OnFailure=return sets the exit code and immediately terminates the script.

The exit code, in combination with a Task's When value, determines which
tasks are executed.

- When=always will always execute, regardless of the exit code.
- When=on_success will only execute when the exit code is a success.
- When=on_failure will only execute when the exit code is a failure.

Example 1:

- Task 1, When: Always, OnFailure: Continue
- Task 2, When: Success, OnFailure: Return

If Task 1 succeeds, Task 2 is executed.
If Task 1 fails, Task 2 is executed.

Example 2:

- Task 1, When: Always, OnFailure: Return
- Task 2, When: Success, OnFailure: Return

If Task 1 succeeds, Task 2 is executed.
If Task 1 fails the script is terminated.

Example 3:

- Task 1, When: Always, OnFailure: Break
- Task 2, When: Failure, OnFailure: Return

If Task 1 succeeds, Task 2 is not executed.
If Task 1 fails, Task 2 is executed.

Example 4:

- Task 1, When: Always, OnFailure: Continue
- Task 2, When: Failure, OnFailure: Continue
- Task 3, When: Failure, OnFailure: Continue
- Task 4, When: Success, OnFailure: Return

In this configuration, it is impossible for Task 2 and Task 3 to be executed.
*/
package taskscript

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
)

type Script []byte

type Program struct {
	WorkingDir string            `json:"working_dir"`
	CacheDir   string            `json:"cache_dir"`
	Timeout    time.Duration     `json:"timeout"`
	ID         int64             `json:"id"`
	Token      string            `json:"token"`
	BaseURL    string            `json:"base_url"`
	Env        map[string]string `json:"env"`
	Files      map[string]string `json:"files"`
	Tasks      []Item            `json:"tasks"`
}

type Item struct {
	Name string `json:"name"`
	Task Task   `json:"task"`

	OnFailure OnFailure `json:"on_failure"`
	When      When      `json:"when"`
}

type Task interface {
	Name() string
	Run(context.Context, *environment.Env) error
}

type unsupportedTask struct {
	name string
}

func (t *unsupportedTask) Name() string {
	return t.name
}

func (t *unsupportedTask) Run(_ context.Context, env *environment.Env) error {
	env.Stderr.Write([]byte(fmt.Sprintf("unknown task %q, skipping.", t.name)))

	return nil
}

type (
	When      string
	OnFailure string
)

const (
	WhenAlways    = When("always")
	WhenOnFailure = When("on_failure")
	WhenOnSuccess = When("on_success")

	OnFailureReturn   = OnFailure("return")   // immediately exit the job
	OnFailureContinue = OnFailure("continue") // ignore failure and continue
	OnFailureBreak    = OnFailure("break")    // jump out of the current task group
)

func (s Script) Unmarshal(factory ...func() Task) (*Program, error) {
	supported := map[string]func() Task{}
	for _, f := range factory {
		supported[f().Name()] = f
	}

	var program Program

	var names struct {
		Tasks []struct {
			Name string `json:"name"`
		} `json:"tasks"`
	}
	if err := json.Unmarshal(s, &names); err != nil {
		return nil, fmt.Errorf("initial decoding tasks: %w", err)
	}

	program.Tasks = make([]Item, len(names.Tasks))
	for idx := range program.Tasks {
		item := &program.Tasks[idx]
		name := names.Tasks[idx].Name

		createTask, ok := supported[name]

		if !ok {
			item.Task = &unsupportedTask{name: name}
		} else {
			item.Task = createTask()
		}
	}

	if err := json.Unmarshal(s, &program); err != nil {
		return nil, fmt.Errorf("decoding tasks: %w", err)
	}

	return &program, nil
}

func (p *Program) Marshal() (Script, error) {
	for idx := range p.Tasks {
		item := &p.Tasks[idx]
		item.Name = item.Task.Name()
	}

	return json.MarshalIndent(p, "", " ")
}

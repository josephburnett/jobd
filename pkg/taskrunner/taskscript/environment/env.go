package environment

import (
	"io"
	"time"
)

type Env struct {
	WorkingDir string
	CacheDir   string
	Timeout    time.Duration

	Env   map[string]string
	Files map[string]string

	ID      int64
	Token   string
	BaseURL string

	Stdout io.Writer
	Stderr io.Writer

	InternalBinDir      string
	InternalGitExecPath string
}

func (e Env) GetID() int64 {
	return e.ID
}

func (e Env) GetToken() string {
	return e.Token
}

func (e Env) GetBaseURL() string {
	return e.BaseURL
}

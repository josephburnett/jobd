package v1

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/jobd/jobd/pkg/taskrunner/shell"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
)

type StepTask struct {
	Step   string   `json:"step"`
	Script []string `json:"script"`

	CommandArgs   []string `json:"command_args"`
	CommandScript string   `json:"command_script"`
	Extension     string   `json:"extension"`
}

func (t StepTask) Name() string {
	return "v1.step"
}

func (t StepTask) Run(ctx context.Context, env *environment.Env) error {
	f, err := os.CreateTemp(env.WorkingDir, "script*"+t.Extension)
	if err != nil {
		return fmt.Errorf("creating shell script file: %w", err)
	}
	defer os.RemoveAll(f.Name())

	if _, err := f.WriteString(t.CommandScript); err != nil {
		return fmt.Errorf("writing shell script: %w", err)
	}
	if err := f.Close(); err != nil {
		return fmt.Errorf("closing shell script: %w", err)
	}

	stdout := shell.Writer(env.Stdout, t.Script)
	defer stdout.Close()

	err = t.exec(ctx, execConfig{
		stdout: stdout,
		stderr: env.Stderr,
		dir:    env.WorkingDir,
		env:    env.Env,
		args:   shell.Args(filepath.Base(f.Name()), t.CommandArgs),
	})
	if err != nil {
		return fmt.Errorf("executing step: %w", err)
	}

	if err := stdout.Close(); err != nil {
		return fmt.Errorf("flushing shell writer: %w", err)
	}

	return nil
}

type execConfig struct {
	stdout, stderr io.Writer
	dir            string
	env            map[string]string
	args           []string
}

func (t *StepTask) exec(ctx context.Context, config execConfig) error {
	cmd := exec.CommandContext(ctx, config.args[0], config.args[1:]...)
	cmd.Stdout = config.stdout
	cmd.Stderr = config.stderr
	cmd.Dir = config.dir

	cmd.Env = os.Environ()
	for key, value := range config.env {
		cmd.Env = append(cmd.Env, key+"="+value)
	}

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("run %v: %w", config.args, err)
	}

	return nil
}

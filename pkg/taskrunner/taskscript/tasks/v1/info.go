package v1

import (
	"context"
	"fmt"

	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
)

type InfoTask struct {
	Message string `json:"message"`
}

func (t InfoTask) Name() string {
	return "v1.info"
}

func (t InfoTask) Run(ctx context.Context, env *environment.Env) error {
	_, err := env.Stdout.Write([]byte(t.Message))
	if err != nil {
		return fmt.Errorf("writing info message: %w", err)
	}

	return nil
}

package v1

import (
	"context"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/archiver"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/cachelock"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/matcher"
)

type CacheArchiveTask struct {
	Key            string   `json:"key"`
	Untracked      bool     `json:"untracked"`
	Paths          []string `json:"paths"`
	ArchiverFormat string   `json:"archiver_format"`
}

func (t CacheArchiveTask) Name() string {
	return "v1.cache_archive"
}

func (t CacheArchiveTask) Run(ctx context.Context, env *environment.Env) error {
	files, stats, err := matcher.Match(ctx, env.WorkingDir, t.Paths, nil, t.Untracked)
	if err != nil {
		return fmt.Errorf("patch matching: %w", err)
	}

	fmt.Fprintf(env.Stdout, "untracked: %v\n", t.Untracked)
	if len(t.Paths) > 0 {
		fmt.Fprintln(env.Stdout, "includes:")
		fmt.Fprintf(env.Stdout, "\t%s\n", strings.Join(t.Paths, "\t"))
	}
	fmt.Fprintf(env.Stdout, "matched %d files, %d untracked, %d bytes in %v\n",
		stats.Files, stats.Untracked, stats.TotalSize, stats.Duration)

	if len(files) == 0 {
		return nil
	}

	cacheDir := filepath.Join(env.CacheDir, t.Key)
	if !hasChanged(files, cachelock.Latest(cacheDir)) {
		return logfmt.Notice(env.Stderr, "cache", t.Key, "hasn't been modified, skipping")
	}

	err = cachelock.Create(cacheDir, func(path string) error {
		f, err := os.Create(filepath.Join(path, "archive"))
		if err != nil {
			return fmt.Errorf("creating archive file: %w", err)
		}
		defer f.Close()

		a, err := archiver.NewArchiver(t.ArchiverFormat, f, env.WorkingDir, archiver.Default)
		if err != nil {
			return fmt.Errorf("creating archiver: %w", err)
		}

		err = a.Archive(ctx, files)
		if err != nil {
			return fmt.Errorf("archiving: %w", err)
		}

		return f.Close()
	})

	if err != nil {
		return fmt.Errorf("creating cache: %w", err)
	}

	return err
}

func hasChanged(files map[string]fs.FileInfo, modtime time.Time) bool {
	for _, fi := range files {
		if fi.ModTime().After(modtime) {
			return true
		}
	}

	return false
}

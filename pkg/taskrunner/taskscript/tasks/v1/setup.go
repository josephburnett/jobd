package v1

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
)

type SetupTask struct {
}

func (t SetupTask) Name() string {
	return "v1.setup"
}

func (t SetupTask) Run(ctx context.Context, env *environment.Env) error {
	tmpDir := os.TempDir()

	// iterate each file, write it to disk, and export the location's name
	// as an environment variable
	h := sha256.New()
	for name, content := range env.Files {
		h.Reset()
		h.Write([]byte(content))
		path := filepath.Join(tmpDir, hex.EncodeToString(h.Sum(nil)))

		if err := os.WriteFile(path, []byte(content), 0777); err != nil {
			return fmt.Errorf("writing variable file: %w", err)
		}

		env.Env[name] = path
	}

	return logfmt.Notice(env.Stdout, "exported", len(env.Files), "files to disk")
}

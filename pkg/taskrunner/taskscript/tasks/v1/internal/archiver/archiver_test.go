package archiver

import (
	"context"
	"io"
	"io/fs"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArchiverFormats(t *testing.T) {
	formats := map[string]bool{
		"zip":     true,
		"gzip":    true,
		"raw":     true,
		"unknown": false,
		"foobar":  true,
	}

	Register("foobar", NewZipArchiver, NewZipExtractor)

	for format, known := range formats {
		assert.Equal(t, known, Has(format))

		a, err := NewArchiver(format, io.Discard, "test", Fastest)
		if known {
			assert.NoError(t, err)

			err = a.Archive(context.Background(), map[string]fs.FileInfo{})
			assert.NoError(t, err)
		} else {
			assert.Error(t, err)
		}
	}
}

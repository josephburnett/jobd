package archiver

import (
	"archive/zip"
	"context"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

const unsupportedModes = os.ModeSocket | os.ModeDevice | os.ModeCharDevice | os.ModeNamedPipe

func init() {
	Register("zip", NewZipArchiver, NewZipExtractor)
}

// ZipArchiver is a zip archiver.
type ZipArchiver struct {
	w   io.Writer
	dir string
}

// NewZipArchiver returns a new ZipArchiver.
func NewZipArchiver(w io.Writer, dir string, level CompressionLevel) (Archiver, error) {
	return &ZipArchiver{w: w, dir: dir}, nil
}

// Archive zips files and writes the stream to the writer passed to
// NewZipArchiver.
func (a *ZipArchiver) Archive(ctx context.Context, files map[string]fs.FileInfo) (err error) {
	zw := zip.NewWriter(a.w)
	defer func() {
		if cerr := zw.Close(); err == nil && cerr != nil {
			err = cerr
		}
	}()

	for pathname, fi := range files {
		if fi.Mode()&unsupportedModes != 0 {
			continue
		}

		if !strings.HasPrefix(pathname, a.dir) {
			return fmt.Errorf("%s cannot be archived from outside of main directory (%s)", pathname, a.dir)
		}

		fh, err := zip.FileInfoHeader(fi)
		if err != nil {
			return fmt.Errorf("file info header: %w", err)
		}

		rel, err := filepath.Rel(a.dir, pathname)
		if err != nil {
			return fmt.Errorf("relative path: %w", err)
		}

		fh.Name = filepath.ToSlash(rel)
		if fh.Mode().IsDir() {
			fh.Name += "/"
		}

		w, err := zw.CreateHeader(fh)
		if err != nil {
			return fmt.Errorf("create header: %w", err)
		}

		switch fi.Mode() & os.ModeType {
		case os.ModeDir:
			// continue
		case os.ModeSymlink:
			link, err := os.Readlink(pathname)
			if err != nil {
				return fmt.Errorf("reading link: %w", err)
			}
			if _, err = io.WriteString(w, link); err != nil {
				return fmt.Errorf("writing link: %w", err)
			}

		default:
			f, err := os.Open(pathname)
			if err != nil {
				return fmt.Errorf("opening file: %w", err)
			}

			_, err = io.Copy(w, f)
			f.Close()

			if err != nil {
				return fmt.Errorf("copying: %w", err)
			}
		}
	}

	return nil
}

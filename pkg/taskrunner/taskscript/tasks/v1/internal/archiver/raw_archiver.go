package archiver

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
)

// ErrTooManyRawFiles is returned if more than one file is passed to the
// RawArchiver.
var ErrTooManyRawFiles = errors.New("only one file can be sent as raw")

func init() {
	Register("raw", NewRawArchiver, nil)
}

// RawArchiver is a raw archiver. It doesn't support compression nor multiple
// files.
type RawArchiver struct {
	w   io.Writer
	dir string
}

// NewRawArchiver returns a new RawArchiver.
func NewRawArchiver(w io.Writer, dir string, level CompressionLevel) (Archiver, error) {
	return &RawArchiver{w: w, dir: dir}, nil
}

// Archive opens and copies a single file to the writer passed to
// NewRawArchiver. If more than one file is passed, ErrTooManyRawFiles is
// returned.
func (a *RawArchiver) Archive(ctx context.Context, files map[string]fs.FileInfo) error {
	if len(files) > 1 {
		return ErrTooManyRawFiles
	}

	for pathname := range files {
		f, err := os.Open(pathname)
		if err != nil {
			return fmt.Errorf("opening file: %w", err)
		}

		_, err = io.Copy(a.w, f)
		f.Close()

		return fmt.Errorf("copying: %w", err)
	}

	return nil
}

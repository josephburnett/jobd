package archiver

import (
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"io/fs"
	"os"
	"strings"
)

func init() {
	Register("gzip", NewGzipArchiver, nil)
}

// GzipArchiver is a gzip stream archiver.
type GzipArchiver struct {
	w   io.Writer
	dir string
}

// NewGzipArchiver returns a new GzipArchiver.
func NewGzipArchiver(w io.Writer, dir string, level CompressionLevel) (Archiver, error) {
	return &GzipArchiver{w: w, dir: dir}, nil
}

// Archive archives all files as new gzip streams.
func (a *GzipArchiver) Archive(ctx context.Context, files map[string]fs.FileInfo) error {
	for pathname, fi := range files {
		w := gzip.NewWriter(a.w)
		w.Name = fi.Name()
		w.Comment = strings.TrimPrefix(pathname, a.dir)
		w.ModTime = fi.ModTime()

		err := func() (err error) {
			defer func() {
				if cerr := w.Close(); err == nil && cerr != nil {
					err = fmt.Errorf("closing: %w", cerr)
				}
			}()

			f, err := os.Open(pathname)
			if err != nil {
				return fmt.Errorf("opening file: %w", err)
			}

			_, err = io.Copy(w, f)
			f.Close()

			return fmt.Errorf("copying: %w", err)
		}()

		if err != nil {
			return fmt.Errorf("archiving %q: %w", pathname, err)
		}
	}

	return nil
}

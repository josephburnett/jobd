package archiver

import (
	"archive/zip"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

// ZipExtractor is a zip extractor.
type ZipExtractor struct {
	r   io.Reader
	dir string
}

// NewZipExtractor returns a new ZipExtractor.
func NewZipExtractor(r io.Reader, dir string) (Extractor, error) {
	return &ZipExtractor{r: r, dir: dir}, nil
}

// Extract extracts files from the reader to the directory passed to
// NewZipExtractor.
func (e *ZipExtractor) Extract(ctx context.Context) error {
	f, err := ioutil.TempFile(e.dir, "")
	if err != nil {
		return fmt.Errorf("creating temporary file: %w", err)
	}
	defer os.RemoveAll(f.Name())
	defer f.Close()
	defer f.Truncate(0)

	n, err := io.Copy(f, e.r)
	if err != nil {
		return fmt.Errorf("copying: %w", err)
	}

	zr, err := zip.NewReader(f, n)
	if err != nil {
		return fmt.Errorf("opening archive: %w", err)
	}

	for _, file := range zr.File {
		if file.Mode()&unsupportedModes != 0 {
			continue
		}

		var err error
		switch file.Mode() & os.ModeType {
		case os.ModeDir:
			err = os.MkdirAll(filepath.Join(e.dir, file.Name), file.Mode().Perm())
			if err != nil {
				err = fmt.Errorf("creating directory: %w", err)
			}

		case os.ModeSymlink:
			err = e.createSymlink(file)
			if err != nil {
				err = fmt.Errorf("creating symlink: %w", err)
			}

		default:
			err = e.createFile(file)
			if err != nil {
				err = fmt.Errorf("creating file: %w", err)
			}
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func (e *ZipExtractor) createSymlink(file *zip.File) error {
	path := filepath.Join(e.dir, file.Name)
	if err := os.Remove(path); err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("removing file: %w", err)
	}

	if err := os.MkdirAll(filepath.Dir(path), 0777); err != nil {
		return fmt.Errorf("creating directory: %w", err)
	}

	f, err := file.Open()
	if err != nil {
		return fmt.Errorf("opening zip entry: %w", err)
	}
	defer f.Close()

	name, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	if err := os.Symlink(string(name), path); err != nil {
		return fmt.Errorf("creating symlink: %w", err)
	}

	return nil
}

func (e *ZipExtractor) createFile(file *zip.File) (err error) {
	path := filepath.Join(e.dir, file.Name)
	if err := os.Remove(path); err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("removing file: %w", err)
	}

	if err := os.MkdirAll(filepath.Dir(path), 0777); err != nil {
		return fmt.Errorf("creating directory: %w", err)
	}

	f, err := file.Open()
	if err != nil {
		return fmt.Errorf("opening zip entry: %w", err)
	}
	defer f.Close()

	w, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, file.Mode().Perm())
	if err != nil {
		return fmt.Errorf("creating file: %w", err)
	}
	defer func() {
		if cerr := w.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()

	_, err = io.Copy(w, f)
	return err
}

// Package archiver provides archiving and extraction for caches and build
// artifacts.
package archiver

import (
	"context"
	"errors"
	"io"
	"io/fs"
	"sync"
)

var (
	lock       sync.Mutex
	archivers  = make(map[string]NewArchiverCallback)
	extractors = make(map[string]NewExtractorCallback)
)

var (
	// ErrArchiverNotFound is returned if an archiver or extractor format
	// requested has not been registered.
	ErrArchiverNotFound = errors.New("archiver not found")
)

// CompressionLevel type for specifying a compression level.
type CompressionLevel int

/// Compression levels from fastest (low/zero compression ratio) to slowest
// (high compression ratio).
const (
	Fastest CompressionLevel = -2
	Fast    CompressionLevel = -1
	Default CompressionLevel = 0
	Slow    CompressionLevel = 1
	Slowest CompressionLevel = 2
)

// Archiver is an interface for the Archive method.
type Archiver interface {
	Archive(ctx context.Context, files map[string]fs.FileInfo) error
}

// Extractor is an interface for the Extract method.
type Extractor interface {
	Extract(ctx context.Context) error
}

// NewArchiverCallback is the callback called when registering a new archiver.
type NewArchiverCallback func(w io.Writer, dir string, level CompressionLevel) (Archiver, error)

// NewExtractorCallback is the callback called when registering a new extractor.
type NewExtractorCallback func(r io.Reader, dir string) (Extractor, error)

// Register registers a new archiver.
func Register(format string, archiver NewArchiverCallback, extractor NewExtractorCallback) {
	lock.Lock()
	defer lock.Unlock()

	archivers[format] = archiver
	extractors[format] = extractor
}

// Has returns whether an archiver has been registered.
func Has(format string) bool {
	lock.Lock()
	defer lock.Unlock()

	_, aok := archivers[format]
	_, eok := extractors[format]
	return aok && eok
}

// NewArchiver returns a new Archiver of the specified format.
//
// The archiver will ensure that files to be archived are children of the
// directory provided.
func NewArchiver(format string, w io.Writer, dir string, level CompressionLevel) (Archiver, error) {
	lock.Lock()
	defer lock.Unlock()

	fn := archivers[format]
	if fn == nil {
		return nil, ErrArchiverNotFound
	}

	return fn(w, dir, level)
}

// NewExtractor returns a new Extractor of the specified format.
//
// The extractor will extract files to the directory provided.
func NewExtractor(format string, r io.Reader, dir string) (Extractor, error) {
	lock.Lock()
	defer lock.Unlock()

	fn := extractors[format]
	if fn == nil {
		return nil, ErrArchiverNotFound
	}

	return fn(r, dir)
}

package matcher

import (
	"bytes"
	"context"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/saracen/matcher"
)

type excludeMatcher struct {
	excludes matcher.Matcher
	includes matcher.Matcher
}

func (m *excludeMatcher) Match(pathname string) (matcher.Result, error) {
	result, err := m.excludes.Match(pathname)
	if result == matcher.Matched || err != nil {
		return matcher.NotMatched, err
	}

	return m.includes.Match(pathname)
}

type matchStats struct {
	Files     int
	Untracked int
	Duration  time.Duration
	TotalSize int64
}

func createMatcher(patterns []string) matcher.Matcher {
	var matchers []matcher.Matcher

	for _, p := range patterns {
		// rewrite pattern to conform to previous or intended behaviour
		switch p {
		case `*`, `.`, `./`:
			matchers = append(matchers, matcher.New("**/*"))
			continue
		}

		// remove any number of ./ at the start of the string
		for {
			lp := len(p)
			if p = strings.TrimPrefix(p, "./"); len(p) != lp {
				continue
			}
			break
		}

		if !strings.HasSuffix(p, "/") {
			matchers = append(matchers, matcher.New(p))
		}

		// add recursive match
		matchers = append(matchers, matcher.New(path.Join(p, "**")))
	}

	return matcher.Multi(matchers...)
}

func Match(
	ctx context.Context,
	dir string,
	includes, excludes []string,
	untracked bool,
) (map[string]os.FileInfo, *matchStats, error) {
	start := time.Now()

	var opts []matcher.GlobOption

	// case-insensitive search on windows
	if runtime.GOOS == "windows" {
		opts = append(opts, matcher.WithPathTransformer(func(pathname string) string {
			return strings.ToLower(pathname)
		}))
		for idx := range excludes {
			excludes[idx] = strings.ToLower(excludes[idx])
		}
		for idx := range includes {
			includes[idx] = strings.ToLower(includes[idx])
		}
	}

	files, err := matcher.Glob(ctx, dir, &excludeMatcher{
		excludes: createMatcher(excludes),
		includes: createMatcher(includes),
	}, opts...)
	if err != nil {
		return files, nil, err
	}

	stats := &matchStats{
		Files: len(files),
	}

	if untracked {
		stats.Untracked, err = processGitUntracked(ctx, dir, files)
		if err != nil {
			return files, stats, err
		}
	}

	for _, fi := range files {
		stats.TotalSize += fi.Size()
	}

	stats.Duration = time.Since(start)

	return files, stats, err
}

func processGitUntracked(ctx context.Context, dir string, files map[string]os.FileInfo) (int, error) {
	buf := new(bytes.Buffer)
	cmd := exec.CommandContext(ctx, "git", "ls-files", "-o", "-z")
	cmd.Dir = dir
	cmd.Stdout = buf

	if err := cmd.Run(); err != nil {
		return 0, err
	}

	var count int
	for _, path := range strings.Split(buf.String(), "\x00") {
		if path == "" {
			continue
		}

		path = filepath.Join(dir, path)
		if _, ok := files[path]; ok {
			continue
		}

		fi, err := os.Lstat(path)
		if err != nil {
			continue
		}

		files[path] = fi
		count++
	}

	return count, nil
}

package matcher

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func matchFilesystemSetup(t *testing.T, fn func(dir string)) {
	t.Helper()

	files := []string{
		"/bar.txt",
		"/build/foo.txt",
		"/build/a/foo.txt",
		"/build/a/b/foo.txt",
		"/build/a/b/c/foo.txt",
	}

	dir := t.TempDir()
	for _, file := range files {
		pathname := filepath.Join(dir, file)

		require.NoError(t, os.MkdirAll(filepath.Dir(pathname), 0700))
		require.NoError(t, ioutil.WriteFile(pathname, nil, 0600))
	}

	fn(dir)
}

func TestMatchRootPatterns(t *testing.T) {
	patterns := []string{".", "./", "*", "**", "**/*"}

	matchFilesystemSetup(t, func(dir string) {
		dir = filepath.Join(dir, "build")

		expected := []string{
			"foo.txt",
			"a",
			"a/foo.txt",
			"a/b",
			"a/b/foo.txt",
			"a/b/c",
			"a/b/c/foo.txt",
		}

		for _, pattern := range patterns {
			t.Run(pattern, func(t *testing.T) {
				files, stats, err := Match(context.Background(), dir, []string{pattern}, nil, false)
				assert.NoError(t, err)
				assert.Equal(t, len(expected), stats.Files)

				for _, name := range expected {
					assert.Contains(t, files, filepath.Join(dir, name))
				}
			})
		}
	})
}

func TestMatchPatternWithDotSlash(t *testing.T) {
	patterns := []string{"./foo.txt", "././foo.txt"}

	matchFilesystemSetup(t, func(dir string) {
		dir = filepath.Join(dir, "build")

		expected := []string{
			"foo.txt",
		}

		for _, pattern := range patterns {
			t.Run(pattern, func(t *testing.T) {
				files, stats, err := Match(context.Background(), dir, []string{pattern}, nil, false)
				assert.NoError(t, err)
				assert.Equal(t, len(expected), stats.Files)

				for _, name := range expected {
					assert.Contains(t, files, filepath.Join(dir, name))
				}
			})
		}
	})
}

func TestMatchRecursive(t *testing.T) {
	matchFilesystemSetup(t, func(dir string) {
		dir = filepath.Join(dir, "build")

		includes := []string{
			"../bar.txt",
			"a/b/",
			"a/b/c",
		}

		expected := []string{
			"a/b",
			"a/b/foo.txt",
			"a/b/c",
			"a/b/c/foo.txt",
		}

		files, stats, err := Match(context.Background(), dir, includes, nil, false)
		assert.NoError(t, err)
		assert.Equal(t, len(expected), stats.Files)

		for _, name := range expected {
			assert.Contains(t, files, filepath.Join(dir, name))
		}
	})
}

func TestMatchRecursiveExclude(t *testing.T) {
	matchFilesystemSetup(t, func(dir string) {
		dir = filepath.Join(dir, "build")

		includes := []string{
			"a/foo.txt", // includes a/foo.txt
			"a/b/*",     // includes a/b/*
		}

		excludes := []string{
			"a/b/*/", // excludes *directories* but not files within a/b
		}

		expected := []string{
			"a/foo.txt",
			"a/b",
			"a/b/foo.txt",
		}

		files, stats, err := Match(context.Background(), dir, includes, excludes, false)
		assert.NoError(t, err)
		assert.Equal(t, len(expected), stats.Files)

		for _, name := range expected {
			assert.Contains(t, files, filepath.Join(dir, name))
		}
	})
}

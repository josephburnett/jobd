// Package cachelock enables caches to be overwritten whilst an older version
// of the cache can continue to be read from.
//
// This helps where a cache directory is shared between multiple jobs that could
// be writing or reading to/from the same cache key.
//
// Each cache key has the following directory structure:
//   /cache_dir/cache_key/<random key>/.lock/
//   /cache_dir/cache_key/<random key>/data/
//
// The .lock subdirectory contains current locks. If not removed (due to a
// crash), these expire after an hour. It is assumed that data access will not
// exceed an hour, because data is only ever copied out of the directory, never
// modified in place.
//
// The data subdirectory contains the cache data.
//
// When a new cache is to replace another, LatestCache() can be called which
// will prune older caches that have no locks present and return the latest.
package cachelock

import (
	"crypto/rand"
	"encoding/hex"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const (
	lockDirName = "lock"
	dataDirName = "data"
	cacheMvName = ".fossil"
	tempPrefix  = "temp-"
)

type Cache struct {
	lock string
	path string
	mod  time.Time
}

func (c Cache) Release() {
	if c.lock != "" {
		os.Remove(c.lock)
	}
}

func (c Cache) Path() string {
	return filepath.Join(c.path, dataDirName)
}

func (c Cache) ModTime() time.Time {
	return c.mod
}

func Create(pathname string, fn func(string) error) error {
	if err := os.MkdirAll(pathname, 0777); err != nil {
		return err
	}

	dir, err := os.MkdirTemp(pathname, tempPrefix)
	if err != nil {
		return err
	}
	defer os.RemoveAll(dir)

	// create lock directory
	if err := os.Mkdir(filepath.Join(dir, lockDirName), 0777); err != nil {
		return err
	}

	// create data directory
	dataPath := filepath.Join(dir, dataDirName)
	if err := os.Mkdir(dataPath, 0777); err != nil {
		return err
	}

	if err := fn(dataPath); err != nil {
		return err
	}

	var b [20]byte
	rand.Read(b[:])

	return os.Rename(dir, filepath.Join(pathname, hex.EncodeToString(b[:])))
}

func Latest(pathname string) time.Time {
	var modtime time.Time

	entries, _ := os.ReadDir(pathname)
	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		}
		if strings.HasPrefix(entry.Name(), tempPrefix) {
			continue
		}

		entryPath := filepath.Join(pathname, entry.Name())
		stat, err := os.Stat(filepath.Join(entryPath, dataDirName))
		if err != nil {
			continue
		}

		if stat.ModTime().After(modtime) {
			modtime = stat.ModTime()
		}
	}

	return modtime
}

// Get returns the latest cache for the specified path and prunes any
// older caches that have no locks. Calls to Path() will be empty if no cache is
// found. The caller should release the cache with .Release() to delete the lock
// held whilst the cache is being used.
func Get(pathname string) Cache {
	var modtime time.Time
	var latest Cache

	entries, _ := os.ReadDir(pathname)
	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		}
		if strings.HasPrefix(entry.Name(), tempPrefix) {
			continue
		}

		entryPath := filepath.Join(pathname, entry.Name())
		stat, err := os.Stat(filepath.Join(entryPath, dataDirName))
		if err != nil {
			continue
		}

		if stat.ModTime().After(modtime) {
			// obtain lock for new cache
			cache, ok := lock(entryPath)
			if !ok {
				continue
			}

			// release previous winner
			latest.Release()

			// try to remove cache (if no more locks)
			remove(latest.path)

			// update latest cache
			modtime = stat.ModTime()
			latest = cache
			continue
		}

		remove(entryPath)
	}

	return latest
}

func lock(pathname string) (Cache, bool) {
	var b [20]byte
	rand.Read(b[:])

	stat, err := os.Stat(filepath.Join(pathname, dataDirName))
	if err != nil {
		return Cache{}, false
	}

	name := filepath.Join(pathname, lockDirName, hex.EncodeToString(b[:]))
	if err := os.Mkdir(name, 0777); err != nil {
		return Cache{}, false
	}

	if _, err := os.Stat(name); err != nil {
		return Cache{}, false
	}

	return Cache{name, pathname, stat.ModTime()}, true
}

func remove(pathname string) bool {
	fossil := filepath.Clean(pathname) + cacheMvName

	if check(pathname) {
		os.Rename(pathname, fossil)
		os.RemoveAll(fossil)
	}

	_, err := os.Stat(pathname)
	return os.IsNotExist(err)
}

func check(pathname string) bool {
	pathname = filepath.Join(pathname, lockDirName)

	entries, _ := os.ReadDir(pathname)
	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		}

		entryPath := filepath.Join(pathname, entry.Name())
		stat, err := os.Stat(entryPath)
		if err != nil {
			os.Remove(entryPath)
			continue
		}

		if time.Now().After(stat.ModTime().Add(time.Hour)) {
			os.Remove(entryPath)
			continue
		}
	}

	// attempt to remove lock directory: only succeeds if empty
	if err := os.Remove(pathname); err != nil {
		return os.IsNotExist(err)
	}

	return true
}

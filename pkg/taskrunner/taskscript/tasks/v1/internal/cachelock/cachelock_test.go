package cachelock

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestCache(t *testing.T) {
	cacheDir := filepath.Join(t.TempDir(), "cache-key")

	// create new cache
	now := time.Now()
	err := Create(cacheDir, func(path string) error {
		err := os.WriteFile(filepath.Join(path, "data"), []byte("data"), 0777)
		require.NoError(t, err)

		// update modification time for test before
		err = os.Chtimes(path, now, now)
		require.NoError(t, err)

		return nil
	})
	require.NoError(t, err)

	// obtain cache
	cache := Get(cacheDir)
	require.True(t, strings.HasPrefix(cache.path, cacheDir))
	require.WithinDuration(t, now, cache.ModTime(), time.Second)
	require.WithinDuration(t, now, Latest(cacheDir), time.Second)

	entries, err := os.ReadDir(filepath.Join(cache.path, lockDirName))
	require.NoError(t, err)
	require.Equal(t, 1, len(entries))

	// attempting to remove fails
	ok := remove(cache.path)
	require.False(t, ok)

	// release lock
	cache.Release()
	entries, err = os.ReadDir(filepath.Join(cache.path, lockDirName))
	require.NoError(t, err)
	require.Equal(t, 0, len(entries))

	// attempting to remove succeeds
	ok = remove(cache.path)
	require.True(t, ok)

	// latest cache now empty
	cache = Get(cacheDir)
	require.Empty(t, cache.path)
}

func TestMultipleCaches(t *testing.T) {
	cacheDir := filepath.Join(t.TempDir(), "cache-key")

	// create one cache
	require.NoError(t, Create(cacheDir, func(path string) error {
		return os.WriteFile(filepath.Join(path, "data"), []byte("one"), 0777)
	}))

	time.Sleep(time.Millisecond)

	// create two cache
	require.NoError(t, Create(cacheDir, func(path string) error {
		return os.WriteFile(filepath.Join(path, "data"), []byte("two"), 0777)
	}))

	// two caches should exist for this key
	entries, err := os.ReadDir(cacheDir)
	require.NoError(t, err)
	require.Equal(t, 2, len(entries))

	cache := Get(cacheDir)
	buf, err := os.ReadFile(filepath.Join(cache.Path(), "data"))
	require.NoError(t, err)
	require.Equal(t, "two", string(buf))
	cache.Release()

	// only one cache should exist for this key
	entries, err = os.ReadDir(cacheDir)
	require.NoError(t, err)
	require.Equal(t, 1, len(entries))
}

func TestLockConcurrency(t *testing.T) {
	dir := t.TempDir()

	type result struct {
		locked  bool
		removed bool
	}

	results := make([]result, 1000)
	var wg sync.WaitGroup
	for i := 0; i < 1000; i++ {
		cacheDir := filepath.Join(dir, fmt.Sprintf("cache-key-%d", i))

		require.NoError(t, Create(cacheDir, func(s string) error {
			return nil
		}))

		wg.Add(2)

		// obtain lock (never unlock)
		go func(idx int) {
			_, locked := lock(cacheDir)
			results[idx].locked = locked
			wg.Done()
		}(i)

		// try remove
		go func(idx int) {
			results[idx].removed = remove(cacheDir)
			wg.Done()
		}(i)
	}

	wg.Wait()

	for _, result := range results {
		require.False(t, result.locked && result.removed, "cache directory both locked and removed")
	}
}

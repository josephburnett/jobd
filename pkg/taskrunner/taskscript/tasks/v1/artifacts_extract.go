package v1

import (
	"context"
	"fmt"

	"gitlab.com/jobd/jobd/pkg/ci/client/artifactclient"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/archiver"
)

type ArtifactsExtractTask struct {
	ID           int64  `json:"id"`
	Token        string `json:"token"`
	ArtifactName string `json:"artifact_name"`
	Filename     string `json:"filename"`
}

func (t ArtifactsExtractTask) Name() string {
	return "v1.artifacts_extract"
}

func (t ArtifactsExtractTask) Run(ctx context.Context, env *environment.Env) error {
	ac, err := artifactclient.New(env)
	if err != nil {
		return fmt.Errorf("creating artifact client: %w", err)
	}

	r, err := ac.Download(ctx, artifactclient.DownloadInfo{
		ID:    t.ID,
		Token: t.Token,
	})
	if err != nil {
		return fmt.Errorf("downloading artifact: %w", err)
	}
	defer r.Close()

	extractor, err := archiver.NewExtractor("zip", r, env.WorkingDir)
	if err != nil {
		return fmt.Errorf("creating extractor: %w", err)
	}

	if err := extractor.Extract(ctx); err != nil {
		return fmt.Errorf("extracting: %w", err)
	}

	return nil
}

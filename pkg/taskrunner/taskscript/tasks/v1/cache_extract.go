package v1

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/archiver"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/cachelock"
)

type CacheExtractTask struct {
	Key            string `json:"key"`
	ArchiverFormat string `json:"archiver_format"`
}

func (t CacheExtractTask) Name() string {
	return "v1.cache_extract"
}

func (t CacheExtractTask) Run(ctx context.Context, env *environment.Env) error {
	cacheDir := filepath.Join(env.CacheDir, t.Key)

	cache := cachelock.Get(cacheDir)
	defer cache.Release()
	if cache.Path() == "" {
		return nil
	}

	f, err := os.Open(filepath.Join(cache.Path(), "archive"))
	if os.IsNotExist(err) {
		return logfmt.Notice(env.Stderr, "cache", t.Key, "doesn't exist, skipping")
	}
	if err != nil {
		return fmt.Errorf("opening cache archive: %w", err)
	}
	defer f.Close()

	extractor, err := archiver.NewExtractor(t.ArchiverFormat, f, env.WorkingDir)
	if err != nil {
		return fmt.Errorf("creating extractor: %w", err)
	}

	if err := extractor.Extract(ctx); err != nil {
		return fmt.Errorf("extracting: %w", err)
	}

	return nil
}

package v1

import (
	"context"
	"fmt"
	"io"
	"strings"

	"gitlab.com/jobd/jobd/pkg/ci/client/artifactclient"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/archiver"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1/internal/matcher"
)

type ArtifactsArchiveTask struct {
	Untracked    bool     `json:"untracked"`
	Paths        []string `json:"paths"`
	Exclude      []string `json:"exclude"`
	ArtifactName string   `json:"archive_name"`
	ExpireIn     string   `json:"expire_in"`
	Format       string   `json:"format"`
	Type         string   `json:"type"`
}

func (t ArtifactsArchiveTask) Name() string {
	return "v1.artifact_archive"
}

func (t ArtifactsArchiveTask) Run(ctx context.Context, env *environment.Env) error {
	files, stats, err := matcher.Match(ctx, env.WorkingDir, t.Paths, t.Exclude, t.Untracked)
	if err != nil {
		return fmt.Errorf("patch matching: %w", err)
	}

	fmt.Fprintf(env.Stdout, "untracked: %v\n", t.Untracked)
	if len(t.Paths) > 0 {
		fmt.Fprintln(env.Stdout, "includes:")
		fmt.Fprintf(env.Stdout, "\t%s\n", strings.Join(t.Paths, "\t"))
	}
	if len(t.Exclude) > 0 {
		fmt.Fprintln(env.Stdout, "excludes:")
		fmt.Fprintf(env.Stdout, "\t%s\n", strings.Join(t.Exclude, "\t"))
	}
	fmt.Fprintf(env.Stdout, "matched %d files, %d untracked, %d bytes in %v\n",
		stats.Files, stats.Untracked, stats.TotalSize, stats.Duration)

	if len(files) == 0 {
		return nil
	}

	ac, err := artifactclient.New(env)
	if err != nil {
		return fmt.Errorf("creating artifact client: %w", err)
	}

	info := artifactclient.UploadInfo{
		Name:     t.ArtifactName,
		ExpireIn: t.ExpireIn,
		Format:   t.Format,
		Type:     t.Type,
	}

	return ac.Upload(ctx, info, func(w io.Writer) error {
		a, err := archiver.NewArchiver(t.Format, w, env.WorkingDir, archiver.Default)
		if err != nil {
			return fmt.Errorf("creating artifact archiver: %w", err)
		}

		if err := a.Archive(ctx, files); err != nil {
			return fmt.Errorf("archiving: %w", err)
		}

		return nil
	})
}

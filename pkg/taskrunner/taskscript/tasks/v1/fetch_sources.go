package v1

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/fs"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
)

var gitTransientError *regexp.Regexp

func init() {
	errs := []string{
		// git errors
		`The remote end hung up unexpectedly`,
		`RPC failed;`,
		`unable to look up`,
		`The requested URL returned error`,

		// curl errors
		`Couldn't resolve host`,
		`Connection timed out`,

		// https://github.com/curl/curl/blob/cbb5429001084df4e71ebd95dbf748c3c302c9f7/lib/strerror.c#L57
		`Couldn't resolve proxy name`,
		`Couldn't resolve host name`,
		`Couldn't connect to server`,
		`Timeout was reached`,
		`SSL connect error`,

		// https://github.com/curl/curl/blob/cbb5429001084df4e71ebd95dbf748c3c302c9f7/lib/strerror.c#L456
		`Network down`,
		`Network unreachable`,
		`Network has been reset`,
		`Connection was reset`,
		`Timed out`,
		`Connection refused`,
		`Host down`,
		`Host unreachable`,
		`Remote error`,
		`Host not found`,
	}

	gitTransientError = regexp.MustCompile(strings.Join(errs, "|"))
}

type FetchSourcesTask struct {
	AllowGitFetch bool     `json:"allow_git_fetch"`
	Checkout      bool     `json:"checkout"`
	MaxAttempts   int      `json:"max_attempts"`
	Submodules    string   `json:"submodules"`
	LFSDisabled   bool     `json:"lfs_disabled"`
	Depth         int      `json:"depth"`
	RepoURL       string   `json:"repo_url"`
	Refspecs      []string `json:"refspecs"`
	SHA           string   `json:"sha"`
}

func (t FetchSourcesTask) Name() string {
	return "v1.fetch_sources"
}

func (t FetchSourcesTask) Run(ctx context.Context, env *environment.Env) error {
	t.LFSDisabled = t.isGitLfsDisabled(env)

	dir := env.WorkingDir
	err := filepath.WalkDir(filepath.Join(dir, ".git"), func(path string, d fs.DirEntry, err error) error {
		switch filepath.Base(path) {
		case "lfs", "objects":
			return filepath.SkipDir
		}

		return os.RemoveAll(path)
	})
	if err != nil {
		return fmt.Errorf("cleaning .git directory: %w", err)
	}

	if err := os.MkdirAll(dir, 0755); err != nil {
		return fmt.Errorf("creating work directory: %w", err)
	}

	var cmds [][]string
	cmds = append(cmds, []string{"init", ".", "--initial-branch", "main", "--template", t.gitTemplateDir(env)})
	cmds = append(cmds, []string{"config", "fetch.recurseSubmodules", "false"})
	cmds = append(cmds, []string{"config", "remote.origin.url", t.RepoURL})

	uri, _ := url.Parse(t.RepoURL)
	if uri != nil {
		uri.Path = ""
		uri.User = nil
		cmds = append(cmds, []string{"config", "http." + uri.String() + ".sslCAInfo", env.Env["CI_SERVER_TLS_CA_FILE"]})
	}

	fetch := []string{"fetch", "origin", "--prune"}
	fetch = append(fetch, t.Refspecs...)
	if t.Depth > 0 {
		fetch = append(fetch, "--depth", strconv.Itoa(t.Depth))
	}
	cmds = append(cmds, fetch)

	// perform checkout
	if t.Checkout {
		cmds = append(cmds, []string{"checkout", "-f", "-q", t.SHA})
		cmds = append(cmds, []string{"clean", "-ffdx"})
	}

	if !t.LFSDisabled {
		cmds = append(cmds, []string{"lfs", "pull"})
	}

	switch t.Submodules {
	case "normal":
		cmds = append(cmds, [][]string{
			{"submodule", "sync"},
			{"submodule", "update", "--init"},
			{"submodule", "foreach", "git", "clean", "-ffxd"},
			{"submodule", "foreach", "git", "reset", "--hard"},
			{"submodule", "foreach", "git", "lfs", "pull"},
		}...)

		if !t.LFSDisabled {
			cmds = append(cmds, []string{"submodule", "foreach", "git", "lfs", "pull"})
		}

	case "recursive":
		cmds = append(cmds, [][]string{
			{"submodule", "sync", "--recursive"},
			{"submodule", "update", "--init", "--recursive"},
			{"submodule", "foreach", "--recursive", "git", "clean", "-ffxd"},
			{"submodule", "foreach", "--recursive", "git", "reset", "--hard"},
			{"submodule", "foreach", "--recursive", "git", "lfs", "pull"},
		}...)

		if !t.LFSDisabled {
			cmds = append(cmds, []string{"submodule", "foreach", "--recursive", "git", "lfs", "pull"})
		}
	}

	gitPath, gitExecPath := t.gitPath(env)
	sshPath := t.internalSshPath(env)

	// execute git commands
	return executeGit(ctx, gitConfig{
		gitPath:     gitPath,
		gitExecPath: gitExecPath,
		sshPath:     sshPath,
		stdout:      env.Stdout,
		stderr:      env.Stderr,
		cmds:        cmds,
		dir:         dir,
		env:         env.Env,
		maxAttempts: t.MaxAttempts,
	})
}

func (t FetchSourcesTask) gitTemplateDir(env *environment.Env) string {
	const envKey = "GIT_TEMPLATE_DIR"
	const defaultPath = "/usr/share/git-core/templates"

	// retain the order documented in:
	// https://git-scm.com/docs/git-init#_template_directory
	if val, ok := os.LookupEnv(envKey); ok {
		return val
	}
	if val, ok := env.Env[envKey]; ok {
		return val
	}

	_, err := os.Stat(defaultPath)
	if !os.IsNotExist(err) {
		return defaultPath
	}

	return ""
}

func (t FetchSourcesTask) isGitLfsDisabled(env *environment.Env) bool {
	_, err := exec.LookPath("git-lfs")
	if err != nil {
		return true
	}

	_, err = exec.LookPath(filepath.Join(env.InternalGitExecPath, "git-lfs"))
	return err != nil
}

func (t FetchSourcesTask) gitPath(env *environment.Env) (string, string) {
	path, err := exec.LookPath("git")
	if err == nil {
		return path, ""
	}

	path, err = exec.LookPath(filepath.Join(env.InternalBinDir, "git"))
	if err == nil {
		return path, env.InternalGitExecPath
	}

	return "", ""
}

func (t FetchSourcesTask) internalSshPath(env *environment.Env) string {
	_, err := exec.LookPath("ssh")
	if err == nil {
		return ""
	}

	path, _ := exec.LookPath(filepath.Join(env.InternalBinDir, "ssh"))
	return path
}

type gitConfig struct {
	gitPath        string
	gitExecPath    string
	sshPath        string
	stdout, stderr io.Writer
	cmds           [][]string
	dir            string
	env            map[string]string
	maxAttempts    int
}

func executeGit(ctx context.Context, config gitConfig) error {
	environment := os.Environ()
	for key, value := range config.env {
		environment = append(environment, key+"="+value)
	}

	// https://github.com/git-lfs/git-lfs/issues/3524
	environment = append(environment, "GIT_LFS_SKIP_SMUDGE=1")

	if config.sshPath != "" {
		environment = append(environment, os.Getenv("PATH")+":"+config.sshPath)
	}

	buf := new(bytes.Buffer)
	for _, args := range config.cmds {
		for attempt := 0; attempt < config.maxAttempts; attempt++ {
			buf.Reset()

			err := func() error {
				if config.gitExecPath != "" {
					args = append([]string{"--exec-path=" + config.gitExecPath}, args...)
				}

				cmd := exec.CommandContext(ctx, config.gitPath, args...)
				cmd.Stdout = config.stdout
				cmd.Stderr = io.MultiWriter(config.stderr, buf)
				cmd.Dir = config.dir
				cmd.Env = environment

				defer func() {
					if cmd.Process != nil {
						cmd.Process.Kill()
					}
				}()

				return cmd.Run()
			}()

			if err == nil {
				break
			}

			if gitTransientError.Match(buf.Bytes()) {
				time.Sleep(5 * time.Second)
				continue
			}
			return err
		}
	}

	return nil
}

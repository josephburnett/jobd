package taskscript

import (
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1"
)

func TestMarshaling(t *testing.T) {
	program := &Program{
		WorkingDir: "work",
		Tasks: []Item{
			{Task: &v1.InfoTask{Message: "this is a message"}},
			{Task: &v1.InfoTask{Message: "this is another message"}},
			{Task: &unsupportedTask{}},
		},
	}

	script, err := program.Marshal()
	assert.NoError(t, err)

	decoded, err := script.Unmarshal(func() Task { return &v1.InfoTask{} })
	assert.NoError(t, err)
	assert.Equal(t, program, decoded)
}

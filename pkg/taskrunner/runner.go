package taskrunner

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"sync"

	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/environment"
)

type Runner struct {
	opts runnerOptions

	mu       sync.Mutex
	finished bool
	exitCode int
	data     map[string][]string
}

type Result struct {
	ExitCode int
}

func NewRunner(options ...RunnerOption) (*Runner, error) {
	opts, err := newRunnerOptions(options)
	if err != nil {
		return nil, err
	}

	return &Runner{
		opts: opts,
	}, nil
}

func (r *Runner) Run(ctx context.Context, script taskscript.Script, output io.WriteCloser) error {
	program, err := script.Unmarshal(r.opts.TaskFactory...)
	if err != nil {
		return err
	}

	lfmt := logfmt.New(output, logfmt.Config{
		StreamID:         logfmt.StreamWorkLevel,
		InsertTimestamps: true,
	})

	go func() {
		defer func() {
			err := output.Close()
			fmt.Println("Closed runner output trace", err)
		}()

		result := r.execute(ctx, program, lfmt)

		r.mu.Lock()
		defer r.mu.Unlock()

		r.finished = true
		r.exitCode = result.ExitCode
		r.data = make(map[string][]string)
	}()

	return nil
}

func (r *Runner) Status() agent.Status {
	r.mu.Lock()
	defer r.mu.Unlock()

	return agent.Status{
		Finished: r.finished,
		ExitCode: r.exitCode,
		Data:     r.data,
	}
}

func (r *Runner) execute(ctx context.Context, program *taskscript.Program, lfmt *logfmt.Fmt) Result {
	result := Result{}

	env := &environment.Env{
		WorkingDir:          program.WorkingDir,
		CacheDir:            program.CacheDir,
		Timeout:             program.Timeout,
		ID:                  program.ID,
		Token:               program.Token,
		BaseURL:             program.BaseURL,
		Env:                 program.Env,
		Files:               program.Files,
		Stdout:              lfmt.Stdout(),
		Stderr:              lfmt.Stderr(),
		InternalBinDir:      r.opts.InternalBinDir,
		InternalGitExecPath: r.opts.InternalGitExecPath,
	}

	defer lfmt.Close()

	exit := 0
	for _, item := range program.Tasks {
		if item.When == "" {
			item.When = taskscript.WhenOnSuccess
		}

		switch {
		case
			item.When == taskscript.WhenOnFailure && exit != 0,
			item.When == taskscript.WhenOnSuccess && exit == 0,
			item.When == taskscript.WhenAlways:
		default:
			logfmt.SectionSkip(env.Stdout, item.Task.Name())
			continue
		}

		var retcode int
		var err error
		logfmt.Section(env.Stdout, item.Task.Name(), func() int {
			err = item.Task.Run(ctx, env)

			if err != nil {
				logfmt.Error(env.Stderr, err.Error())

				retcode = 1
				var exitCodeError *exec.ExitError
				if errors.As(err, &exitCodeError) {
					retcode = exitCodeError.ExitCode()
				}
			}

			return retcode
		})

		if err != nil {
			if item.OnFailure == taskscript.OnFailureReturn {
				result.ExitCode = retcode
				break
			}

			if item.OnFailure == taskscript.OnFailureBreak {
				result.ExitCode = retcode
				break
			}

			if item.OnFailure == taskscript.OnFailureContinue {
				continue
			}
		}
	}

	return result
}

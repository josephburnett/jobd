package timestamper

import (
	"bytes"
	"io"
	"time"
)

const (
	// Format is a fixed-precision RFC3339 Nano
	Format = "2006-01-02T15:04:05.000000000Z"

	StdoutType StreamType = 'O'
	StderrType StreamType = 'E'

	PartialLineType LineType = '+'
	FullLineType    LineType = ' '

	hextable = "0123456789abcdef"

	bufSize = 8 * 1024
)

type (
	StreamType byte
	LineType   byte
)

var (
	now = func() time.Time {
		return time.Now().UTC()
	}

	lineEscape = []byte("\n")
)

// Logger implements the standard io.Write interface and adds lightweight
// metadata in the form of:
// <date> <stream id><stream type><append flag><message>
//
// Where:
// - <data> is a RFC3339 Nano formatted date
// - <stream id> is a 2-digit hex encoded user provided stream identifier
// - <stream type> is either 'stdout' or 'stderr'
// - <append flag> is either ' ' (no-op) or '+' (append line to last line)
// - <message> is a user provided message.
//
// This format is intended to be well suited to CI/CD logs, where timed output
// can help determine the duration of executed commands.
//
// A new log line is emitted for each new-line character (\n) found within data
// provided to Write().
//
// A new log line is also emitted for the last carriage return (\r) in calls to
// Write() that don't contain a new-line character. Such lines are often used
// to display progress bars, so having them "flushed" to the underlying stream
// can help with live log viewing.
type Logger struct {
	buf bytes.Buffer
	w   io.Writer

	bufStream []byte
	timeLen   int
	timestamp bool
}

func New(w io.Writer, streamType StreamType, streamNumber uint8, timestamp bool) *Logger {
	l := &Logger{
		w:         w,
		timestamp: timestamp,
	}

	if timestamp {
		l.timeLen = len(Format) + 1
	}
	l.bufStream = make([]byte, l.timeLen+4)
	if timestamp {
		l.bufStream[l.timeLen-1] = ' '
	}
	l.bufStream[l.timeLen+0] = hextable[streamNumber>>4]
	l.bufStream[l.timeLen+1] = hextable[streamNumber&0x0f]
	l.bufStream[l.timeLen+2] = byte(streamType)
	l.bufStream[l.timeLen+3] = byte(FullLineType)

	return l
}

func (l *Logger) Write(p []byte) (n int, err error) {
	n, err = l.writeLines(p)
	if err != nil {
		return n, err
	}

	nn, err := l.writeCarriageReturns(p[n:])
	n += nn
	if err != nil {
		return n, err
	}

	nn, err = l.buffer(p[n:])
	n += nn
	return n, err
}

func (l *Logger) buffer(p []byte) (n int, err error) {
	if len(p) == 0 {
		return n, err
	}

	// if we exceed our buffer size, write directly to underlying writer
	if len(p)+l.buf.Len() > bufSize {
		if l.buf.Len() == 0 {
			if err = l.writeHeader(l.w); err != nil {
				return n, err
			}
		}

		_, err := l.w.Write(l.buf.Bytes())
		if err != nil {
			return 0, err
		}
		l.buf.Reset()

		// ensure next write is a continuation
		l.bufStream[l.timeLen+3] = byte(PartialLineType)

		nn, err := l.w.Write(p)
		n += nn
		if err != nil {
			return n, err
		}

		_, err = l.w.Write(lineEscape)
		return n, err
	}

	// start new buffer
	if l.buf.Len() == 0 {
		if err = l.writeHeader(&l.buf); err != nil {
			return n, err
		}

		return l.buf.Write(p)
	}

	// append to existing buffer
	return l.buf.Write(p)
}

func (l *Logger) writeLines(p []byte) (n int, err error) {
	idx := bytes.IndexByte(p, '\n')
	if idx == -1 {
		return n, err
	}

	if l.buf.Len() > 0 {
		_, err := l.w.Write(l.buf.Bytes())
		if err != nil {
			return 0, err
		}

		l.buf.Reset()

		nn, err := l.w.Write(p[:idx+1])
		n += nn
		if err != nil {
			return n, err
		}
	}

	for {
		idx := bytes.IndexByte(p[n:], '\n')
		if idx == -1 {
			return n, err
		}

		if err = l.writeHeader(l.w); err != nil {
			return n, err
		}

		nn, err := l.w.Write(p[n : n+idx+1])
		n += nn
		if err != nil {
			return n, err
		}
	}
}

func (l *Logger) writeCarriageReturns(p []byte) (n int, err error) {
	idx := bytes.LastIndexByte(p, '\r')
	if idx == -1 {
		return n, err
	}

	if l.buf.Len() > 0 {
		_, err := l.w.Write(l.buf.Bytes())
		if err != nil {
			return 0, err
		}

		l.buf.Reset()
	} else {
		if err = l.writeHeader(l.w); err != nil {
			return n, err
		}
	}

	// ensure next write is a continuation
	l.bufStream[l.timeLen+3] = byte(PartialLineType)

	nn, err := l.w.Write(p[n : n+idx+1])
	n += nn
	if err != nil {
		return n, err
	}

	_, err = l.w.Write(lineEscape)
	return n, err
}

func (l *Logger) writeHeader(w io.Writer) error {
	if l.timestamp {
		now().AppendFormat(l.bufStream[:l.timeLen][:0], Format)
	}
	_, err := w.Write(l.bufStream)

	l.bufStream[l.timeLen+3] = byte(FullLineType)

	return err
}

func (l *Logger) Close() error {
	_, err := l.w.Write(l.buf.Bytes())
	return err
}

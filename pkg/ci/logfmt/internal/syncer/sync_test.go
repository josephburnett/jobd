package syncer

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/require"
)

type nopCloser struct {
	w io.Writer
}

func (w *nopCloser) Write(p []byte) (int, error) {
	return w.w.Write(p)
}

func (w *nopCloser) Close() error {
	return nil
}

func TestWrites(t *testing.T) {
	buf := new(bytes.Buffer)

	s := New()

	w1 := s.Writer(buf)
	w2 := s.Writer(buf)
	wc := s.WriteCloser(&nopCloser{buf})

	w1.Write([]byte("one"))
	w2.Write([]byte("two"))
	wc.Write([]byte("three"))
	wc.Close()

	require.Equal(t, "onetwothree", string(buf.Bytes()))
}

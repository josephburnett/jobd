package syncer

import (
	"io"
	"sync"
)

// Syncer synchronizes writers at a new-line or carriage return boundary.
//
// Writes are not buffered, but instead a lock is held by an individual writer
// until it is either closed or a new-line or carriage return in the data
// unblocks all other waiting writers.
//
// This allows, for example, for stdout and stderr to be synchronized even if
// an underlying writer is buffering them.
type Syncer struct {
	mu sync.Mutex
}

type writer struct {
	s *Syncer
	w io.Writer
}

// New returns a new Syncer.
func New() *Syncer {
	return &Syncer{}
}

// Writer wraps a Writer and synchronizes it's writes with other wrapped
// writers.
func (s *Syncer) Writer(w io.Writer) io.Writer {
	return &writer{s: s, w: w}
}

// Writer wraps a WriteCloser and synchronizes it's writes with other wrapped
// writers.
func (s *Syncer) WriteCloser(w io.WriteCloser) io.WriteCloser {
	return &writer{s: s, w: w}
}

func (w *writer) Write(p []byte) (n int, err error) {
	w.s.mu.Lock()
	defer w.s.mu.Unlock()

	return w.w.Write(p)
}

func (w *writer) Close() error {
	wc, ok := w.w.(io.WriteCloser)
	if !ok {
		return nil
	}

	w.s.mu.Lock()
	defer w.s.mu.Unlock()

	return wc.Close()
}

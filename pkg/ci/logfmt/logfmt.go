package logfmt

import (
	"fmt"
	"io"

	logger "gitlab.com/jobd/jobd/pkg/ci/logfmt/internal/logger"
	"gitlab.com/jobd/jobd/pkg/ci/logfmt/internal/syncer"
)

const (
	// StreamExecutorLevel is the stream number for an executor log line
	StreamExecutorLevel = 0
	// StreamWorkLevel is the stream number for a work log line
	StreamWorkLevel = 1
)

// Fmt is a log formatter, adding steam metadata: timestamps, stream IDs and
// stdout/stderr indicators to log lines.
type Fmt struct {
	w      io.Writer
	stdout io.WriteCloser
	stderr io.WriteCloser
}

type Config struct {
	StreamID         uint8
	InsertTimestamps bool
}

// New returns a new log formatter.
func New(w io.Writer, config Config) *Fmt {
	s := syncer.New()

	return &Fmt{
		w:      s.Writer(w),
		stdout: s.WriteCloser(logger.New(w, logger.StdoutType, config.StreamID, config.InsertTimestamps)),
		stderr: s.WriteCloser(logger.New(w, logger.StderrType, config.StreamID, config.InsertTimestamps)),
	}
}

// Stdout returns an io.Writer for metadata-enabled stdout.
func (f *Fmt) Stdout() io.Writer {
	return f.stdout
}

// Stdout returns an io.Writer for metadata-enabled stderr.
func (f *Fmt) Stderr() io.Writer {
	return f.stderr
}

// Writer returns an io.Writer that directly writes to the underlying writer.
func (f *Fmt) Writer() io.Writer {
	return f.w
}

// Close closes the writer.
func (f *Fmt) Close() error {
	errStdout := f.stdout.Close()
	errStderr := f.stderr.Close()

	if errStdout != nil {
		return errStdout
	}
	return errStderr
}

// Log logs a line to stdout with stream metadata.
func (f *Fmt) Log(a ...interface{}) error {
	_, err := fmt.Fprintln(f.stdout, a...)
	return err
}

// Error logs a line to stderr with stream metadata.
func (f *Fmt) Error(a ...interface{}) error {
	_, err := fmt.Fprintln(f.stderr, a...)
	return err
}

// Section opens a named section and ends it.
func Section(w io.Writer, name string, fn func() int) error {
	_, openErr := fmt.Fprintf(w, "[section_open name=%q]\n", name)
	exitCode := fn()
	_, closeErr := fmt.Fprintf(w, "[section_close name=%q code=%d]\n", name, exitCode)

	if openErr != nil {
		return fmt.Errorf("writing section open: %w", openErr)
	}
	if closeErr != nil {
		return fmt.Errorf("writing section close: %w", closeErr)
	}

	return nil
}

func SectionSkip(w io.Writer, name string) error {
	_, err := fmt.Fprintf(w, "[section_skip name=%q]\n", name)
	if err != nil {
		return fmt.Errorf("writing section skip: %w", err)
	}

	return nil
}

func Command(w io.Writer, a ...any) error {
	_, err := fmt.Fprintf(w, "[command] %s", fmt.Sprintln(a...))
	return err
}

func Error(w io.Writer, a ...any) error {
	_, err := fmt.Fprintf(w, "[error] %s", fmt.Sprintln(a...))
	return err
}

func Notice(w io.Writer, a ...any) error {
	_, err := fmt.Fprintf(w, "[notice] %s", fmt.Sprintln(a...))
	return err
}

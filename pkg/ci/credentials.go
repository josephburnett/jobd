package ci

// JobCredentials is an interface representing a job's set of credentials.
type JobCredentials interface {
	GetID() int64
	GetToken() string
	GetBaseURL() string
}

// Credentials is a basic concrete type implementing the JobCredentials
// interface.
type Credentials struct {
	ID      int64
	Token   string
	BaseURL string
}

// CredentialsFromJobCredentials converts a type implementing job credentials
// into the basic Credentials concrete type.
func CredentialsFromJobCredentials(creds JobCredentials) Credentials {
	if creds == nil {
		return Credentials{}
	}

	if creds, ok := creds.(Credentials); ok {
		return creds
	}

	return Credentials{
		ID:      creds.GetID(),
		Token:   creds.GetToken(),
		BaseURL: creds.GetBaseURL(),
	}
}

func (c Credentials) GetID() int64 {
	return c.ID
}

func (c Credentials) GetToken() string {
	return c.Token
}

func (c Credentials) GetBaseURL() string {
	return c.BaseURL
}

package artifactclient

import (
	"context"
	"crypto/x509"
	"io"
	"net"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci/client"
	"gitlab.com/jobd/jobd/pkg/ci/client/apitest"
	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

func TestArtifactTransfer(t *testing.T) {
	srv := apitest.New()
	defer srv.Close()

	topts := []transport.Option{
		transport.WithCACerts(func() []*x509.Certificate {
			return []*x509.Certificate{srv.Certificate()}
		}),
	}

	gc, err := client.New(srv.RegistrationToken(), srv.URL(), client.WithTransportOptions(topts...))
	require.NoError(t, err)
	require.NoError(t, gc.Register(context.Background()))

	srv.QueueJob(spec.Spec{})

	var meta spec.Spec
	_, err = gc.RequestJob(context.Background(), &meta)
	require.NoError(t, err)

	ac, err := New(meta.JobCredentials(), WithTransportOptions(topts...))
	require.NoError(t, err)

	info := UploadInfo{
		Name:     "foobar",
		ExpireIn: "1 week",
		Format:   "zip",
		Type:     "archive",
	}

	t.Run("upload", func(t *testing.T) {
		err := ac.Upload(context.Background(), info, func(w io.Writer) error {
			_, err := w.Write([]byte("foobar"))
			return err
		})
		require.NoError(t, err)

		a := srv.Job(meta.ID).Artifact("foobar.zip")
		assert.Equal(t, []byte("foobar"), a.Content)
	})

	t.Run("download", func(t *testing.T) {
		rc, err := ac.Download(context.Background(), DownloadInfo{ID: meta.ID, Token: meta.Token})
		require.NoError(t, err)

		buf, err := io.ReadAll(rc)
		require.NoError(t, err)

		assert.Equal(t, []byte("foobar"), buf)
		assert.NoError(t, rc.Close())
	})

	t.Run("invalid artifact", func(t *testing.T) {
		var rerr *transport.ResponseError

		_, err := ac.Download(context.Background(), DownloadInfo{ID: 5000, Token: meta.Token})
		require.ErrorAs(t, err, &rerr)
		require.NotNil(t, rerr.Response)
		assert.Equal(t, http.StatusForbidden, rerr.Response.StatusCode)
	})

	t.Run("upload redirection", func(t *testing.T) {
		srv.Hijack(func(rw http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodPost {
				buf := make([]byte, 64*1024)
				_, _ = r.Body.Read(buf)

				http.Redirect(rw, r, r.URL.String(), http.StatusTemporaryRedirect)
				rw.(http.Flusher).Flush()

				r.Context().Value(apitest.ClientConnContextKey).(net.Conn).Close()
			}
		})

		err := ac.Upload(context.Background(), info, func(w io.Writer) error {
			_, err := w.Write([]byte("foobar"))
			return err
		})
		require.NoError(t, err)

		a := srv.Job(meta.ID).Artifact("foobar.zip")
		assert.Equal(t, []byte("foobar"), a.Content)
	})
}

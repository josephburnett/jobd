package artifactclient

import (
	"context"
	"crypto/rand"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/client/internal/base"
)

// Client is a client for uploading and downloading artifacts to and from a
// GitLab instance.
type Client struct {
	base  *base.BaseClient
	creds ci.Credentials
}

const pathJobArtifacts = "/api/v4/jobs/%d/artifacts"

type UploadInfo struct {
	Name     string
	ExpireIn string
	Format   string
	Type     string
}

type DownloadInfo struct {
	ID    int64
	Token string
}

// New returns a new Artifact client.
func New(creds ci.JobCredentials, options ...Option) (*Client, error) {
	opts, err := newOptions(options)
	if err != nil {
		return nil, fmt.Errorf("configuring options: %w", err)
	}

	return &Client{
		base:  base.New(opts.TransportOptions.NewHTTPClient(), opts.Logger),
		creds: ci.CredentialsFromJobCredentials(creds),
	}, nil
}

// Uploads uploads an artifact. The bodyFn is called so that the caller can
// provide the data for the artifact. In some cases (such as in the case of HTTP
// redirections or retryable errors), bodyFn can be called more than once. The
// caller should provide the complete data for the artifact for each call to
// bodyFn.
func (c *Client) Upload(ctx context.Context, info UploadInfo, bodyFn func(io.Writer) error) error {
	u, err := url.Parse(c.creds.GetBaseURL() + fmt.Sprintf(pathJobArtifacts, c.creds.GetID()))
	if err != nil {
		return fmt.Errorf("parsing upload URL: %w", err)
	}

	q := u.Query()
	q.Set("expire_in", info.ExpireIn)
	q.Set("artifact_format", info.Format)
	q.Set("artifact_type", info.Type)
	u.RawQuery = q.Encode()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, u.String(), nil)
	if err != nil {
		return err
	}

	boundary, getBody := newReusableMultipartWriter(info, bodyFn)
	req.Body, _ = getBody()
	req.GetBody = getBody
	req.Header.Set(base.HeaderJobToken, c.creds.GetToken())
	req.Header.Set(base.HeaderContentType, "multipart/form-data; boundary="+boundary)

	resp, err := c.base.Do(req)
	if err != nil {
		return err
	}
	defer base.CloseBody(resp.Body)

	return nil
}

// Download downloads an artifact dependency with the provided job credentials.
func (c *Client) Download(ctx context.Context, info DownloadInfo) (io.ReadCloser, error) {
	url := c.creds.GetBaseURL() + fmt.Sprintf(pathJobArtifacts, info.ID)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(base.HeaderJobToken, info.Token)

	resp, err := c.base.Do(req)
	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}

// newReusableMultipartWriter provides a http GetBody() callback so that
// failures or redirects can reset the data to be POSTed.
func newReusableMultipartWriter(info UploadInfo, fn func(io.Writer) error) (string, func() (io.ReadCloser, error)) {
	var buf [30]byte
	_, err := io.ReadFull(rand.Reader, buf[:])
	if err != nil {
		panic(err)
	}
	boundary := fmt.Sprintf("%x", buf[:])

	extension := ".zip"
	switch info.Format {
	case "raw":
		extension = ""
	case "gzip":
		extension = ".gz"
	}

	return boundary, func() (io.ReadCloser, error) {
		pr, pw := io.Pipe()

		go func() {
			mw := multipart.NewWriter(pw)
			if err := mw.SetBoundary(boundary); err != nil {
				pw.CloseWithError(err)
				return
			}

			fw, err := mw.CreateFormFile("file", info.Name+extension)
			if err != nil {
				pw.CloseWithError(err)
				return
			}

			if err := fn(fw); err != nil {
				pw.CloseWithError(err)
				return
			}

			pw.CloseWithError(mw.Close())
		}()

		return pr, nil
	}
}

package artifactclient

import (
	"fmt"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
)

type options struct {
	Logger           hclog.Logger
	TransportOptions transport.Options
}

// Option is an option used to configure the artifact client.
type Option func(*options) error

func newOptions(opts []Option) (options, error) {
	var options options

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.Logger == nil {
		options.Logger = hclog.Default()
	}

	return options, nil
}

func WithTransportOptions(opts ...transport.Option) Option {
	return func(o *options) error {
		opts, err := transport.NewOptions(opts)
		if err != nil {
			return fmt.Errorf("artifact transport options: %w", err)
		}

		o.TransportOptions = opts
		return nil
	}
}

func WithLogger(logger hclog.Logger) Option {
	return func(o *options) error {
		o.Logger = logger
		return nil
	}
}

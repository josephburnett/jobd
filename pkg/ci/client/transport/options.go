package transport

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"
	"time"
)

type Options struct {
	CACertsFn           func() []*x509.Certificate
	ClientCertificateFn func() *tls.Certificate
}

// Option is an option used for configuring a client's underlying transport.
type Option func(*Options) error

func NewOptions(opts []Option) (Options, error) {
	var options Options

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	return options, nil
}

// DefaultHTTPClient returns the default HTTP client used if it is not
// specified as an option.
func (opts Options) NewHTTPClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			Proxy:                 http.ProxyFromEnvironment,
			MaxIdleConns:          100,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			ResponseHeaderTimeout: 10 * time.Minute,
			ForceAttemptHTTP2:     true,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify:   true, // not skipped, but uses custom verficiation
				VerifyConnection:     opts.verify,
				GetClientCertificate: opts.getClientCertificate,
			},
		},
	}
}

func (opts Options) verify(cs tls.ConnectionState) error {
	roots, _ := x509.SystemCertPool()
	if roots == nil {
		x509.NewCertPool()
	}

	vopts := x509.VerifyOptions{
		DNSName:       cs.ServerName,
		Intermediates: x509.NewCertPool(),
		Roots:         roots,
	}

	for _, cert := range cs.PeerCertificates[1:] {
		vopts.Intermediates.AddCert(cert)
	}

	// From go1.18, certificate verification attempts to verify using both
	// Go's chain verification and fallbacks to the system verifier if that
	// fails. However, when the system verifier is used, none of the custom
	// certs provided are taken into account. This can be some what resolved by
	// adding them to both the Roots and Intermediate pools.
	//
	// When Go's verification is used, the user provided certificates will be
	// treated as trust-anchors (added to Roots).
	//
	// When the system's verifier is used, the user provided certificates will
	// be treated as intermediate certificates (added to Intermediates), they
	// won't be treated as trust-anchors, but can potentially help fill in gaps
	// to a complete chain where a trust-anchor is present on the system.
	if opts.CACertsFn != nil {
		for _, cert := range opts.CACertsFn() {
			vopts.Roots.AddCert(cert)
			vopts.Intermediates.AddCert(cert)
		}
	}

	_, err := cs.PeerCertificates[0].Verify(vopts)
	return err
}

func (opts Options) getClientCertificate(cri *tls.CertificateRequestInfo) (*tls.Certificate, error) {
	if opts.ClientCertificateFn != nil {
		cert := opts.ClientCertificateFn()

		if cri.SupportsCertificate(cert) == nil {
			return cert, nil
		}
	}

	return new(tls.Certificate), nil
}

// WithClientCertificate provides a callback for returning a TLS client
// certificate each time a new client connection is made.
func WithClientCertificate(fn func() *tls.Certificate) Option {
	return func(o *Options) error {
		o.ClientCertificateFn = fn
		return nil
	}
}

// WithCACerts provides a callback for returning CA certs each time a
// new client connection is made.
func WithCACerts(fn func() []*x509.Certificate) Option {
	return func(o *Options) error {
		o.CACertsFn = fn
		return nil
	}
}

package transport

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

// ResponseError is an error type that is returned when there is an issue
// calling the remote server. It contains the http.Response responsible for
// the error and the error payload provided by the server.
type ResponseError struct {
	Response *http.Response
	Msg      ErrorResponseMessage `json:"message"`
}

type ErrorResponseMessage string

func (e *ErrorResponseMessage) UnmarshalJSON(data []byte) error {
	type simple ErrorResponseMessage
	err := json.Unmarshal(data, (*simple)(e))
	if err == nil {
		return nil
	}

	var complex map[string][]interface{}
	err = json.Unmarshal(data, &complex)
	if err != nil {
		// explicitly ignore error, we can't decode this type
		return nil
	}

	messages := make([]string, 0, len(complex))
	for key, val := range complex {
		values := make([]string, 0, len(val))
		for _, msg := range val {
			values = append(values, fmt.Sprintf("%v", msg))
		}
		messages = append(messages, fmt.Sprintf("%s: %s", key, strings.Join(values, "; ")))
	}

	*e = ErrorResponseMessage(strings.Join(messages, ", "))
	return nil
}

func (r *ResponseError) Error() string {
	return fmt.Sprintf("%v %v: %d %v %v",
		r.Response.Request.Method, r.Response.Request.URL,
		r.Response.StatusCode, http.StatusText(r.Response.StatusCode), r.Msg)
}

package base

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
)

const (
	HeaderJobToken     = "Job-Token"
	HeaderContentType  = "Content-Type"
	HeaderContentRange = "Content-Range"
	HeaderAccept       = "Accept"

	apiMediaType  = "application/json"
	maxRetryCount = 10
)

// BaseClient is a base client for the gitlab runner, artifacts and job clients.
type BaseClient struct {
	client *http.Client
	logger hclog.Logger
}

func New(client *http.Client, logger hclog.Logger) *BaseClient {
	if logger == nil {
		logger = hclog.Default()
	}

	return &BaseClient{
		client: client,
		logger: logger,
	}
}

// Do is similar to http.Client.Do() but with the following differences:
// - It attempts to retry a request if the request is deemed recoverable.
// - It returns an error for "bad" HTTP responses.
//
// A http.Response can still be returned on an error, but it's body will always
// have been read and closed.
func (c *BaseClient) Do(req *http.Request) (*http.Response, error) {
	var (
		resp     *http.Response
		err      error
		attempts int
	)

	for attempts = 1; attempts <= maxRetryCount; attempts++ {
		start := time.Now()
		resp, err = c.do(req)

		logger := c.logger
		if resp != nil {
			logger.With("code", resp.StatusCode)
		}

		logger.Trace("http request", "method", req.Method,
			"host", req.URL.Host, "path", req.URL.Path, "attempt", attempts,
			"took", time.Since(start), "err", err)

		var recoverable *errRecoverableNetwork
		if errors.As(err, &recoverable) {
			if req.GetBody != nil {
				req.Body, err = req.GetBody()
				if err != nil {
					return resp, fmt.Errorf("body recovery: %w", err)
				}
			}

			// if a rate limit header was found, use that, otherwise, use an
			// exponential backoff based on attempts.
			if recoverable.limit > 0 {
				time.Sleep(recoverable.limit)
			} else {
				time.Sleep(backoffDelay(attempts))
			}

			continue
		}

		if err != nil {
			return resp, err
		}
		break
	}

	if err != nil {
		return resp, fmt.Errorf("do retry (%d attempts): %w", attempts, err)
	}

	return resp, nil
}

// API wraps Do() but encodes and decodes requests and responses with JSON.
func (c *BaseClient) API(ctx context.Context, method, url string, request, response interface{}) (*http.Response, error) {
	var buf []byte
	var err error
	if request != nil {
		buf, err = json.Marshal(request)
		if err != nil {
			return nil, fmt.Errorf("marshaling request: %w", err)
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, url, bytes.NewReader(buf))
	if err != nil {
		return nil, fmt.Errorf("creating request: %w", err)
	}

	req.GetBody = func() (io.ReadCloser, error) {
		return io.NopCloser(bytes.NewReader(buf)), nil
	}
	req.Header.Set(HeaderAccept, apiMediaType)
	if request != nil {
		req.Header.Set(HeaderContentType, apiMediaType)
	}

	resp, err := c.Do(req)
	if err != nil {
		return resp, fmt.Errorf("api request: %w", err)
	}
	defer CloseBody(resp.Body)

	if response != nil {
		if err := json.NewDecoder(resp.Body).Decode(response); err != nil {
			return resp, fmt.Errorf("unmarshaling %q body: %w", url, err)
		}
	}

	return resp, nil
}

func (c *BaseClient) do(req *http.Request) (*http.Response, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		var uerr *url.Error
		if errors.As(err, &uerr) {
			if !uerr.Temporary() {
				return nil, err
			}
		}

		return nil, &errRecoverableNetwork{err, 0}
	}

	if err = c.checkResponse(resp); err != nil {
		CloseBody(resp.Body)
		return resp, err
	}

	return resp, nil
}

func (c *BaseClient) checkResponse(r *http.Response) error {
	if r.StatusCode >= 200 && r.StatusCode <= 299 {
		return nil
	}

	err := &transport.ResponseError{Response: r}
	_ = json.NewDecoder(io.LimitReader(r.Body, 4*1024)).Decode(err)

	switch {
	case r.StatusCode >= 500 && r.StatusCode != 501, r.StatusCode == http.StatusPreconditionFailed:
		return &errRecoverableNetwork{err, 0}

	case r.StatusCode == http.StatusTooManyRequests:
		resetTimeRaw := r.Header.Get("RateLimit-ResetTime")
		resetTime, resetTimeErr := time.Parse(time.RFC1123, resetTimeRaw)
		if resetTimeErr != nil {
			c.logger.Warn("invalid rate limit header", "value", resetTimeRaw)

			// retries have an exponential backoff, so that mechanism can be
			// replied upon if we're unable to parse the rate limit correctly.
			return &errRecoverableNetwork{err, 0}
		}

		return &errRecoverableNetwork{err, time.Until(resetTime)}
	}

	return err
}

// errRecoverableNetwork is the error returned when a network error was
// encountered, but it should be retryable.
type errRecoverableNetwork struct {
	err   error
	limit time.Duration
}

func (err *errRecoverableNetwork) Is(target error) bool {
	_, ok := target.(*errRecoverableNetwork)
	return ok
}

func (err *errRecoverableNetwork) Error() string {
	return err.err.Error()
}

func (err *errRecoverableNetwork) Unwrap() error {
	return err.err
}

// CloseBody can be used to close the http.Response body.
func CloseBody(body io.ReadCloser) {
	_, _ = io.Copy(io.Discard, io.LimitReader(body, 32*1024))
	body.Close()
}

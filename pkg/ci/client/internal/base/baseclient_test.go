package base

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/require"
)

func TestUnretryableURLError(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/retry", nil)
	require.NoError(t, err)

	client := New(http.DefaultClient, nil)

	_, err = client.Do(req)
	require.Error(t, err)
}

func TestDoRetries(t *testing.T) {
	hclog.Default().SetLevel(hclog.Trace)

	tests := map[int]bool{
		http.StatusForbidden:                    false,
		http.StatusRequestedRangeNotSatisfiable: false,
		http.StatusNotFound:                     false,
		http.StatusInternalServerError:          true,
		http.StatusBadGateway:                   true,
		http.StatusServiceUnavailable:           true,
		http.StatusGatewayTimeout:               true,
	}

	for status, retries := range tests {
		t.Run(http.StatusText(status), func(t *testing.T) {
			count := 0

			mux := http.NewServeMux()
			mux.HandleFunc("/retry", func(w http.ResponseWriter, r *http.Request) {
				buf, err := io.ReadAll(r.Body)
				require.NoError(t, err)
				require.Equal(t, []byte("this is a test"), buf)

				if count == 0 {
					http.Error(w, http.StatusText(status), status)
				}
				count++
			})

			srv := httptest.NewServer(mux)
			defer srv.Close()

			req, err := http.NewRequest(http.MethodPost, srv.URL+"/retry", strings.NewReader("this is a test"))
			require.NoError(t, err)

			client := New(srv.Client(), nil)

			_, err = client.Do(req)
			if retries {
				require.NoError(t, err)
			} else {
				require.Error(t, err)
			}
		})
	}
}

func TestRateLimit(t *testing.T) {
	count := 0
	var start time.Time

	mux := http.NewServeMux()
	mux.HandleFunc("/limit", func(w http.ResponseWriter, r *http.Request) {
		switch count {
		case 0:
			start = time.Now()
			w.Header().Add("RateLimit-ResetTime", time.Now().Add(3*time.Second).Format(time.RFC1123))
			http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)

		case 1:
			require.GreaterOrEqual(t, time.Since(start), 1*time.Second)

			w.Header().Add("RateLimit-ResetTime", "invalid")
			http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)

		case 2:
			require.GreaterOrEqual(t, time.Since(start), 1*time.Second)
		}
		count++
	})

	srv := httptest.NewServer(mux)
	defer srv.Close()

	req, err := http.NewRequest(http.MethodGet, srv.URL+"/limit", nil)
	require.NoError(t, err)

	client := New(http.DefaultClient, nil)

	_, err = client.Do(req)
	require.NoError(t, err)
	require.Equal(t, 3, count)
}

func TestAPI(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, apiMediaType, r.Header.Get(HeaderAccept))

		var m map[string]string
		json.NewDecoder(r.Body).Decode(&m)
		json.NewEncoder(w).Encode(m)
	})

	srv := httptest.NewServer(mux)
	defer srv.Close()

	encode := map[string]string{
		"hello": "world",
		"foo":   "bar",
	}
	var decode map[string]string

	client := New(http.DefaultClient, nil)
	_, err := client.API(context.Background(), http.MethodGet, srv.URL+"/api", encode, &decode)
	require.NoError(t, err)
	require.Equal(t, encode, decode)
}

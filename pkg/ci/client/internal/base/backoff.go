package base

import (
	"math/rand"
	"sync"
	"time"
)

const (
	min    = float64(time.Second)
	max    = float64(2 * time.Minute)
	factor = 1.6
	jitter = 0.2
)

var (
	mu     sync.Mutex
	random = rand.New(rand.NewSource(time.Now().UnixNano()))
)

func backoffDelay(retries int) time.Duration {
	backoff := min
	for i := 0; i < retries; i++ {
		backoff *= factor
		if backoff > max {
			backoff = max
			break
		}
	}

	mu.Lock()
	defer mu.Unlock()

	return time.Duration(backoff*1 + jitter*(random.Float64()*2-1))
}

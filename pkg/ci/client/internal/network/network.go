package network

// VersionInfo contains information about the runner's version, supported
// features and executor used.
type VersionInfo struct {
	RunnerInfo
	Features FeaturesInfo `json:"features"`
}

type RunnerInfo struct {
	Name         string `json:"name,omitempty"`
	Version      string `json:"version,omitempty"`
	Revision     string `json:"revision,omitempty"`
	Platform     string `json:"platform,omitempty"`
	Architecture string `json:"architecture,omitempty"`
	Executor     string `json:"executor,omitempty"`
	Shell        string `json:"shell,omitempty"`
}

// FeaturesInfo contains information about what features are supported.
type FeaturesInfo struct {
	Variables               bool `json:"variables"`
	Artifacts               bool `json:"artifacts"`
	Cache                   bool `json:"cache"`
	UploadMultipleArtifacts bool `json:"upload_multiple_artifacts"`
	UploadRawArtifacts      bool `json:"upload_raw_artifacts"`
	Terminal                bool `json:"terminal"`
	Refspecs                bool `json:"refspecs"`
	Masking                 bool `json:"masking"`
	RawVariables            bool `json:"raw_variables"`
	ArtifactsExclude        bool `json:"artifacts_exclude"`
	MultiBuildSteps         bool `json:"multi_build_steps"`
	TraceReset              bool `json:"trace_reset"`
	TraceChecksum           bool `json:"trace_checksum"`
	TraceSize               bool `json:"trace_size"`
	VaultSecrets            bool `json:"vault_secrets"`
	Cancelable              bool `json:"cancelable"`
	ReturnExitCode          bool `json:"return_exit_code"`
	ServiceVariables        bool `json:"service_variables"`

	Image    bool `json:"image"`
	Services bool `json:"services"`
	Shared   bool `json:"shared"`
	Proxy    bool `json:"proxy"`
	Session  bool `json:"session"`
}

type RunnerOptions struct {
	Description     string `json:"description,omitempty"`
	MaintenanceNote string `json:"maintenance_note,omitempty"`
	Tags            string `json:"tag_list,omitempty"`
	RunUntagged     bool   `json:"run_untagged"`
	Locked          bool   `json:"locked"`
	MaximumTimeout  int    `json:"maximum_timeout,omitempty"`
	AccessLevel     string `json:"access_level,omitempty"`
	Active          bool   `json:"active"`
}

type RegisterRequest struct {
	RunnerOptions
	Info  VersionInfo `json:"info"`
	Token string      `json:"token"`
}

type RegisterResponse struct {
	Token string `json:"token"`
}

type UnregisterRequest struct {
	Token string `json:"token"`
}

type RequestJobRequest struct {
	Info       VersionInfo              `json:"info"`
	Token      string                   `json:"token"`
	LastUpdate string                   `json:"last_update"`
	Session    RequestJobRequestSession `json:"session"`
}

type RequestJobRequestSession struct {
	URL           string `json:"url,omitempty"`
	Certificate   string `json:"certificate,omitempty"`
	Authorization string `json:"authorization,omitempty"`
}

type TracePingRequest struct {
	Info          VersionInfo `json:"info"`
	Token         string      `json:"token"`
	State         string      `json:"state"`
	FailureReason string      `json:"failure_reason,omitempty"`
	ExitCode      int         `json:"exit_code,omitempty"`

	Checksum string                 `json:"checksum,omitempty"` // deprecated
	Output   TracePingRequestOutput `json:"output,omitempty"`
}

type TracePingRequestOutput struct {
	Checksum string `json:"checksum"`
	Bytesize int64  `json:"bytesize"`
}

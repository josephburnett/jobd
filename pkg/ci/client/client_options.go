package client

import (
	"fmt"
	"runtime"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci/client/internal/network"
	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
)

type options struct {
	Logger           hclog.Logger
	VersionInfo      VersionInfo
	TransportOptions transport.Options
}

// Option is an option used to configure the client.
type Option func(*options) error

func newOptions(opts []Option) (options, error) {
	var options options

	options.VersionInfo.Name = "jobd-client"
	options.VersionInfo.Platform = runtime.GOOS
	options.VersionInfo.Architecture = runtime.GOARCH
	options.VersionInfo.Features = network.FeaturesInfo{
		TraceReset:     true,
		TraceChecksum:  true,
		TraceSize:      true,
		ReturnExitCode: true,
	}

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.Logger == nil {
		options.Logger = hclog.Default()
	}

	return options, nil
}

func WithVersionInfoUpdater(fn func(info *VersionInfo)) Option {
	return func(o *options) error {
		fn(&o.VersionInfo)
		return nil
	}
}

func WithTransportOptions(opts ...transport.Option) Option {
	return func(o *options) error {
		opts, err := transport.NewOptions(opts)
		if err != nil {
			return fmt.Errorf("transport options: %w", err)
		}

		o.TransportOptions = opts
		return nil
	}
}

func WithLogger(logger hclog.Logger) Option {
	return func(o *options) error {
		o.Logger = logger
		return nil
	}
}

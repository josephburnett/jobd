package jobclient

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
)

type options struct {
	Logger           hclog.Logger
	TraceReadWriter  func(id int64) (ReadAtWriterCloser, error)
	TransportOptions transport.Options
}

// Option is an option used to configure the job client.
type Option func(*options) error

func newOptions(opts []Option) (options, error) {
	var options options

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.TraceReadWriter == nil {
		options.TraceReadWriter = defaultTraceReadWriter
	}

	if options.Logger == nil {
		options.Logger = hclog.Default()
	}

	return options, nil
}

func WithTransportOptions(opts ...transport.Option) Option {
	return func(o *options) error {
		opts, err := transport.NewOptions(opts)
		if err != nil {
			return fmt.Errorf("artifact transport options: %w", err)
		}

		o.TransportOptions = opts
		return nil
	}
}

func WithLogger(logger hclog.Logger) Option {
	return func(o *options) error {
		o.Logger = logger
		return nil
	}
}

func WithReadWriter(rw func(id int64) (ReadAtWriterCloser, error)) Option {
	return func(o *options) error {
		o.TraceReadWriter = rw
		return nil
	}
}

// ReadAtWriterCloser is the interface that groups the basic io.ReaderAt,
// io.WriterAt and io.Close methods.
type ReadAtWriterCloser interface {
	io.ReaderAt
	io.Writer
	io.Closer
}

// defaultTraceReadWriter is the default ReadWriterAtCloser implementation. It
// opens a file in the system's temporary directory. It is deleted when closed.
func defaultTraceReadWriter(jobID int64) (ReadAtWriterCloser, error) {
	f, err := ioutil.TempFile("", "jobd")
	if err != nil {
		return nil, fmt.Errorf("creating trace file: %w", err)
	}

	return &rmcloser{f}, err
}

type rmcloser struct {
	*os.File
}

func (c *rmcloser) Close() error {
	if err := c.File.Close(); err != nil {
		return err
	}
	return os.Remove(c.Name())
}

package jobclient

import (
	"bytes"
	"context"
	"crypto/x509"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci/client"
	"gitlab.com/jobd/jobd/pkg/ci/client/apitest"
	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

func TestFinalUpdate(t *testing.T) {
	tests := map[string]error{
		"no error":       nil,
		"script failure": ErrScriptFailure,
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			srv := apitest.New()
			defer srv.Close()

			topts := []transport.Option{
				transport.WithCACerts(func() []*x509.Certificate {
					return []*x509.Certificate{srv.Certificate()}
				}),
			}

			gc, err := client.New(srv.RegistrationToken(), srv.URL(), client.WithTransportOptions(topts...))
			require.NoError(t, err)
			require.NoError(t, gc.Register(context.Background()))

			srv.QueueJob(spec.Spec{})

			var meta spec.Spec
			_, err = gc.RequestJob(context.Background(), &meta)
			require.NoError(t, err)

			jc, err := New(meta.JobCredentials(), gc.VersionInfo(), WithTransportOptions(topts...))
			require.NoError(t, err)

			content := bytes.Repeat([]byte("final message"), MaxTracePatchSize)
			_, err = jc.Write(content)
			require.NoError(t, err)

			status := "success"
			if tc != nil {
				jc.Fail(1, tc)
				status = "failed"
			}

			simulateError := func(rw http.ResponseWriter, r *http.Request) {
				rw.WriteHeader(http.StatusInternalServerError)
			}

			// hijack the next 2 requests and simulate an internal server error
			srv.Hijack(simulateError)
			srv.Hijack(simulateError)

			require.NoError(t, jc.Close())

			assert.Equal(t, status, srv.Job(meta.ID).State())
			if tc != nil {
				assert.Equal(t, tc.Error(), srv.Job(meta.ID).FailureReason())
			}
			assert.Equal(t, content, srv.Job(meta.ID).Trace())
			assert.Equal(t, "crc32:8214176b", srv.Job(meta.ID).Checksum())
		})
	}
}

func TestRemoteState(t *testing.T) {
	srv := apitest.New()
	defer srv.Close()

	topts := []transport.Option{
		transport.WithCACerts(func() []*x509.Certificate {
			return []*x509.Certificate{srv.Certificate()}
		}),
	}

	gc, err := client.New(srv.RegistrationToken(), srv.URL(), client.WithTransportOptions(topts...))
	require.NoError(t, err)
	require.NoError(t, gc.Register(context.Background()))

	srv.QueueJob(spec.Spec{})

	var meta spec.Spec
	_, err = gc.RequestJob(context.Background(), &meta)
	require.NoError(t, err)

	srv.Job(meta.ID).Watch(true)

	jc, err := New(meta.JobCredentials(), gc.VersionInfo(), WithTransportOptions(topts...))
	assert.NoError(t, err)

	remoteState, err := jc.Wait()
	require.NoError(t, err)
	assert.Equal(t, RemoteStateRunning, remoteState)
	assert.Empty(t, srv.Job(meta.ID).Trace())

	srv.Job(meta.ID).Cancel()

	_, err = jc.Write([]byte("This is a simple trace message\n"))
	require.NoError(t, err)

	remoteState, err = jc.Wait()
	var target *transport.ResponseError
	require.ErrorAs(t, err, &target)
	assert.Equal(t, http.StatusForbidden, target.Response.StatusCode)
	assert.Equal(t, RemoteStateCanceled, remoteState)

	err = jc.Close()
	require.ErrorAs(t, err, &target)
	assert.Equal(t, http.StatusForbidden, target.Response.StatusCode)
	assert.Empty(t, srv.Job(meta.ID).Trace())

	remoteState, err = jc.Wait()
	require.ErrorIs(t, err, ErrClientIsClosed)
	assert.Equal(t, RemoteStateCanceled, remoteState)
}

package jobclient

import (
	"context"
	"errors"
	"fmt"
	"hash"
	"hash/crc32"
	"io"
	"net/http"
	"sync"
	"time"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/client"
	"gitlab.com/jobd/jobd/pkg/ci/client/internal/base"
	"gitlab.com/jobd/jobd/pkg/ci/client/internal/network"
)

// Client communicates to GitLab in relation to a specific job.
//
// Close() should be called once a job has reached the end of its execution.
// The sets the state of the job to 'Success' or 'Failed' if Fail() was called
// during the execution.
//
// Wait() can also be used to monitor the remote state of the job. A job's
// state can be modified out-of-band, for example, if a user were to cancel the
// job via the API or Web interfaces.
//
// The jobclient implements io.Writer, allowing for log output to be streamed to
// GitLab. Wait() needs to be called for this data to be periodically flushed.
type Client struct {
	base  *base.BaseClient
	creds ci.Credentials
	rw    ReadAtWriterCloser // log read/writer
	opts  options

	mu                sync.Mutex
	updateInterval    time.Duration // update interval as set by GitLab
	remoteState       RemoteState   // remote state of job as set by GitLab
	remoteStateChange bool          // tracks whether the remote state has changed since the last time we checked

	err      error // stores the error state set by Fail()
	exitCode int   // stores the exit code set by Fail()

	requestLock sync.Mutex // prevents regular ping/flush logic whilst performing final flush
	closed      bool

	nextFlush time.Time // the next time we flush (now() + updateInterval)
	nextPing  time.Time // the next time we ping/force flush (now() + updateInterval * nextPingUpdateMultiplier)

	written  int64 // bytes written to trace log
	uploaded int64 // bytes uploaded from trace log
	checksum hash.Hash32
	complete bool

	info network.VersionInfo
}

// ErrScriptFailure, ErrRunnerSystemFailure, ErrJobExecutionTimeout and
// ErrUnknownFailure can be used with Fail() to notify the remote server why a
// build failed.
var (
	ErrScriptFailure       = errors.New("script_failure")
	ErrRunnerSystemFailure = errors.New("runner_system_failure")
	ErrJobExecutionTimeout = errors.New("job_execution_timeout")
	ErrUnknownFailure      = errors.New("unknown_failure")
)

var (
	ErrClientIsClosed = errors.New("client is closed")
)

type RemoteState string

// Remote states a job can be in. Canceled is recieved if the user cancels the
// job via the GitLab UI. Canceling is not yet supported by GitLab.com, so can
// be removed if never implemented.
const (
	RemoteStateRunning   RemoteState = "running"
	RemoteStateCanceled  RemoteState = "canceled"
	RemoteStateCanceling RemoteState = "canceling"
)

type state string

const (
	stateRunning state = "running"
	stateSuccess state = "success"
	stateFailed  state = "failed"
)

const (
	MaxTracePatchSize        = 1024 * 1024
	DefaultUpdateInterval    = 5 * time.Second
	nextPingUpdateMultiplier = 4

	headerJobStatus                 = "Job-Status"
	headerGitlabTraceUpdateInterval = "X-Gitlab-Trace-Update-Interval"

	pathJob      = "/api/v4/jobs/%d"
	pathJobTrace = "/api/v4/jobs/%d/trace"
)

// New returns a new Trace.
//
// Trace implements the standard io.Write and io.Close methods.
//
// Calling Close() will also flush any remaining data.
func New(creds ci.JobCredentials, info client.VersionInfo, options ...Option) (*Client, error) {
	opts, err := newOptions(options)
	if err != nil {
		return nil, fmt.Errorf("configuring options: %w", err)
	}

	rw, err := opts.TraceReadWriter(creds.GetID())
	if err != nil {
		return nil, err
	}

	opts.Logger = opts.Logger.With("job_id", creds.GetID())

	return &Client{
		base:              base.New(opts.TransportOptions.NewHTTPClient(), opts.Logger),
		creds:             ci.CredentialsFromJobCredentials(creds),
		opts:              opts,
		rw:                rw,
		updateInterval:    DefaultUpdateInterval,
		remoteState:       RemoteStateRunning,
		remoteStateChange: true,
		nextPing:          time.Now(),
		nextFlush:         time.Now(),
		info:              network.VersionInfo(info),
		checksum:          crc32.NewIEEE(),
	}, nil
}

// Fail fails a job. The remote server is notified only when Close() is called.
func (c *Client) Fail(exitCode int, err error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.exitCode = exitCode
	c.err = err
}

// Close flushes any renaming data and lets the remote server know whether the
// state of the job was a failure or success.
func (c *Client) Close() error {
	c.requestLock.Lock()
	closed := c.closed
	if !closed {
		c.closed = true
	}
	c.requestLock.Unlock()

	if closed {
		return ErrClientIsClosed
	}

	defer c.rw.Close()

	for c.remaining() {
		if err := c.flush(true); err != nil {
			return fmt.Errorf("final flush: %w", err)
		}
	}

	// a last ping is required, as it updates the final job status
	for c.incomplete() {
		if err := c.ping(true); err != nil {
			return fmt.Errorf("final ping: %w", err)
		}
		time.Sleep(DefaultUpdateInterval)
	}

	return nil
}

// Write is the standard Write interface.
func (c *Client) Write(p []byte) (n int, err error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	n, err = c.rw.Write(p)
	c.written += int64(n)

	if _, cerr := c.checksum.Write(p[:n]); cerr != nil && err == nil {
		return n, cerr
	}

	return n, err
}

// Wait continously flushes data to the upstream server and returns any remote
// state changes.
//
// This function returns either when the remote state has changed or an error
// occurs.
//
// If no error is returned, Wait() can be called again to continue waiting for
// further state changes.
func (c *Client) Wait() (RemoteState, error) {
	for {
		if c.isClosed() {
			remoteState, _ := c.getRemoteState()
			return remoteState, ErrClientIsClosed
		}

		time.Sleep(time.Second)

		var err error
		if time.Now().After(c.nextPing) {
			err = c.ping(false)
		} else if c.remaining() && time.Now().After(c.nextFlush) {
			err = c.flush(false)
		}

		remoteState, changed := c.getRemoteState()
		if err != nil || changed {
			return remoteState, err
		}
	}
}

func (c *Client) getRemoteState() (RemoteState, bool) {
	c.mu.Lock()
	defer c.mu.Unlock()

	changed := c.remoteStateChange
	if changed {
		c.remoteStateChange = false
	}

	return c.remoteState, changed
}

// Ping informs GitLab the current status of the job. It is typically to ping
// the GitLab instance if no trace data has been sent recently. A final ping
// is also sent after all trace data has been transferred.
func (c *Client) ping(final bool) error {
	if !final && c.isClosed() {
		return nil
	}

	c.requestLock.Lock()
	defer c.requestLock.Unlock()

	c.mu.Lock()
	failErr := c.err
	failExitCode := c.exitCode
	checksum := fmt.Sprintf("crc32:%08x", c.checksum.Sum32())
	written := c.written
	c.mu.Unlock()

	request := network.TracePingRequest{
		Info:     c.info,
		Token:    c.creds.GetToken(),
		State:    string(stateRunning),
		Checksum: checksum,
		Output: network.TracePingRequestOutput{
			Checksum: checksum,
			Bytesize: written,
		},
	}

	if final {
		request.State = string(stateSuccess)

		if failErr != nil {
			request.State = string(stateFailed)
			switch {
			case errors.Is(failErr, ErrScriptFailure):
				request.FailureReason = ErrScriptFailure.Error()
			case errors.Is(failErr, ErrRunnerSystemFailure):
				request.FailureReason = ErrRunnerSystemFailure.Error()
			case errors.Is(failErr, ErrJobExecutionTimeout):
				request.FailureReason = ErrJobExecutionTimeout.Error()
			default:
				request.FailureReason = ErrUnknownFailure.Error()
			}
			request.ExitCode = failExitCode
		}
	}

	logger := c.opts.Logger.With(
		"state", request.State,
		"exit_code", request.ExitCode,
		"checksum", request.Checksum,
		"size", request.Output.Bytesize,
		"final", final,
	)
	logger.Debug("pinging")

	url := c.creds.GetBaseURL() + fmt.Sprintf(pathJob, c.creds.GetID())

	resp, err := c.base.API(context.Background(), http.MethodPut, url, request, nil)
	if err != nil {
		logger.Error("ping error", "err", err)
	}

	c.mu.Lock()
	defer c.mu.Unlock()

	if final && resp.StatusCode == http.StatusOK {
		c.complete = true
	}

	c.processHeaders(resp)

	c.nextPing = time.Now().Add(c.updateInterval * nextPingUpdateMultiplier)

	return err
}

func (c *Client) remaining() bool {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.written > c.uploaded
}

func (c *Client) incomplete() bool {
	c.mu.Lock()
	defer c.mu.Unlock()

	return !c.complete
}

func (c *Client) flush(final bool) error {
	if !final && c.isClosed() {
		return nil
	}

	c.requestLock.Lock()
	defer c.requestLock.Unlock()

	c.mu.Lock()
	written := c.written
	uploaded := c.uploaded
	c.mu.Unlock()

	n := written - uploaded
	if n > MaxTracePatchSize {
		n = MaxTracePatchSize
	}

	url := c.creds.GetBaseURL() + fmt.Sprintf(pathJobTrace, c.creds.GetID())

	req, err := http.NewRequest(http.MethodPatch, url, io.NewSectionReader(c.rw, uploaded, n))
	if err != nil {
		return err
	}

	req.GetBody = func() (io.ReadCloser, error) {
		return io.NopCloser(io.NewSectionReader(c.rw, uploaded, n)), nil
	}
	req.Header.Set(base.HeaderJobToken, c.creds.GetToken())
	req.Header.Set(base.HeaderContentType, "text/plain")
	req.Header.Set(base.HeaderContentRange, fmt.Sprintf("%d-%d", uploaded, uploaded+n-1))

	logger := c.opts.Logger.With(
		"log_from", uploaded,
		"log_to", uploaded+n-1,
	)
	logger.Debug("flushing")

	resp, err := c.base.Do(req)
	if err == nil {
		defer base.CloseBody(resp.Body)
	}

	c.mu.Lock()
	defer c.mu.Unlock()

	c.processHeaders(resp)

	c.nextFlush = time.Now().Add(c.updateInterval)
	c.nextPing = time.Now().Add(c.updateInterval * nextPingUpdateMultiplier)

	if err != nil {
		logger.Error("flush error")
		return err
	}

	c.uploaded += n

	return nil
}

func (c *Client) processHeaders(resp *http.Response) {
	if resp == nil {
		return
	}

	if resp.Header == nil {
		return
	}

	state := RemoteState(resp.Header.Get(headerJobStatus))
	switch state {
	case RemoteStateCanceled, RemoteStateCanceling:
		c.remoteState = state
		c.remoteStateChange = true
	}

	interval, _ := time.ParseDuration(resp.Header.Get(headerGitlabTraceUpdateInterval))
	if interval <= 0 {
		return
	}
	c.updateInterval = interval
}

func (t *Client) isClosed() bool {
	t.requestLock.Lock()
	defer t.requestLock.Unlock()

	return t.closed
}

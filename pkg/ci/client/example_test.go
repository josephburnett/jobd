package client_test

import (
	"archive/zip"
	"context"
	"fmt"
	"io"
	"os"
	"os/signal"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/jobd/jobd/pkg/ci/client"
	"gitlab.com/jobd/jobd/pkg/ci/client/artifactclient"
	"gitlab.com/jobd/jobd/pkg/ci/client/jobclient"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

func Example() {
	hclog.Default().SetLevel(hclog.Trace)

	gc, err := client.New("<token>", "https://gitlab.com")
	if err != nil {
		panic(err)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	var meta spec.Spec

	// in a real application, requesting a job would occur in a main loop, here
	// we only do it once.
	found, err := gc.RequestJob(ctx, &meta)
	if err != nil {
		panic(err)
	}

	if !found {
		panic("no jobs found")
	}

	// create job client for the job spec
	jc, err := jobclient.New(meta.JobCredentials(), gc.VersionInfo())
	if err != nil {
		panic(err)
	}
	defer jc.Close()

	// update job log
	fmt.Fprintln(jc, "working on job...")

	// upload a fake artifact for any defined zip artifacts
	for _, artifact := range meta.Artifacts {
		switch artifact.Format {
		case "zip", "":
		default:
			continue
		}

		ac, err := artifactclient.New(meta.JobCredentials())
		if err != nil {
			panic(err)
		}

		info := artifactclient.UploadInfo{
			Name:     artifact.Name,
			ExpireIn: artifact.ExpireIn,
			Format:   string(artifact.Format),
			Type:     artifact.Type,
		}

		err = ac.Upload(ctx, info, func(w io.Writer) error {
			zw := zip.NewWriter(w)

			fw, err := zw.Create("file")
			if err != nil {
				return err
			}

			if _, err := fmt.Fprintln(fw, "file data!"); err != nil {
				return err
			}

			return zw.Close()
		})
		if err != nil {
			panic(err)
		}
	}

	// finish job
	if err := jc.Close(); err != nil {
		panic(err)
	}
}

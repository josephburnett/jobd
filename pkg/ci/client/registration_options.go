package client

import (
	"fmt"
	"strings"

	"gitlab.com/jobd/jobd/pkg/ci/client/internal/network"
)

type registrationOptions = network.RunnerOptions

// RegistrationOption is an option used when registering or updating a runner details.
type RegistrationOption func(*registrationOptions) error

type AccessLevel string

const (
	NotProtected AccessLevel = "not_protected"
	RefProtected AccessLevel = "ref_protected"
)

func newRunnerOptions(opts []RegistrationOption) (registrationOptions, error) {
	var options registrationOptions

	options.Active = true
	options.AccessLevel = string(NotProtected)

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if len(options.Tags) == 0 {
		options.RunUntagged = true
	}

	return options, nil
}

// WithRegistrationDescription sets the runner's description.
func WithRegistrationDescription(description string) RegistrationOption {
	return func(o *registrationOptions) error {
		o.Description = description
		return nil
	}
}

// WithRegistrationPaused sets whether the runner is paused.
func WithRegistrationPaused() RegistrationOption {
	return func(o *registrationOptions) error {
		o.Active = false
		return nil
	}
}

// WithRegistrationLocked sets whether the runner is locked.
func WithRegistrationLocked() RegistrationOption {
	return func(o *registrationOptions) error {
		o.Locked = true
		return nil
	}
}

// WithRegistrationRunUntagged sets whether the runner will execute untagged jobs.
func WithRegistrationRunUntagged() RegistrationOption {
	return func(o *registrationOptions) error {
		o.RunUntagged = true
		return nil
	}
}

// WithRegistrationTag sets a runner tag. WithRegistrationTag can be called multiple times.
func WithRegistrationTag(tag string) RegistrationOption {
	return func(o *registrationOptions) error {
		if len(o.Tags) == 0 {
			o.Tags = tag
		} else {
			o.Tags += "," + tag
		}
		return nil
	}
}

// WithRegistrationMaximumTimeout sets a runner's maximum timeout.
func WithRegistrationMaximumTimeout(timeout int) RegistrationOption {
	return func(o *registrationOptions) error {
		o.MaximumTimeout = timeout
		return nil
	}
}

// WithRegistrationAccessLevel sets a runner's access level.
func WithRegistrationAccessLevel(level AccessLevel) RegistrationOption {
	return func(o *registrationOptions) error {
		lvl := AccessLevel(strings.ToLower(string(level)))

		switch lvl {
		case NotProtected, RefProtected:
			o.AccessLevel = string(lvl)
			return nil
		}

		return fmt.Errorf("unknown access level %q", level)
	}
}

// WithRegistrationMaintenanceNote sets a runner's maintenance note.
func WithRegistrationMaintenanceNote(note string) RegistrationOption {
	return func(o *registrationOptions) error {
		o.MaintenanceNote = note
		return nil
	}
}

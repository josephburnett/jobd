// Package client can be used to register, unregister and ask for jobs from a
// GitLab server.
//
// The client package is further split into multiple sub-packages:
//
// - artifactclient: client used for uploading/downloading a job's artifacts
//
// - jobclient: client used for handling a job (statuses, log updates etc.)
//
// - apitest: server faking the GitLab Runner API
package client

import (
	"context"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/jobd/jobd/pkg/ci/client/internal/base"
	"gitlab.com/jobd/jobd/pkg/ci/client/internal/network"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

// Client is a GitLab client for runners.
type Client struct {
	base    *base.BaseClient
	baseURL string

	opts options

	info       network.VersionInfo
	token      string
	lastUpdate string
}

type VersionInfo struct {
	network.RunnerInfo
	Features network.FeaturesInfo
}

var (
	ErrUnsupportedURLScheme = errors.New("only http or https scheme is supported")
)

const (
	pathRunners     = "/api/v4/runners"
	pathJobsRequest = "/api/v4/jobs/request"
)

// New returns a new Client.
func New(token string, baseURL string, options ...Option) (*Client, error) {
	u, err := url.Parse(baseURL)
	if err != nil {
		return nil, fmt.Errorf("parsing URL: %w", err)
	}
	if u.Scheme != "http" && u.Scheme != "https" {
		return nil, fmt.Errorf("invalid URL: %w", ErrUnsupportedURLScheme)
	}

	opts, err := newOptions(options)
	if err != nil {
		return nil, fmt.Errorf("configuring options: %w", err)
	}

	return &Client{
		base:    base.New(opts.TransportOptions.NewHTTPClient(), opts.Logger),
		baseURL: u.String(),
		opts:    opts,
		info:    network.VersionInfo(opts.VersionInfo),
		token:   token,
	}, nil
}

// Token returns the client's token.
func (c *Client) Token() string {
	return c.token
}

// VersionInfo returns the client's version info.
func (c *Client) VersionInfo() VersionInfo {
	return VersionInfo(c.info)
}

// Register registers a runner. This exchanges the registration token
// with one we can persist and continue to use.
func (c *Client) Register(ctx context.Context, options ...RegistrationOption) error {
	opts, err := newRunnerOptions(options)
	if err != nil {
		return err
	}

	var response network.RegisterResponse
	_, err = c.base.API(ctx, http.MethodPost, c.baseURL+pathRunners, network.RegisterRequest{
		RunnerOptions: opts,
		Info:          c.info,
		Token:         c.token,
	}, &response)
	if err != nil {
		return fmt.Errorf("register request: %w", err)
	}

	c.token = response.Token

	return nil
}

// Unregister unregisters a runner.
func (c *Client) Unregister(ctx context.Context) error {
	_, err := c.base.API(ctx, http.MethodDelete, c.baseURL+pathRunners, network.UnregisterRequest{Token: c.token}, nil)
	if err != nil {
		return fmt.Errorf("unregister request: %w", err)
	}

	return nil
}

// RequestJob requests a job from the Gitlab server.
func (c *Client) RequestJob(ctx context.Context, meta *spec.Spec) (bool, error) {
	request := network.RequestJobRequest{
		Info:       c.info,
		Token:      c.token,
		LastUpdate: c.lastUpdate,
	}

	resp, err := c.base.API(ctx, http.MethodPost, c.baseURL+pathJobsRequest, request, meta)
	if resp != nil && resp.Header != nil {
		if update := resp.Header.Get("X-GitLab-Last-Update"); len(update) > 0 {
			c.lastUpdate = update
		}
	}
	if errors.Is(err, io.EOF) {
		return false, nil
	}
	if err != nil {
		return false, fmt.Errorf("request job: %w", err)
	}

	var certificates [][]byte
	if c.opts.TransportOptions.CACertsFn != nil {
		for _, cert := range c.opts.TransportOptions.CACertsFn() {
			certificates = append(certificates, cert.Raw)
		}
	}

	// Copy server certificate to the certificate chain.
	//
	// This certificate is used so that jobs executed in a different environment
	// will trust the server without requiring a full chain.
	//
	// Newer versions of git should be able to trust incomplete chains, should
	// the PerrCertificates here not provide chain all the way to root. If this
	// is not the case, the user can supply an explicit CA bundle (WithCACerts()).
	if resp.TLS != nil {
		for _, cert := range resp.TLS.PeerCertificates {
			certificates = append(certificates, cert.Raw)
		}
	}

	var sb strings.Builder
	for _, certificate := range certificates {
		_ = pem.Encode(&sb, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: certificate,
		})
	}
	meta.Variables = append(meta.Variables, spec.Variable{
		Key:   "CI_SERVER_TLS_CA_FILE",
		Value: sb.String(),
		File:  true,
		Raw:   true,
	})

	return true, nil
}

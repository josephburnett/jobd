package apitest

import (
	"sync"

	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

type job struct {
	mu     sync.Mutex
	runner string
	meta   spec.Spec

	state         string
	failureReason string
	watching      bool
	trace         []byte
	checksum      string
	artifacts     map[string]Artifact
}

type Artifact struct {
	ExpireIn string
	Type     string
	Format   string
	Content  []byte
}

func (j *job) setState(state, failure string) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.state = state
	j.failureReason = failure
}

// State returns the job's state.
func (j *job) State() string {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.state
}

// FailureReason returns the job's reason for failure.
func (j *job) FailureReason() string {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.failureReason
}

func (j *job) appendTrace(p []byte) {
	j.trace = append(j.trace, p...)
}

func (j *job) getTraceSize() int64 {
	j.mu.Lock()
	defer j.mu.Unlock()

	return int64(len(j.trace))
}

func (j *job) setTraceChecksum(checksum string) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.checksum = checksum
}

func (j *job) assign(runner string) bool {
	j.mu.Lock()
	defer j.mu.Unlock()

	if j.runner == "" {
		j.runner = runner
		j.state = "running"
		return true
	}

	return false
}

// Watch sets whether a job's log is actively being watched.
func (j *job) Watch(watching bool) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.watching = watching
}

// IsWatched returns true if a job's log is actively being watched.
func (j *job) IsWatched() bool {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.watching
}

// Trace returns the current trace data.
func (j *job) Trace() []byte {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.trace
}

// Checksum returns the trace's checksum
func (j *job) Checksum() string {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.checksum
}

// Cancel cancels the job. This is similar to pressing the Cancel button on a
// job in GitLab's UI.
func (j *job) Cancel() {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.state = "canceled"
}

func (j *job) addArtifact(name string, a Artifact) {
	j.mu.Lock()
	defer j.mu.Unlock()

	if j.artifacts == nil {
		j.artifacts = make(map[string]Artifact)
	}

	j.artifacts[name] = a
}

// Artifact returns a job's artifact by name.
func (j *job) Artifact(name string) Artifact {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.artifacts[name]
}

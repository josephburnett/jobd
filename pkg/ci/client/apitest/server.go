package apitest

import (
	"context"
	"crypto/rand"
	"crypto/x509"
	"encoding/hex"
	"net"
	"net/http"
	"net/http/httptest"
	"sync"
	"time"

	"gitlab.com/jobd/jobd/pkg/ci/client/internal/network"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

const maxQueueDepth = 45

type Server struct {
	server   *httptest.Server
	hijacker *hijacker
	Config   Config

	mu                sync.Mutex
	runners           map[string]*runner
	jobs              map[int64]*job
	registrationToken string
	maxRegistrations  int
	nextJobID         int64
}

type Config struct {
	LongPollTimeout time.Duration
}

type hijacker struct {
	mu   sync.Mutex
	mux  *http.ServeMux
	next []func(rw http.ResponseWriter, r *http.Request)
}

type contextKey string

var ClientConnContextKey = contextKey("client-conn")

func (w *hijacker) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	var fn func(rw http.ResponseWriter, r *http.Request)

	w.mu.Lock()
	if len(w.next) > 0 {
		fn, w.next = w.next[0], w.next[1:]
	}
	w.mu.Unlock()

	if fn == nil {
		w.mux.ServeHTTP(rw, r)
		return
	}

	fn(rw, r)
}

type runner struct {
	network.RunnerOptions
	network.VersionInfo

	mu         sync.RWMutex
	queue      chan *job
	queueValue string
}

func (r *runner) ensureQueueValue() string {
	r.mu.Lock()
	defer r.mu.Unlock()
	if r.queueValue == "" {
		r.queueValue = randomHex()
	}
	return r.queueValue
}

func (r *runner) queueValueLatest(lastUpdate string) bool {
	r.mu.Lock()
	defer r.mu.Unlock()

	return r.queueValue == lastUpdate
}

// New returns a new "GitLab CI" server used for testing GitLab CI/CD API. Its
// behaviour is modeled after GitLab's implementation.
func New() *Server {
	s := &Server{
		hijacker: &hijacker{},
		Config: Config{
			LongPollTimeout: 5 * time.Second,
		},
		runners: make(map[string]*runner),
		jobs:    make(map[int64]*job),
	}

	s.hijacker.mux = http.NewServeMux()
	s.hijacker.mux.HandleFunc("/api/v4/runners", s.handleRunners)
	s.hijacker.mux.HandleFunc("/api/v4/jobs/request", s.handleJobRequest)
	s.hijacker.mux.HandleFunc("/api/v4/jobs/", s.handleJobs)

	s.server = httptest.NewUnstartedServer(s.hijacker)
	s.server.Config.ConnContext = func(ctx context.Context, c net.Conn) context.Context {
		return context.WithValue(ctx, ClientConnContextKey, c)
	}
	s.server.StartTLS()
	s.ResetRegistrationToken()

	return s
}

// URL returns the server's URL.
func (s *Server) URL() string {
	return s.server.URL
}

// Client returns a http.Client configured for the server (configuration such as
// TLS certificates).
func (s *Server) Client() *http.Client {
	return s.server.Client()
}

// Certificate returns the server's configured TLS certificate.
func (s *Server) Certificate() *x509.Certificate {
	return s.server.Certificate()
}

// Close closes the server.
func (s *Server) Close() {
	s.server.Close()
}

// Hijack hijacks the next incoming HTTP request. This is useful for adding in
// internal server errors when they would otherwise not be expected.
func (s *Server) Hijack(fn func(rw http.ResponseWriter, r *http.Request)) {
	s.hijacker.mu.Lock()
	defer s.hijacker.mu.Unlock()

	s.hijacker.next = append(s.hijacker.next, fn)
}

// ResetRegistrationToken resets the registration token. This is similar to
// pressing the reset registration button on a project's Runner settings page.
func (s *Server) ResetRegistrationToken() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.registrationToken = randomHex()
}

// RegistrationToken retursn the current registration token.
func (s *Server) RegistrationToken() string {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.registrationToken
}

// SetMaxRegistrations sets the maximum numbers of registations the server
// allows.
func (s *Server) SetMaxRegistrations(n int) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.maxRegistrations = n
}

// QueueJob consumes a Spec and queues a new job.
func (s *Server) QueueJob(meta spec.Spec) {
	s.mu.Lock()
	s.nextJobID++
	meta.ID = s.nextJobID
	s.mu.Unlock()

	meta.Token = randomHex()
	meta.Variables = append(
		meta.Variables,
		spec.Variable{
			Key:   "CI_SERVER_URL",
			Value: s.server.URL,
		},
		spec.Variable{
			Key:    "CI_JOB_TOKEN",
			Value:  meta.Token,
			Masked: true,
		},
	)

	for _, runner := range s.runners {
		if !runner.Active {
			continue
		}
		// todo: select correct runner
		if false {
			continue
		}

		runner.mu.Lock()
		runner.queueValue = randomHex()
		runner.mu.Unlock()
		runner.queue <- &job{meta: meta, state: "created"}
		break
	}
}

// Job returns a job by ID.
func (s *Server) Job(id int64) *job {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.jobs[id]
}

func (s *Server) addJob(job *job) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.jobs[job.meta.ID] = job
}

func randomHex() string {
	var b [16]byte
	rand.Read(b[:])
	return hex.EncodeToString(b[:])
}

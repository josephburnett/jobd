package apitest

import (
	"context"
	"crypto/subtle"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jobd/jobd/pkg/ci/client/internal/network"
)

func (s *Server) handleRunners(rw http.ResponseWriter, r *http.Request) {
	if !isValidContentType(rw, r) {
		return
	}

	switch r.Method {
	// Registers a new Runner
	// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L11
	case http.MethodPost:
		var request network.RegisterRequest
		if !isValidPayload(&request, rw, r) {
			return
		}

		if subtle.ConstantTimeCompare([]byte(request.Token), []byte(s.registrationToken)) == 0 {
			http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}

		s.mu.Lock()
		var maxRunners int
		if len(s.runners) >= s.maxRegistrations {
			maxRunners = s.maxRegistrations
		}
		s.mu.Unlock()
		if maxRunners > 0 {
			rw.WriteHeader(http.StatusBadRequest)
			rw.Write([]byte(fmt.Sprintf(`{"message":{"runner_projects.base":["Maximum number of ci registered project runners (%d) exceeded"]}}`, maxRunners)))
			return
		}

		resp := network.RegisterResponse{Token: randomHex()}
		runner := &runner{
			RunnerOptions: request.RunnerOptions,
			VersionInfo:   request.Info,
			queue:         make(chan *job, maxQueueDepth),
		}

		s.mu.Lock()
		s.runners[resp.Token] = runner
		s.mu.Unlock()

		rw.WriteHeader(http.StatusCreated)
		json.NewEncoder(rw).Encode(resp)

	// Deletes a registered Runner
	// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L54
	case http.MethodDelete:
		var request network.UnregisterRequest
		if !isValidPayload(&request, rw, r) {
			return
		}

		s.mu.Lock()
		_, ok := s.runners[request.Token]
		delete(s.runners, request.Token)
		s.mu.Unlock()

		if ok {
			rw.WriteHeader(http.StatusNoContent)
		} else {
			http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		}

	default:
		http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
}

// handleJobRequest requests a new job
// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L82
func (s *Server) handleJobRequest(rw http.ResponseWriter, r *http.Request) {
	if !isValidContentType(rw, r) {
		return
	}

	if r.Method != http.MethodPost {
		http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	var request network.RequestJobRequest
	if !isValidPayload(&request, rw, r) {
		return
	}

	s.mu.Lock()
	runner, ok := s.runners[request.Token]
	s.mu.Unlock()
	if !ok {
		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	// not active, reset queue value
	if !runner.Active {
		rw.Header().Set("X-GitLab-Last-Update", runner.ensureQueueValue())
		rw.WriteHeader(http.StatusNoContent)
		return
	}

	// simulate long polling, as handled by workhorse
	ctx, cancel := context.WithTimeout(r.Context(), s.Config.LongPollTimeout)
	defer cancel()
	for {
		timer := time.NewTimer(time.Second)
		select {
		case <-timer.C:
		case <-ctx.Done():
			timer.Stop()
			rw.WriteHeader(http.StatusNoContent)
			return
		}

		if !runner.queueValueLatest(request.LastUpdate) {
			break
		}
	}

	// build not found, reset queue value
	if len(runner.queue) == 0 {
		rw.Header().Set("X-GitLab-Last-Update", runner.ensureQueueValue())
		rw.WriteHeader(http.StatusNoContent)
		return
	}

	for job := range runner.queue {
		if job.assign(request.Token) {
			s.addJob(job)

			rw.WriteHeader(http.StatusCreated)
			json.NewEncoder(rw).Encode(job.meta)
			return
		}
	}

	http.Error(rw, http.StatusText(http.StatusConflict), http.StatusConflict)
}

func (s *Server) handleJobs(rw http.ResponseWriter, r *http.Request) {
	path := strings.TrimPrefix(r.URL.Path, "/api/v4/jobs/")
	idx := strings.Index(path+"/", "/")
	id, _ := strconv.ParseInt(path[:idx], 10, 64)
	path = path[idx:]

	job := s.Job(id)
	switch path {
	case "":
		switch r.Method {
		// Updates a job
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L160
		case http.MethodPut:
			s.handleTracePing(job, rw, r)
			return

		default:
			http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}
	case "/trace":
		switch r.Method {
		// Appends a patch to the job trace
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L195
		case http.MethodPatch:
			s.handleTracePatch(job, rw, r)
			return

		default:
			http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}

	case "/artifacts":
		switch r.Method {
		// Upload artifacts for job
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L267 (proxied via workhorse)
		case http.MethodPost:
			s.handleArtifactPost(job, rw, r)

		// Download the artifacts file for job
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L305
		case http.MethodGet:
			s.handleArtifactGet(job, rw, r)
		default:
			http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}

	default:
		panic(path)
	}
}

func (s *Server) handleTracePing(job *job, rw http.ResponseWriter, r *http.Request) {
	if !isValidContentType(rw, r) {
		return
	}

	var request network.TracePingRequest
	if !isValidPayload(&request, rw, r) {
		return
	}

	if job == nil || job.meta.Token != request.Token {
		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	if !isJobRunning(job, rw, r, true) {
		return
	}

	job.setState(request.State, request.FailureReason)
	if request.State != "running" {
		job.setTraceChecksum(request.Output.Checksum)

		rw.WriteHeader(http.StatusOK)
		return
	}

	if job.IsWatched() {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "5")
	} else {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "60")
	}
	rw.WriteHeader(http.StatusAccepted)
}

func (s *Server) handleTracePatch(job *job, rw http.ResponseWriter, r *http.Request) {
	if !isValidToken(job, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, true) {
		return
	}

	start, end, ok := parseContentRange(r.Header.Get("Content-Range"))
	if !ok {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	body, err := io.ReadAll(io.LimitReader(r.Body, 2*1024*1024))
	if err != nil {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if len(body) > 1024*1024 {
		http.Error(rw, http.StatusText(http.StatusRequestEntityTooLarge), http.StatusRequestEntityTooLarge)
		return
	}

	// https://gitlab.com/gitlab-org/gitlab-runner/issues/3275 fixed?
	if len(body) != int(end-start+1) {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if end-start > 125*1024*1024 {
		job.setState("failure", "trace_size_exceeded")
		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	if job.getTraceSize() != start {
		rw.Header().Set("Range", fmt.Sprintf("0-%d", job.getTraceSize()))
		http.Error(rw, http.StatusText(http.StatusRequestedRangeNotSatisfiable), http.StatusRequestedRangeNotSatisfiable)
		return
	}

	job.appendTrace(body)

	rw.Header().Set("Job-Status", job.State())
	rw.Header().Set("Range", fmt.Sprintf("0-%d", job.getTraceSize()))
	if job.IsWatched() {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "5")
	} else {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "60")
	}
}

func parseContentRange(cr string) (int64, int64, bool) {
	r := strings.SplitN(strings.TrimSpace(cr), "-", 2)
	if len(r) < 2 {
		return 0, 0, false
	}

	start, err := strconv.ParseInt(r[0], 10, 64)
	if err != nil {
		return 0, 0, false
	}
	end, err := strconv.ParseInt(r[1], 10, 64)
	if err != nil {
		return 0, 0, false
	}

	return start, end, true
}

func (s *Server) handleArtifactPost(job *job, rw http.ResponseWriter, r *http.Request) {
	if !isValidToken(job, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, false) {
		return
	}

	f, fh, err := r.FormFile("file")
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	defer f.Close()

	content, err := ioutil.ReadAll(f)
	if err != nil {
		http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	job.addArtifact(fh.Filename, Artifact{
		ExpireIn: r.URL.Query().Get("expire_in"),
		Type:     r.URL.Query().Get("artifact_type"),
		Format:   r.URL.Query().Get("artifact_format"),
		Content:  content,
	})
}

func (s *Server) handleArtifactGet(job *job, rw http.ResponseWriter, r *http.Request) {
	if !isValidToken(job, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, false) {
		return
	}

	for _, a := range job.artifacts {
		if a.Type == "archive" {
			_, err := rw.Write(a.Content)
			if err != nil {
				http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}
		}
	}
}

func isValidContentType(rw http.ResponseWriter, r *http.Request) bool {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)

		return false
	}

	return true
}

func isValidToken(job *job, rw http.ResponseWriter, r *http.Request) bool {
	if job == nil || job.meta.Token != r.Header.Get("Job-Token") {
		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)

		return false
	}

	return true
}

func isJobRunning(job *job, rw http.ResponseWriter, r *http.Request, setJobStatus bool) bool {
	if job.State() != "running" {
		if setJobStatus {
			rw.Header().Set("Job-Status", job.state)
		}

		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)

		return false
	}

	return true
}

func isValidPayload(request interface{}, rw http.ResponseWriter, r *http.Request) bool {
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return false
	}

	return true
}

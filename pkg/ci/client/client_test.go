package client

import (
	"context"
	"crypto/x509"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci/client/apitest"
	"gitlab.com/jobd/jobd/pkg/ci/client/transport"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

func TestNewBaseURL(t *testing.T) {
	tests := map[string]struct {
		url      string
		succeeds bool
	}{
		"empty url":           {"", false},
		"invalid url":         {"::/", false},
		"invalid scheme":      {"foobar://host/", false},
		"valid scheme: http":  {"http://host/", true},
		"valid scheme: https": {"HTTPS://HOST/", true},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			_, err := New("", tc.url)
			if tc.succeeds {
				require.NoError(t, err)
			} else {
				require.Error(t, err)
			}
		})
	}
}

func TestRegister(t *testing.T) {
	srv := apitest.New()
	defer srv.Close()

	options := []Option{
		WithTransportOptions(transport.WithCACerts(func() []*x509.Certificate {
			return []*x509.Certificate{srv.Certificate()}
		})),
		WithVersionInfoUpdater(func(info *VersionInfo) {
			info.Features.Services = true
		}),
	}

	client, err := New("", srv.URL(), options...)
	require.NoError(t, err)

	t.Run("invalid token", func(t *testing.T) {
		client.token = "foobar"
		err := client.Register(context.Background())

		var target *transport.ResponseError
		require.ErrorAs(t, err, &target)
		require.Equal(t, http.StatusForbidden, target.Response.StatusCode)
	})

	t.Run("valid token", func(t *testing.T) {
		client.token = srv.RegistrationToken()
		assert.NoError(t, client.Register(
			context.Background(),
			WithRegistrationDescription("foobar"),
			WithRegistrationPaused(),
			WithRegistrationLocked(),
			WithRegistrationRunUntagged(),
			WithRegistrationTag("foo"),
			WithRegistrationTag("bar"),
			WithRegistrationMaximumTimeout(10),
		))
	})

	t.Run("max registations", func(t *testing.T) {
		srv.SetMaxRegistrations(1)

		client.token = srv.RegistrationToken()

		var target *transport.ResponseError
		err = client.Register(context.Background(), WithRegistrationDescription("foobar"))
		assert.ErrorAs(t, err, &target)
		assert.Equal(t, "runner_projects.base: Maximum number of ci registered project runners (1) exceeded", string(target.Msg))
	})
}

func TestUnregister(t *testing.T) {
	srv := apitest.New()
	defer srv.Close()

	options := []Option{
		WithTransportOptions(transport.WithCACerts(func() []*x509.Certificate {
			return []*x509.Certificate{srv.Certificate()}
		})),
	}

	client, err := New(srv.RegistrationToken(), srv.URL(), options...)
	require.NoError(t, err)
	require.NoError(t, client.Register(context.Background()))
	token := client.token

	t.Run("invalid token", func(t *testing.T) {
		client.token = "foobar"
		err := client.Unregister(context.Background())

		var target *transport.ResponseError
		require.ErrorAs(t, err, &target)
		assert.Equal(t, http.StatusForbidden, target.Response.StatusCode)
	})

	t.Run("valid token", func(t *testing.T) {
		client.token = token
		require.NoError(t, client.Unregister(context.Background()))
	})
}

func TestRequestJob(t *testing.T) {
	srv := apitest.New()
	defer srv.Close()

	options := []Option{
		WithTransportOptions(transport.WithCACerts(func() []*x509.Certificate {
			return []*x509.Certificate{srv.Certificate()}
		})),
	}

	client, err := New(srv.RegistrationToken(), srv.URL(), options...)
	require.NoError(t, err)
	require.NoError(t, client.Register(context.Background()))

	var job spec.Spec

	found, err := client.RequestJob(context.Background(), &job)
	require.NoError(t, err)
	assert.False(t, found)

	srv.QueueJob(spec.Spec{})

	found, err = client.RequestJob(context.Background(), &job)
	require.NoError(t, err)
	assert.True(t, found)
	assert.Equal(t, int64(1), job.ID)
}

package spec

import "gitlab.com/jobd/jobd/pkg/ci"

type Spec struct {
	ID            int64          `json:"id"`
	Token         string         `json:"token"`
	AllowGitFetch bool           `json:"allow_git_fetch"`
	JobInfo       JobInfo        `json:"job_info"`
	GitInfo       GitInfo        `json:"git_info"`
	RunnerInfo    RunnerInfo     `json:"runner_info"`
	Variables     []Variable     `json:"variables"`
	Steps         []Step         `json:"steps"`
	Image         Image          `json:"image"`
	Services      []Service      `json:"services"`
	Artifacts     []Artifact     `json:"artifacts"`
	Cache         []Cache        `json:"cache"`
	Credentials   []Credentials  `json:"credentials"`
	Dependencies  []Dependency   `json:"dependencies"`
	Features      GitlabFeatures `json:"features"`
}

func (s Spec) JobCredentials() ci.Credentials {
	base := "https://gitlab.com"

	for i := len(s.Variables) - 1; i >= 0; i-- {
		if s.Variables[i].Key == "CI_SERVER_URL" {
			base = s.Variables[i].Value
			break
		}
	}

	return ci.Credentials{
		ID:      s.ID,
		Token:   s.Token,
		BaseURL: base,
	}
}

type JobInfo struct {
	Name        string `json:"name"`
	Stage       string `json:"stage"`
	ProjectID   int64  `json:"project_id"`
	ProjectName string `json:"project_name"`
}

type GitInfoRefType string

const (
	RefTypeBranch GitInfoRefType = "branch"
	RefTypeTag    GitInfoRefType = "tag"
)

type GitInfo struct {
	RepoURL   string         `json:"repo_url"`
	Ref       string         `json:"ref"`
	Sha       string         `json:"sha"`
	BeforeSha string         `json:"before_sha"`
	RefType   GitInfoRefType `json:"ref_type"`
	Refspecs  []string       `json:"refspecs"`
	Depth     int            `json:"depth"`
}

type RunnerInfo struct {
	Timeout int `json:"timeout"`
}

type Variable struct {
	Key    string `json:"key"`
	Value  string `json:"value"`
	Public bool   `json:"public"`
	File   bool   `json:"file"`
	Masked bool   `json:"masked"`
	Raw    bool   `json:"raw"`
}

type StepName string

const (
	StepNameScript      StepName = "script"
	StepNameAfterScript StepName = "after_script"
)

type When string

const (
	WhenOnFailure When = "on_failure"
	WhenOnSuccess When = "on_success"
	WhenAlways    When = "always"
)

type CachePolicy string

const (
	CachePolicyUndefined CachePolicy = ""
	CachePolicyPullPush  CachePolicy = "pull-push"
	CachePolicyPull      CachePolicy = "pull"
	CachePolicyPush      CachePolicy = "push"
)

type Step struct {
	Name         StepName `json:"name"`
	Script       []string `json:"script"`
	Timeout      int      `json:"timeout"`
	When         When     `json:"when"`
	AllowFailure bool     `json:"allow_failure"`
}

type Image struct {
	Name       string   `json:"name"`
	Alias      string   `json:"alias,omitempty"`
	Command    []string `json:"command,omitempty"`
	Entrypoint []string `json:"entrypoint,omitempty"`
}

type Service struct {
	Image
	Variables []Variable `json:"variables"`
}

type ArtifactPaths []string

type ArtifactFormat string

const (
	ArtifactFormatDefault ArtifactFormat = ""
	ArtifactFormatZip     ArtifactFormat = "zip"
	ArtifactFormatGzip    ArtifactFormat = "gzip"
	ArtifactFormatRaw     ArtifactFormat = "raw"
)

type Artifact struct {
	Name      string         `json:"name"`
	Untracked bool           `json:"untracked"`
	Paths     ArtifactPaths  `json:"paths"`
	Exclude   ArtifactPaths  `json:"exclude"`
	When      When           `json:"when"`
	Type      string         `json:"artifact_type"`
	Format    ArtifactFormat `json:"artifact_format"`
	ExpireIn  string         `json:"expire_in"`
}

type Cache struct {
	Key       string        `json:"key"`
	Untracked bool          `json:"untracked"`
	Policy    CachePolicy   `json:"policy"`
	Paths     ArtifactPaths `json:"paths"`
	When      When          `json:"when"`
}

type Credentials struct {
	Type     string `json:"type"`
	URL      string `json:"url"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type DependencyArtifactsFile struct {
	Filename string `json:"filename"`
	Size     int64  `json:"size"`
}

type Dependency struct {
	ID            int64                   `json:"id"`
	Token         string                  `json:"token"`
	Name          string                  `json:"name"`
	ArtifactsFile DependencyArtifactsFile `json:"artifacts_file"`
}

type GitlabFeatures struct {
	TraceSections bool `json:"trace_sections"`
}

package spec

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRescursiveExpand(t *testing.T) {
	variables := []Variable{
		{Key: "foo", Value: "bar"},
		{Key: "expanded-foo", Value: "$foo"},
		{Key: "expanded-expanded-foo", Value: "${expanded-foo}"},
	}

	require.Equal(t, "bar", Expand(variables, "${expanded-expanded-foo}"))
}

func TestCyclicExpand(t *testing.T) {
	variables := []Variable{
		{Key: "one", Value: "$two"},
		{Key: "two", Value: "$one"},
	}

	require.Equal(t, "", Expand(variables, "$one"))
}

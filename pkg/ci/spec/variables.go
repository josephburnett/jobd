package spec

import "os"

func Expand(variables []Variable, value string) string {
	visited := make(map[string]string)
	resolving := make(map[string]struct{})

	var get func(string) string
	get = func(key string) string {
		if val, ok := visited[key]; ok {
			return val
		}
		if _, ok := resolving[key]; ok {
			return ""
		}

		resolving[key] = struct{}{}

		switch key {
		case "$":
			visited[key] = key
			return key
		case "*", "#", "@", "!", "?", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
			visited[key] = ""
			return ""
		}
		for i := len(variables) - 1; i >= 0; i-- {
			if variables[i].Key == key {
				visited[key] = os.Expand(variables[i].Value, get)

				return visited[key]
			}
		}
		return ""
	}

	return os.Expand(value, get)
}

package spec

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/jobd/jobd/pkg/ci"
)

func TestBaseURL(t *testing.T) {
	t.Run("without server url", func(t *testing.T) {
		s := Spec{ID: 123, Token: "abcdefg"}

		require.Equal(t, ci.Credentials{
			ID:      123,
			Token:   "abcdefg",
			BaseURL: "https://gitlab.com",
		}, s.JobCredentials())
	})

	t.Run("with server url", func(t *testing.T) {
		s := Spec{
			ID:    123,
			Token: "abcdefg",
			Variables: []Variable{
				{Key: "CI_SERVER_URL", Value: "https://example.com"},
			},
		}

		require.Equal(t, ci.Credentials{
			ID:      123,
			Token:   "abcdefg",
			BaseURL: "https://example.com",
		}, s.JobCredentials())
	})
}

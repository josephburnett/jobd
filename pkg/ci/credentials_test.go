package ci

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type credentialProvider struct {
}

func (credentialProvider) GetID() int64 {
	return 123
}

func (credentialProvider) GetToken() string {
	return "abcdefg"
}

func (credentialProvider) GetBaseURL() string {
	return "https://example.com"
}

func TestCredentialsFromJobCredentials(t *testing.T) {
	expected := Credentials{
		ID:      123,
		Token:   "abcdefg",
		BaseURL: "https://example.com",
	}

	require.Equal(t, int64(123), expected.GetID())
	require.Equal(t, "abcdefg", expected.GetToken())
	require.Equal(t, "https://example.com", expected.GetBaseURL())

	require.Equal(t, expected, CredentialsFromJobCredentials(credentialProvider{}))
	require.Equal(t, expected, CredentialsFromJobCredentials(expected))
	require.Equal(t, Credentials{}, CredentialsFromJobCredentials(nil))
}

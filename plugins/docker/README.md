# Docker Executor

The Docker executor connects to a Docker daemon to execute workloads.

Work is carried out with the help of a `taskrunner` binary that executes tasks
within the workload's defined container. Service containers can run alongside
to support these tasks.

The following Docker resources are created:
  - `taskrunner` volume: this is persisted and reused for all future jobs.
  - `taskrunner` installation container: this is only executed for installation
    to the `taskrunner` volume.
  - For each job:
    - workload container
    - workload volume
    - service container: for each defined service.
    - service volume: for each defined service and is mounted to both the
      service and workload container, under the path
      `/runner/services/<service name>`).
    - network bridge: this is an isolated network from other workloads, but due
      to the creation overhead, these are reused when no other workload has the
      bridge attached.

Each time the manager connects to the daemon, it searches for existing workloads
to check if any can be resumed. This is done with an initial listing of
containers based on the presence of a `jobd.job` label. This is called the
"Initial collection". Similarly, the creation of new work and reporting it back
to the manager are separate. Work is only reported to the manager by watching
the docker event stream for containers with the same label. This is called
"Continuous collection" and forms part of the main event loop for both
reconciliation and garbage collection, as resources that are found that have an
error or cannot be reattached to the GitLab instance are ultimately removed.

## Runner configuration

```cuelang
limit: 10                     // restrict to 10 concurrent jobs
url:   "https://gitlab.com"   // gitlab instance url
token: "BYHhbzWWDj5ceAKrSYQp" // runner token

executor: {
  // Path to docker plugin.
  type: "/path/to/docker-plugin"

  // The credentialsProviderFile is an optional docker credential store config.
  // This contains credsStore and credHelpers definitions for the docker
  // executor to access images from registries that require authentication.
  credentialProviderFile: "/path/to/credential/store/config" 

  // Whether work and service container uses privileged mode. This shouldn't be
  // used in shared and untrusted environments, as it can allow host access.
  privileged: true
}
```

## Services

### DinD

To use Docker-in-Docker, the service container needs to be in privileged
mode. This shouldn't be used in shared and untrusted environments, as it can
allow host access.

The DinD container automatically generates TLS certificates that it expects the
docker client to then use. We can use `DOCKER_TLS_CERTDIR` to tell the service
where to put the generated certifcates and `DOCKER_CERT_PATH` to let the client
know where to find them. We make use of the automatic volume share between
service and work container available at `/runner/services/docker`.

```yaml
variables:
  DOCKER_TLS_CERTDIR: "/runner/services/docker/certs"
  DOCKER_CERT_PATH: "/runner/services/docker/certs/client"
  DOCKER_TLS_VERIFY: "1"
  DOCKER_HOST: "tcp://docker:2376"

dind-work:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker run 
```

## Known issues / TODO

- Docker resources need a label to represent the Runner that owns each workload
  and which created network bridges. At the moment, if two Runners point to the
  same daemon, they'll conflict with each other.

## Differences to GitLab-Runner docker executor

- Each service automatically has a volume shared between it and the work
  container available at `/runner/services/<image name or alias>`.
- The dockerd config containing credential store config is not automatically
  used and must be explicitly loaded with `credentialProviderFile`.
- Isolated networks are created by default. Default `dockerd` configurations
  only allow up to 31 networks. The default network ranges can also conflict
  with your existing network. Ensure that the correct `default-address-pools` 
  setup has been configured in your Docker configuration to avoid problems.

package impl

import (
	"context"
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor"
	"gitlab.com/jobd/jobd/pkg/executor/clientstore"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"

	"gitlab.com/jobd/jobd/plugins/docker/impl/handler"
)

type Executor struct {
	cfg     handler.Config
	handler *handler.Handler
	store   clientstore.Store[*handler.Workload]
	logger  hclog.Logger
}

func (e *Executor) Init(ctx context.Context, log hclog.Logger, config []byte) error {
	e.logger = log
	e.logger.Debug("init")

	cfg, err := parseConfig(config)
	if err != nil {
		return err
	}
	e.cfg = cfg

	e.handler, err = handler.New(ctx, e.logger, cfg, &e.store)
	if err != nil {
		return fmt.Errorf("creating client: %w", err)
	}

	return nil
}

func (e *Executor) Discover(ctx context.Context, cursor uint64, follow bool, fn func(ci.Credentials, uint64) error) error {
	e.logger.Debug("discover", "follow", follow, "cursor", cursor)

	return e.store.Next(ctx, cursor, follow, fn)
}

func (e *Executor) Create(ctx context.Context, meta spec.Spec, w io.Writer, script taskscript.Script) error {
	lfmt := logfmt.New(w, logfmt.Config{
		StreamID:         logfmt.StreamExecutorLevel,
		InsertTimestamps: true,
	})
	defer lfmt.Close()

	e.logger.Debug("create", "id", meta.ID)

	return e.handler.Create(ctx, &meta, lfmt, script)
}

func (e *Executor) Delete(ctx context.Context, credentials ci.Credentials) error {
	e.logger.Debug("delete", "id", credentials.ID)

	return e.handler.Delete(ctx, credentials)
}

func (e *Executor) Attach(ctx context.Context, credentials ci.Credentials, w io.Writer, offset int64) (*executor.Result, error) {
	e.logger.Debug("attach", "id", credentials.ID)

	workload := e.store.Get(credentials)
	if workload == nil {
		return nil, fmt.Errorf("unknown workload")
	}

	if err := workload.Client().Follow(ctx, 0, w); err != nil {
		return nil, err
	}

	status, err := workload.Client().Status(ctx)
	if err != nil {
		return nil, err
	}

	return &executor.Result{
		Finished: status.Finished,
		ExitCode: status.ExitCode,
	}, nil
}

package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

type Config struct {
	AuthConfigs       map[string]AuthConfig `json:"auths"`
	CredentialsStore  string                `json:"credsStore,omitempty"`
	CredentialHelpers map[string]string     `json:"credHelpers,omitempty"`
}

type AuthConfig struct {
	Username      string `json:"username,omitempty"`
	Password      string `json:"password,omitempty"`
	Auth          string `json:"auth,omitempty"`
	Email         string `json:"email,omitempty"`
	ServerAddress string `json:"serveraddress,omitempty"`
	IdentityToken string `json:"identitytoken,omitempty"`
	RegistryToken string `json:"registrytoken,omitempty"`
}

type Options struct {
	FromSpecCredentials []spec.Credentials
	FromData            string
	FromFile            string
}

const (
	credentialProgramPrefix = "docker-credential-"
	credentialTokenUsername = "<token>"
)

func Get(image string, opts Options) (string, error) {
	cfg, err := get(image, opts)
	if err != nil {
		return "", err
	}

	result, _ := json.Marshal(cfg)
	return base64.StdEncoding.EncodeToString(result), nil
}

func get(image string, opts Options) (AuthConfig, error) {
	registry := parseImage(image)

	// match from spec credentials
	for _, creds := range opts.FromSpecCredentials {
		if match(registry, creds.URL) {
			return AuthConfig{
				Username:      creds.Username,
				Password:      creds.Password,
				ServerAddress: creds.URL,
			}, nil
		}
	}

	// match from data
	if opts.FromData != "" {
		var cfg Config
		if err := json.Unmarshal([]byte(opts.FromData), &cfg); err != nil {
			return AuthConfig{}, fmt.Errorf("unmarshaling auth data: %w", err)
		}

		for domain, auth := range cfg.AuthConfigs {
			if match(registry, domain) {
				return auth, nil
			}
		}

		// gitlab-runner allows credHelpers and credsStore for workload provided
		// configs, but I'm unaware of any valid use case. If the native
		// credential store has to be configured on the host anyway, then the
		// admin can utilize the helper settings via FromFile.
		if len(cfg.CredentialsStore) > 0 || len(cfg.CredentialHelpers) > 0 {
			return AuthConfig{}, fmt.Errorf("credHelpers/credsStore is unsupported in this context")
		}
	}

	// match from file
	if opts.FromFile != "" {
		buf, err := os.ReadFile(opts.FromFile)
		if err != nil {
			return AuthConfig{}, fmt.Errorf("reading auth file: %w", err)
		}

		var cfg Config
		if err := json.Unmarshal(buf, &cfg); err != nil {
			return AuthConfig{}, fmt.Errorf("unmarshaling auth file: %w", err)
		}

		for domain, auth := range cfg.AuthConfigs {
			if match(registry, domain) {
				return auth, nil
			}
		}

		for domain, store := range cfg.CredentialHelpers {
			if match(registry, domain) {
				return api(store, domain)
			}
		}

		if cfg.CredentialsStore != "" {
			return api(cfg.CredentialsStore, registry)
		}
	}

	return AuthConfig{}, nil
}

func match(registry, url string) bool {
	url = strings.ToLower(url)
	url = strings.TrimPrefix(url, "http://")
	url = strings.TrimPrefix(url, "https://")
	domain, _, _ := strings.Cut(url, "/")

	return isDefault(registry) == isDefault(domain) || registry == domain
}

func parseImage(image string) string {
	registry, _, found := strings.Cut(image, "/")

	if !found || !strings.ContainsAny(registry, ".:") {
		registry = "" // default registry
	}

	return strings.ToLower(registry)
}

func isDefault(domain string) bool {
	switch domain {
	case "", "index.docker.io", "docker.io":
		return true
	}

	return false
}

func api(name, registry string) (AuthConfig, error) {
	name = filepath.Base(name)
	cmd := exec.Command(credentialProgramPrefix+name, "get")
	cmd.Stdin = strings.NewReader(registry)

	buf, err := cmd.Output()
	if err != nil {
		return AuthConfig{}, fmt.Errorf("fetching credentials (%q): %w", string(buf), err)
	}

	var creds struct {
		ServerURL string
		Username  string
		Secret    string
	}
	creds.ServerURL = registry

	if err := json.Unmarshal(buf, &creds); err != nil {
		return AuthConfig{}, fmt.Errorf("unmarshaling credentials from provider: %w", err)
	}

	var auth AuthConfig
	if creds.Username == credentialTokenUsername {
		auth.IdentityToken = creds.Secret
	} else {
		auth.Password = creds.Secret
		auth.Username = creds.Username
	}

	return auth, err
}

package impl

import (
	"fmt"

	"cuelang.org/go/cue"
	"cuelang.org/go/cue/cuecontext"
	"gitlab.com/jobd/jobd/plugins/docker/impl/handler"
)

const configConstraint = `
executor: close({
	type: "docker" | string

	credentialProviderFile?: string

	privileged: bool
})
`

func parseConfig(config []byte) (handler.Config, error) {
	var cfg handler.Config

	cctx := cuecontext.New()
	cval := cctx.CompileBytes(config)
	if err := cval.Err(); err != nil {
		return cfg, fmt.Errorf("compiling config: %w", err)
	}

	constraint := cctx.CompileString(configConstraint)
	if err := constraint.Err(); err != nil {
		return cfg, fmt.Errorf("compiling constraint: %w", err)
	}

	cval = cval.Unify(constraint)
	if err := cval.Validate(cue.Concrete(true), cue.Final()); err != nil {
		return cfg, fmt.Errorf("executor validating config: %w", err)
	}

	err := cval.LookupPath(cue.ParsePath("executor")).Decode(&cfg)
	if err != nil {
		return cfg, fmt.Errorf("decoding config: %w", err)
	}

	return cfg, nil
}

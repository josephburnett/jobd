package handler

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"sync"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"golang.org/x/sync/errgroup"
)

const minPoolNetworks = 5

type networkPool struct {
	c *client.Client

	mu       sync.Mutex
	freelist map[string]struct{}
}

func newNetworkPool(c *client.Client) *networkPool {
	h := &networkPool{
		c:        c,
		freelist: make(map[string]struct{}),
	}

	return h
}

func (np *networkPool) init(ctx context.Context) error {
	filter := filters.NewArgs(
		filters.KeyValuePair{
			Key:   "label",
			Value: "jobd",
		},
		filters.KeyValuePair{
			Key:   "dangling",
			Value: "true",
		},
	)

	networks, err := np.c.NetworkList(ctx, types.NetworkListOptions{
		Filters: filter,
	})
	if err != nil {
		return fmt.Errorf("fetching network list: %w", err)
	}

	for _, network := range networks {
		np.freelist[network.Name] = struct{}{}
	}

	group, ctx := errgroup.WithContext(ctx)
	for i := len(np.freelist); i < minPoolNetworks; i++ {
		group.Go(func() error {
			name, err := np.create(ctx)
			if err != nil {
				return err
			}

			np.mu.Lock()
			np.freelist[name] = struct{}{}
			np.mu.Unlock()

			return nil
		})
	}

	return group.Wait()
}

func (np *networkPool) Get(ctx context.Context) (string, error) {
	var name string
	np.mu.Lock()
	for free := range np.freelist {
		name = free
		break
	}
	delete(np.freelist, name)
	np.mu.Unlock()

	if name != "" {
		return name, nil
	}

	return np.create(ctx)
}

func (np *networkPool) Put(network string) {
	np.mu.Lock()
	np.freelist[network] = struct{}{}
	np.mu.Unlock()
}

func (np *networkPool) create(ctx context.Context) (string, error) {
	var buf [8]byte
	rand.Read(buf[:])
	name := "jobd-" + hex.EncodeToString(buf[:])

	_, err := np.c.NetworkCreate(ctx, name, types.NetworkCreate{
		Labels: map[string]string{
			"jobd": "jobd",
		},
	})
	if err != nil {
		return "", fmt.Errorf("creating new network: %w", err)
	}

	return name, nil
}

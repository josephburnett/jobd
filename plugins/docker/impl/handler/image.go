package handler

import (
	"context"
	"encoding/json"
	"io"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/pkg/jsonmessage"
	"golang.org/x/sync/errgroup"

	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/plugins/docker/impl/internal/auth"
)

func (h *Handler) fetchImages(ctx context.Context, meta *spec.Spec, lfmt *logfmt.Fmt) error {
	// expand image names
	images := []string{taskrunnerImage, spec.Expand(meta.Variables, meta.Image.Name)}
	for _, service := range meta.Services {
		images = append(images, service.Name)
	}

	// authentication options
	opts := auth.Options{
		FromSpecCredentials: meta.Credentials,
		FromData: func() string {
			for _, variable := range meta.Variables {
				if variable.Key == "DOCKER_AUTH_CONFIG" {
					return variable.Value
				}
			}
			return ""
		}(),
		FromFile: h.cfg.CredentialProviderFile,
	}

	// we don't limit pulls here and leave dockerd to throttle downloads with
	// its 'max-concurrent-downloads' setting.
	group, ctx := errgroup.WithContext(ctx)
	for _, image := range images {
		imageName := image

		group.Go(func() error {
			rc, err := h.client.ImagePull(ctx, imageName, types.ImagePullOptions{
				PrivilegeFunc: func() (string, error) {
					return auth.Get(imageName, opts)
				},
			})
			if err != nil {
				return err
			}
			defer rc.Close()

			dec := json.NewDecoder(rc)
			for {
				var jm jsonmessage.JSONMessage
				if err := dec.Decode(&jm); err != nil {
					if err == io.EOF {
						break
					}
					return err
				}

				if jm.ID == "" {
					lfmt.Stderr().Write([]byte(jm.Status + "\n"))
					continue
				}
			}

			return nil
		})
	}

	return group.Wait()
}

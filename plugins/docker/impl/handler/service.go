package handler

import (
	"fmt"
	"strings"

	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

type services struct {
	Services []service

	Hosts []string
}

type service struct {
	Name   string
	Image  string
	Volume string
	Env    []string

	Parent        string
	ContainerName string
}

func getServices(meta *spec.Spec) services {
	svcs := services{}

	var env []string
	var variables []spec.Variable
	for _, variable := range meta.Variables {
		if !variable.Public || variable.File {
			continue
		}
		variables = append(variables, variable)
	}
	for _, variable := range variables {
		if !variable.Raw {
			variable.Value = spec.Expand(variables, variable.Value)
		}
		env = append(env, variable.Key+"="+variable.Value)
	}

	for idx, item := range meta.Services {
		svc := service{}

		svc.Name = spec.Expand(variables, item.Name)
		svc.Parent = fmt.Sprintf("jobd-%d", meta.ID)
		svc.ContainerName = fmt.Sprintf("jobd-%d-service-%d", meta.ID, idx)

		alias := "/" + item.Name
		alias = item.Name[strings.LastIndex(alias, "/"):]
		alias, _, _ = strings.Cut(alias, ":")

		svcs.Hosts = append(svcs.Hosts, alias+":127.0.0.1")

		if item.Alias == "" {
			item.Alias = alias
		} else {
			svcs.Hosts = append(svcs.Hosts, item.Alias+":127.0.0.1")
		}

		svc.Volume = "/runner/services/" + item.Alias

		svc.Env = make([]string, len(env))
		copy(svc.Env, env)

		svariables := make([]spec.Variable, len(variables))
		copy(svariables, variables)

		svariables = append(svariables, item.Variables...)
		for _, variable := range item.Variables {
			if !variable.Raw {
				variable.Value = spec.Expand(svariables, variable.Value)
			}
			svc.Env = append(svc.Env, variable.Key+"="+variable.Value)
		}

		svcs.Services = append(svcs.Services, svc)
	}

	return svcs
}

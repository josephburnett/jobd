package handler

import (
	"context"
	"fmt"
	"io"

	"github.com/docker/docker/client"
	"github.com/hashicorp/go-hclog"

	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor/clientstore"
	"gitlab.com/jobd/jobd/pkg/taskrunner/agent"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
)

const (
	taskrunnerImage = "registry.gitlab.com/jobd/jobd/taskrunner:v0.0.0-dev"

	jobdParentLabel  = "jobd.parent"
	jobdJobLabel     = "jobd.job"
	jobdProjectLabel = "jobd.project"
	jobdTokenLabel   = "jobd.token"
	jobdBaseURLLabel = "jobd.base_url"
)

type Handler struct {
	cfg    Config
	client *client.Client
	logger hclog.Logger
	store  *clientstore.Store[*Workload]

	networkPool *networkPool
}

type Config struct {
	CredentialProviderFile string
	Privileged             bool
}

type Workload struct {
	handler *Handler
	client  agent.Client
}

func (w *Workload) Client() agent.Client {
	return w.client
}

func New(ctx context.Context, logger hclog.Logger, cfg Config, store *clientstore.Store[*Workload]) (*Handler, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, fmt.Errorf("docker client: %w", err)
	}

	c := &Handler{
		cfg:         cfg,
		client:      cli,
		logger:      logger,
		store:       store,
		networkPool: newNetworkPool(cli),
	}

	if err := c.networkPool.init(ctx); err != nil {
		return nil, fmt.Errorf("initializing network pool: %w", err)
	}

	go c.continuousContainerCollect()
	if err := c.initialContainerCollect(ctx); err != nil {
		return nil, fmt.Errorf("initial container collect: %w", err)
	}

	return c, nil
}

func (h *Handler) Create(ctx context.Context, meta *spec.Spec, lfmt *logfmt.Fmt, script taskscript.Script) error {
	if err := h.fetchImages(ctx, meta, lfmt); err != nil {
		return fmt.Errorf("fetching images: %w", err)
	}

	// install taskrunner to volume
	if err := h.createTaskrunnerVolume(ctx); err != nil {
		return fmt.Errorf("creating taskrunner volume: %w", err)
	}

	if err := h.createWorkContainer(ctx, meta); err != nil {
		return err
	}

	creds := meta.JobCredentials()
	err := h.store.Add(creds, h.createWorkload(creds))
	if err != nil {
		return fmt.Errorf("adding item to clientstore: %w", err)
	}

	return h.store.Get(creds).Client().Run(ctx, script)
}

func (h *Handler) Delete(ctx context.Context, credentials ci.Credentials) error {
	name := fmt.Sprintf("jobd-%d", credentials.ID)

	if err := h.removeContainers(ctx, name); err != nil {
		return fmt.Errorf("removing containers: %w", err)
	}

	if err := h.removeVolumes(ctx, name); err != nil {
		return fmt.Errorf("removing volumes: %w", err)
	}

	return nil
}

func (h *Handler) createWorkload(creds ci.Credentials) func() (*Workload, error) {
	return func() (*Workload, error) {
		name := fmt.Sprintf("jobd-%d", creds.ID)

		agentClient, err := h.getTaskrunnerClient(creds, name)
		if err != nil {
			return nil, err
		}

		return &Workload{
			handler: h,
			client:  agentClient,
		}, nil
	}
}

func (h *Handler) getTaskrunnerClient(credentials ci.Credentials, name string) (agent.Client, error) {
	return agent.New(credentials, func(stdin io.Reader, stdout io.Writer) (func(context.Context) error, error) {
		return func(ctx context.Context) error {
			return h.attachContainer(ctx, name, stdin, stdout)
		}, nil
	})
}

package handler

import (
	"context"
	"fmt"
	"io"
	"os"
	"strconv"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/pkg/stdcopy"
	"golang.org/x/sync/errgroup"

	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

func (h *Handler) createWorkContainer(ctx context.Context, meta *spec.Spec) (err error) {
	credentials := meta.JobCredentials()
	name := fmt.Sprintf("jobd-%d", credentials.ID)

	networkName, err := h.networkPool.Get(ctx)
	if err != nil {
		return fmt.Errorf("creating work network: %w", err)
	}

	services := getServices(meta)

	containerConfig := &container.Config{
		Image: spec.Expand(meta.Variables, meta.Image.Name),
		Cmd:   []string{"/taskrunner/usr/bin/taskrunner", "serve"},
		Labels: map[string]string{
			jobdParentLabel:  name,
			jobdJobLabel:     strconv.FormatInt(credentials.ID, 10),
			jobdProjectLabel: strconv.FormatInt(meta.JobInfo.ProjectID, 10),
			jobdTokenLabel:   credentials.Token, // todo: encrypt
			jobdBaseURLLabel: credentials.BaseURL,
		},
	}

	hostConfig := &container.HostConfig{
		Privileged:  h.cfg.Privileged,
		NetworkMode: container.NetworkMode(networkName),
		ExtraHosts:  services.Hosts,
		Mounts: []mount.Mount{
			{
				Type:     mount.TypeVolume,
				Source:   "taskrunner",
				Target:   "/taskrunner",
				ReadOnly: true,
			},
			{
				Type:   mount.TypeVolume,
				Source: name,
				Target: "/work",
				VolumeOptions: &mount.VolumeOptions{
					Labels: map[string]string{
						jobdParentLabel: name,
					},
				},
			},
		},
	}

	for _, svc := range services.Services {
		hostConfig.Mounts = append(hostConfig.Mounts, mount.Mount{
			Type:   mount.TypeVolume,
			Source: svc.ContainerName,
			Target: svc.Volume,
			VolumeOptions: &mount.VolumeOptions{
				Labels: map[string]string{
					jobdParentLabel: name,
				},
			},
		})
	}

	netConfig := &network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			networkName: {},
		},
	}

	uct, err := h.client.ContainerCreate(ctx, containerConfig, hostConfig, netConfig, nil, name)
	if err != nil {
		return fmt.Errorf("creating work container: %w", err)
	}
	defer func() {
		if err == nil {
			return
		}

		if err := h.Delete(ctx, credentials); err != nil {
			h.logger.Error("deleting failed container", "name", name, "err", err)
		}
	}()

	if err := h.client.ContainerStart(ctx, uct.ID, types.ContainerStartOptions{}); err != nil {
		return fmt.Errorf("starting work container: %w", err)
	}

	// create service containers
	group, gctx := errgroup.WithContext(ctx)
	{
		for idx := range services.Services {
			idx := idx
			group.Go(func() error {
				return h.createServiceContainer(gctx, services.Services[idx])
			})
		}

		err := group.Wait()
		if err != nil {
			return err
		}
	}

	return nil
}

func (h *Handler) createServiceContainer(ctx context.Context, svc service) (err error) {
	containerConfig := &container.Config{
		Image: svc.Name,
		Labels: map[string]string{
			jobdParentLabel: svc.Parent,
		},
		Env: svc.Env,
	}

	hostConfig := &container.HostConfig{
		Privileged:  h.cfg.Privileged,
		NetworkMode: container.NetworkMode("container:" + svc.Parent),
	}

	hostConfig.Mounts = append(hostConfig.Mounts, mount.Mount{
		Type:   mount.TypeVolume,
		Source: svc.ContainerName,
		Target: svc.Volume,
		VolumeOptions: &mount.VolumeOptions{
			Labels: map[string]string{
				jobdParentLabel: svc.Parent,
			},
		},
	})

	uct, err := h.client.ContainerCreate(ctx, containerConfig, hostConfig, nil, nil, svc.ContainerName)
	if err != nil {
		return fmt.Errorf("creating service container: %w", err)
	}
	defer func() {
		if err == nil {
			return
		}

		if err := h.client.ContainerRemove(ctx, uct.ID, types.ContainerRemoveOptions{
			RemoveVolumes: true,
			Force:         true,
		}); err != nil {
			h.logger.Error("removing service container", "service", uct.ID, "name", svc.ContainerName, "err", err)
		}
	}()

	if err := h.client.ContainerStart(ctx, uct.ID, types.ContainerStartOptions{}); err != nil {
		return fmt.Errorf("starting service container: %w", err)
	}

	return nil
}

func (h *Handler) createTaskrunnerVolume(ctx context.Context) error {
	containerConfig := &container.Config{
		Image: taskrunnerImage,
		Cmd:   []string{"/usr/bin/taskrunner", "install"},
	}
	hostConfig := &container.HostConfig{
		AutoRemove:     true,
		ReadonlyRootfs: true,
		NetworkMode:    container.NetworkMode("none"),
		Mounts: []mount.Mount{
			{
				Type:   mount.TypeVolume,
				Source: "taskrunner",
				Target: "/taskrunner",
			},
		},
	}

	trc, err := h.client.ContainerCreate(ctx, containerConfig, hostConfig, nil, nil, "")
	if err != nil {
		return err
	}
	defer h.client.ContainerRemove(ctx, trc.ID, types.ContainerRemoveOptions{
		RemoveVolumes: true,
		Force:         true,
	})

	okCh, errCh := h.client.ContainerWait(ctx, trc.ID, container.WaitConditionNextExit)

	err = h.client.ContainerStart(ctx, trc.ID, types.ContainerStartOptions{})
	if err != nil {
		return fmt.Errorf("starting install container: %w", err)
	}

	select {
	case err := <-errCh:
		return err

	case ok := <-okCh:
		if ok.StatusCode != 0 {
			return fmt.Errorf("container not zero exit")
		}
	}

	return nil
}

func (h *Handler) attachContainer(ctx context.Context, name string, stdin io.Reader, stdout io.Writer) error {
	resp, err := h.client.ContainerExecCreate(ctx, name, types.ExecConfig{
		Cmd:          []string{"/taskrunner/usr/bin/taskrunner", "proxy"},
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
	})
	if err != nil {
		return err
	}

	hijacked, err := h.client.ContainerExecAttach(ctx, resp.ID, types.ExecStartCheck{})
	if err != nil {
		return err
	}
	defer hijacked.Close()

	h.logger.Info("connected to container", "id", name, "name", name)

	var group errgroup.Group

	group.Go(func() error {
		defer hijacked.CloseWrite()

		_, err := io.Copy(hijacked.Conn, stdin)
		if err != nil {
			return err
		}

		return hijacked.CloseWrite()
	})

	group.Go(func() error {
		defer hijacked.Close()

		_, err = stdcopy.StdCopy(stdout, os.Stderr, hijacked.Reader)
		if err != nil {
			return err
		}

		return nil
	})

	return group.Wait()
}

func (h *Handler) removeContainers(ctx context.Context, name string) error {
	filter := filters.NewArgs(
		filters.KeyValuePair{
			Key:   "label",
			Value: jobdParentLabel + "=" + name,
		},
	)

	containers, err := h.client.ContainerList(ctx, types.ContainerListOptions{
		All:     true,
		Filters: filter,
	})
	if err != nil {
		return fmt.Errorf("listing containers: %w", err)
	}

	group, gctx := errgroup.WithContext(ctx)
	for _, ctr := range containers {
		ctr := ctr

		// work container & user defined network
		if _, ok := ctr.Labels[jobdJobLabel]; ok {
			if container.NetworkMode(ctr.HostConfig.NetworkMode).IsUserDefined() {
				err = h.client.NetworkDisconnect(ctx, string(ctr.HostConfig.NetworkMode), ctr.ID, true)
				if err != nil {
					return fmt.Errorf("removing network: %w", err)
				}

				h.networkPool.Put(ctr.HostConfig.NetworkMode)
			}
		}

		group.Go(func() error {
			if err := h.client.ContainerRemove(gctx, ctr.ID, types.ContainerRemoveOptions{
				RemoveVolumes: true,
				Force:         true,
			}); err != nil {
				return fmt.Errorf("removing container: %w", err)
			}

			return nil
		})
	}

	return group.Wait()
}

func (h *Handler) removeVolumes(ctx context.Context, name string) error {
	filter := filters.NewArgs(
		filters.KeyValuePair{
			Key:   "label",
			Value: jobdParentLabel + "=" + name,
		},
	)

	volumes, err := h.client.VolumeList(ctx, filter)
	if err != nil {
		return fmt.Errorf("listing volumes: %w", err)
	}

	group, gctx := errgroup.WithContext(ctx)
	for _, vol := range volumes.Volumes {
		vol := vol

		if err := h.client.VolumeRemove(gctx, vol.Name, true); err != nil {
			return fmt.Errorf("removing volume: %w", err)
		}
	}

	return group.Wait()
}

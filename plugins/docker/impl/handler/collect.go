package handler

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"

	"gitlab.com/jobd/jobd/pkg/ci"
)

func (h *Handler) initialContainerCollect(ctx context.Context) error {
	filters := filters.NewArgs(
		filters.KeyValuePair{
			Key:   "label",
			Value: jobdJobLabel,
		},
	)

	list, err := h.client.ContainerList(ctx, types.ContainerListOptions{
		All:     true,
		Filters: filters,
	})
	if err != nil {
		return fmt.Errorf("fetching container list: %w", err)
	}

	for _, item := range list {
		h.addFoundContainer(item.Labels)
	}

	return nil
}

func (h *Handler) continuousContainerCollect() {
	filters := filters.NewArgs(
		filters.KeyValuePair{
			Key:   "type",
			Value: "container",
		},
		filters.KeyValuePair{
			Key:   "label",
			Value: jobdJobLabel,
		},
	)

	for {
		eventCh, errCh := h.client.Events(context.Background(), types.EventsOptions{
			Filters: filters,
		})

		select {
		case msg := <-eventCh:
			if !strings.HasPrefix(msg.Action, "exec_start:") {
				continue
			}

			h.addFoundContainer(msg.Actor.Attributes)

		case err := <-errCh:
			h.logger.Error("continuous container collect", "err", err)
			time.Sleep(10 * time.Second)
		}
	}
}

func (h *Handler) addFoundContainer(labels map[string]string) {
	if labels == nil {
		return
	}

	id, _ := strconv.ParseInt(labels[jobdJobLabel], 10, 64)
	creds := ci.Credentials{
		ID:      id,
		Token:   labels[jobdTokenLabel],
		BaseURL: labels[jobdBaseURLLabel],
	}

	err := h.store.Add(creds, h.createWorkload(creds))
	if err != nil {
		h.logger.Error("adding existing workload", "err", err)
	}
}

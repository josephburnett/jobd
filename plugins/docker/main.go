package main

import (
	"gitlab.com/jobd/jobd/pkg/executor/plugin"
	"gitlab.com/jobd/jobd/plugins/docker/impl"
)

func main() {
	plugin.Serve(&impl.Executor{}, nil)
}

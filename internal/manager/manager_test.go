package manager

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLoadConfig(t *testing.T) {
	dir := os.TempDir()
	cfgPath := filepath.Join(dir, "config.cue")

	tests := map[string]struct {
		content string
		invalid bool
	}{
		"valid":         {`executor: { type: "docker" }, url: "https://example.org", token: "abcdef", limit: 2`, false},
		"no type":       {`executor: {}, url: "https://example.org", token: "abcdef"`, true},
		"no token":      {`executor: { type: "docker" },, url: "https://example.org"`, true},
		"no url":        {`executor: { type: "docker" },, token: "abcdef"`, true},
		"invalid limit": {`executor: { type: "docker" },, url: "https://example.org", token: "abcdef", limit: 0`, true},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			require.NoError(t, os.WriteFile(cfgPath, []byte(tc.content), 0777))
			_, err := LoadConfig(cfgPath)

			if tc.invalid {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

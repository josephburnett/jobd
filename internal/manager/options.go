package manager

import (
	"github.com/hashicorp/go-hclog"
)

type options struct {
	Logger  hclog.Logger
	Limiter *limiter
}

// Option is an option used to configure the job client.
type Option func(*options) error

func newOptions(opts []Option) (options, error) {
	var options options

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.Logger == nil {
		options.Logger = hclog.Default()
	}

	return options, nil
}

func WithLogger(logger hclog.Logger) Option {
	return func(o *options) error {
		o.Logger = logger
		return nil
	}
}

func WithLimiter(limiter *limiter) Option {
	return func(o *options) error {
		o.Limiter = limiter
		return nil
	}
}

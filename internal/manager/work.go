package manager

import (
	"sync"

	"gitlab.com/jobd/jobd/pkg/ci/client/jobclient"
)

type inflight struct {
	m sync.Map
}

func (in *inflight) add(id int64, w *work) {
	in.m.Store(id, w)
}

func (in *inflight) get(id int64) *work {
	val, ok := in.m.Load(id)
	if !ok {
		return nil
	}

	return val.(*work)
}

func (in *inflight) delete(id int64) {
	in.m.Delete(id)
}

type work struct {
	jc      *jobclient.Client
	mu      sync.Mutex
	handled bool
}

func (w *work) Client() *jobclient.Client {
	return w.jc
}

func (w *work) Handled() bool {
	w.mu.Lock()
	defer w.mu.Unlock()

	handled := w.handled
	w.handled = true

	return handled
}

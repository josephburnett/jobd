package manager

import (
	"context"
	"errors"
	"fmt"
	"time"

	"golang.org/x/sync/errgroup"

	"gitlab.com/jobd/jobd/internal/taskrunner/taskscript/builder"
	"gitlab.com/jobd/jobd/pkg/ci"
	"gitlab.com/jobd/jobd/pkg/ci/client"
	"gitlab.com/jobd/jobd/pkg/ci/client/jobclient"
	"gitlab.com/jobd/jobd/pkg/ci/logfmt"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/executor"
)

type Manager struct {
	ep     ExecutorPlugin
	config Config
	client *client.Client
	opts   options
	cursor uint64

	in inflight
}

type ExecutorPlugin interface {
	Executor() executor.Interface
	Kill()
}

func New(ctx context.Context, cfg Config, ep ExecutorPlugin, options ...Option) (*Manager, error) {
	opts, err := newOptions(options)
	if err != nil {
		return nil, fmt.Errorf("configuring options: %w", err)
	}

	// todo: executor plugin interface needs to be updated to accept features
	gc, err := client.New(cfg.Token, cfg.URL, client.WithVersionInfoUpdater(func(info *client.VersionInfo) {
		info.Features.Variables = true
		info.Features.Artifacts = true
		info.Features.Cache = true
		info.Features.UploadMultipleArtifacts = true
		info.Features.UploadRawArtifacts = true
		info.Features.Refspecs = true
		info.Features.RawVariables = true
		info.Features.MultiBuildSteps = true
		info.Features.ReturnExitCode = true
		info.Features.ServiceVariables = true
		info.Features.Image = true
		info.Features.Services = true
		info.Features.Shared = true
	}))
	if err != nil {
		return nil, fmt.Errorf("creating client: %w", err)
	}

	if err := ep.Executor().Init(ctx, opts.Logger, cfg.RawConfig); err != nil {
		return nil, fmt.Errorf("initializing executor: %w", err)
	}

	// constrain limiter by config setting
	opts.Limiter = Limiter(cfg.Limit, opts.Limiter)

	return &Manager{
		ep:     ep,
		config: cfg,
		client: gc,
		opts:   opts,
	}, nil
}

func (m *Manager) Start(ctx context.Context) error {
	// discovery function checks to see if work is already inflight and if not,
	// acquires a lock to ensure there's capacity for it to run.
	//
	// this is used for both the initial discovery (work we're not aware of, but
	// our executor is) and continuous discovery (work we recently created and
	// we've got the go-ahead to attach to).
	discoverFn := func(credentials ci.Credentials, cursor uint64) error {
		m.opts.Logger.Debug("discovered", "id", credentials.ID)

		w := m.in.get(credentials.ID)
		if w == nil {
			if err := m.opts.Limiter.Acquire(ctx); err != nil {
				return fmt.Errorf("discovery acquire: %w", err)
			}

			jc, err := jobclient.New(credentials, m.client.VersionInfo())
			if err != nil {
				return fmt.Errorf("creating jobclient (%d): %w", credentials.ID, err)
			}

			w = &work{jc: jc}
			m.in.add(credentials.ID, w)
		}

		// advance cursor to keep track of items we've already discovered
		m.cursor = cursor

		// handle work
		go m.handle(ctx, credentials, w)

		return nil
	}

	// initial discovery until synced: this ensures that we're not accepting new
	// work until we're finished with what can be potentially resumed.
	err := m.ep.Executor().Discover(ctx, m.cursor, false, discoverFn)
	if err != nil {
		return fmt.Errorf("discover (initial): %w", err)
	}

	wg, ctx := errgroup.WithContext(ctx)

	// continuous discovery: new work we just created should appear here, and
	// should already be in our inflight store.
	wg.Go(func() error {
		for {
			err := m.ep.Executor().Discover(ctx, m.cursor, true, discoverFn)
			if err != nil {
				return fmt.Errorf("discover (follow): %w", err)
			}

			m.opts.Logger.Warn("discovery terminated unexpectedly terminated")
			time.Sleep(time.Second)
		}
	})

	// start polling for jobs
	wg.Go(func() error {
		return m.poll(ctx)
	})

	return wg.Wait()
}

func (m *Manager) poll(ctx context.Context) error {
	for {
		if err := m.opts.Limiter.Acquire(ctx); err != nil {
			return fmt.Errorf("poll acquire: %w", err)
		}

		var meta spec.Spec
		found, err := m.client.RequestJob(ctx, &meta)
		if err != nil {
			return fmt.Errorf("requesting work: %w", ctx.Err())
		}

		if found {
			jc, err := jobclient.New(meta.JobCredentials(), m.client.VersionInfo())
			if err != nil {
				return fmt.Errorf("creating jobclient (%d): %w", meta.ID, err)
			}

			m.in.add(meta.ID, &work{jc: jc})

			go m.dispatch(ctx, meta, jc)
		} else {
			m.opts.Limiter.Release()
		}
	}
}

func (m *Manager) dispatch(ctx context.Context, meta spec.Spec, jc *jobclient.Client) {
	logger := m.opts.Logger.With("id", meta.ID)
	logger.Info("picked up work")

	lfmt := logfmt.New(jc, logfmt.Config{
		StreamID:         logfmt.StreamExecutorLevel,
		InsertTimestamps: true,
	})
	defer lfmt.Close()

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	now := time.Now()
	script, err := builder.Build(meta, builder.WithShell(m.config.Shell))
	if err != nil {
		logger.Error("building taskscript", "err", err)
	}

	logfmt.Notice(lfmt.Stdout(), "taskscript built in", time.Since(now))

	now = time.Now()
	if err == nil {
		err = m.ep.Executor().Create(ctx, meta, lfmt.Writer(), script)

		if err != nil {
			logger.Error("creating work with executor", "err", err)
		}
	}
	logfmt.Notice(lfmt.Stdout(), "created environment in", time.Since(now))

	if err == nil {
		return
	}

	defer m.in.delete(meta.ID)

	jc.Fail(1, jobclient.ErrRunnerSystemFailure)

	if err := jc.Close(); err != nil {
		logger.Error("jobclient close", "err", err)
	}
}

func (m *Manager) handle(ctx context.Context, credentials ci.Credentials, w *work) {
	if w.Handled() {
		m.opts.Logger.Debug("work already handled", "id", credentials.ID)
		return
	}

	lfmt := logfmt.New(w.jc, logfmt.Config{
		StreamID:         logfmt.StreamExecutorLevel,
		InsertTimestamps: true,
	})
	defer lfmt.Close()

	defer m.opts.Limiter.Release()
	defer m.in.delete(credentials.ID)

	logger := m.opts.Logger.With("id", credentials.ID)
	logger.Info("starting work")

	logfmt.Notice(lfmt.Stdout(), "starting work...")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	client := w.Client()
	defer client.Close()

	go func() {
		defer client.Close()

		for {
			rs, err := client.Wait()

			switch rs {
			case jobclient.RemoteStateCanceling:
				// todo: graceful termination

			case jobclient.RemoteStateCanceled:
				// todo: termination
			}

			if errors.Is(err, jobclient.ErrClientIsClosed) {
				return
			}

			// the jobclient automatically retries requests until successful,
			// if an error is returned, we make the tough decision to delete
			// the workload.
			//
			// todo: do we want to delete the workload? Might it be better we
			// leave it and ensure that it's resumable?
			if err != nil {
				logger.Error("jobclient wait", "err", err)
				if err := m.ep.Executor().Delete(ctx, credentials); err != nil {
					logger.Error("deleting work", "err", err)
				}

				return
			}
		}
	}()

	result, err := m.ep.Executor().Attach(ctx, credentials, lfmt.Writer(), 0)

	// todo: revisit how we handle errors after an attach fail
	// we might not want to immediately fail but retry?
	switch {
	case err != nil:
		logger.Error("failed work (attach)", "err", err)

		client.Fail(1, jobclient.ErrRunnerSystemFailure)

	case result.Finished:
		logger.Info("finished work", "exit-code", result.ExitCode)

		if result.ExitCode > 0 {
			client.Fail(result.ExitCode, jobclient.ErrScriptFailure)
		}
	}

	if err := client.Close(); err != nil {
		logger.Error("jobclient close", "err", err)
	}

	if err := m.ep.Executor().Delete(ctx, credentials); err != nil {
		logger.Error("deleting work", "err", err)
	}
}

func (m *Manager) Close() error {
	m.ep.Kill()

	return nil
}

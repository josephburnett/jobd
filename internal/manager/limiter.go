package manager

import (
	"context"

	"golang.org/x/sync/semaphore"
)

type limiter struct {
	n      int
	sem    *semaphore.Weighted
	parent *limiter
}

// Limiter returns a work limiter that can have a shared parent amongst
// additionaly limiters.
//
// For example:
//   parent := Limiter(2, nil)
//   child1 := Limiter(2, parent)
//   child2 := Limiter(2, parent)
//
// In this example, the parent restricts acquiring locks more than twice.
// Without this parent restriction, the children would be able to acquire 2
// locks each all of the time.
func Limiter(n int, parent *limiter) *limiter {
	l := &limiter{n: n, parent: parent}

	if n > 0 {
		l.sem = semaphore.NewWeighted(int64(n))
	}

	return l
}

// Acquire acquires a lock to perform work. Once the work is complete, Release
// should be called.
func (l *limiter) Acquire(ctx context.Context) (err error) {
	if l.parent != nil {
		if err := l.parent.Acquire(ctx); err != nil {
			return err
		}

		// if we encounter an error acquiring
		defer func() {
			if err != nil {
				l.parent.Release()
			}
		}()
	}

	if l.sem == nil {
		return nil
	}

	return l.sem.Acquire(ctx, 1)
}

// Releases an acquisition.
func (l *limiter) Release() {
	if l.parent != nil {
		defer l.parent.Release()
	}

	if l.sem == nil {
		return
	}

	l.sem.Release(1)
}

// Wait blocks until either ctx is done or all acquisitions have been released
// for this limiter. The parent is NOT always waited on.
func (l *limiter) Wait(ctx context.Context) error {
	if l.sem == nil {
		return nil
	}

	if err := l.sem.Acquire(ctx, int64(l.n)); err != nil {
		return err
	}
	l.sem.Release(int64(l.n))

	return nil
}

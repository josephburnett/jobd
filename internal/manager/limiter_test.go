package manager

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestLimiterByParent(t *testing.T) {
	parent := Limiter(2, nil)
	child1 := Limiter(2, parent)
	child2 := Limiter(2, parent)

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	require.NoError(t, child1.Acquire(ctx))
	require.NoError(t, child2.Acquire(ctx))
	require.Error(t, child1.Acquire(ctx))

	child1.Release()
	require.NoError(t, child1.Wait(ctx))

	child2.Release()
	require.NoError(t, child2.Wait(ctx))

	require.NoError(t, parent.Wait(ctx))
}

func TestLimiterByChild(t *testing.T) {
	parent := Limiter(3, nil)
	child1 := Limiter(2, parent)

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	require.NoError(t, child1.Acquire(ctx))
	require.NoError(t, child1.Acquire(ctx))
	require.Error(t, child1.Acquire(ctx))
	require.NoError(t, parent.Acquire(ctx))

	child1.Release()
	child1.Release()
	require.NoError(t, child1.Wait(ctx))

	parent.Release()
	require.NoError(t, parent.Wait(ctx))
}

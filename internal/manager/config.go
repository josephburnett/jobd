package manager

import (
	"fmt"

	"cuelang.org/go/cue"
	"cuelang.org/go/cue/cuecontext"
	"cuelang.org/go/cue/load"
)

type Config struct {
	URL   string
	Token string
	Limit int
	Shell string

	Executor struct {
		Type string
	}
	RawConfig []byte
}

const configConstraint = `
close({
	url: string
	token: string
	limit: *1 | int
	limit: >= 1

	shell: *"sh" | string

	executor: {
		type: string
	}
})
`

func LoadConfig(configpath string) (Config, error) {
	cfg := Config{}

	instances := load.Instances([]string{configpath}, nil)
	if len(instances) != 1 {
		return cfg, fmt.Errorf("expected one cue config instance")
	}

	cctx := cuecontext.New()
	cval := cctx.BuildInstance(instances[0])
	cval = cctx.CompileString(configConstraint).Unify(cval)

	if err := cval.Validate(cue.Concrete(true), cue.Final()); err != nil {
		return cfg, fmt.Errorf("validating config: %w", err)
	}

	err := cval.Decode(&cfg)
	if err != nil {
		return cfg, fmt.Errorf("decoding config: %w", err)
	}

	cfg.RawConfig, err = cval.MarshalJSON()
	if err != nil {
		return cfg, fmt.Errorf("marshaling config: %w", err)
	}

	return cfg, err
}

package variables

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

func TestVariableProvider(t *testing.T) {
	spec := spec.Spec{
		Variables: []spec.Variable{
			{Key: "FOO", Value: "BAR"},
			{Key: "MASKED", Value: "$FOO secret", Masked: true},
			{Key: "FILE", Value: "data", File: true},
		},
	}

	expectedEnv := map[string]string{
		"CI_SERVER": "yes",
		"FOO":       "BAR",
		"MASKED":    "BAR secret",
	}

	expectedFiles := map[string]string{
		"FILE": "data",
	}

	expectedMasked := []string{"BAR secret"}

	vp := NewVariableProvider(spec)

	assert.Equal(t, expectedEnv, vp.Env())
	assert.Equal(t, expectedFiles, vp.Files())
	assert.Equal(t, expectedMasked, vp.Masked())
	assert.Equal(t, "BAR", vp.Expand("$FOO"))
	assert.Equal(t, "$FOO", vp.Expand("$$FOO"))
	assert.Equal(t, "", vp.Expand("$9"))
}

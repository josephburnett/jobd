package variables

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type variabler map[string]string

func (v variabler) Variable(key string) string {
	return v[key]
}

func TestVariables(t *testing.T) {
	v := variabler{
		"foobar": "barfoo",
		"false":  "false",
		"FALSE":  "FALSE",
		"F":      "F",
		"0":      "0",
		"true":   "true",
		"TRUE":   "TRUE",
		"T":      "T",
		"1":      "1",
	}

	assert.Equal(t, "barfoo", Default(v, "foobar", "nono"))
	assert.Equal(t, "nothing", Default(v, "non-existant", "nothing"))

	assert.Equal(t, true, DefaultBool(v, "foobar", true))
	assert.Equal(t, false, DefaultBool(v, "foobar", false))

	assert.Equal(t, true, DefaultBool(v, "non-existant", true))
	assert.Equal(t, false, DefaultBool(v, "non-existant", false))

	assert.Equal(t, false, DefaultBool(v, "false", true))
	assert.Equal(t, false, DefaultBool(v, "false", false))
	assert.Equal(t, false, DefaultBool(v, "FALSE", true))
	assert.Equal(t, false, DefaultBool(v, "FALSE", false))
	assert.Equal(t, false, DefaultBool(v, "F", true))
	assert.Equal(t, false, DefaultBool(v, "F", false))
	assert.Equal(t, false, DefaultBool(v, "0", true))
	assert.Equal(t, false, DefaultBool(v, "0", false))

	assert.Equal(t, true, DefaultBool(v, "true", true))
	assert.Equal(t, true, DefaultBool(v, "true", false))
	assert.Equal(t, true, DefaultBool(v, "TRUE", true))
	assert.Equal(t, true, DefaultBool(v, "TRUE", false))
	assert.Equal(t, true, DefaultBool(v, "T", true))
	assert.Equal(t, true, DefaultBool(v, "T", false))
	assert.Equal(t, true, DefaultBool(v, "1", true))
	assert.Equal(t, true, DefaultBool(v, "1", false))

	assert.Equal(t, 1, DefaultIntClamp(v, "foobar", 1, 0, 10))
	assert.Equal(t, 1, DefaultIntClamp(v, "non-existant", 1, 0, 10))
	assert.Equal(t, 1, DefaultIntClamp(v, "non-existant", 1, 0, 10))
	assert.Equal(t, 1, DefaultIntClamp(v, "1", 1, 0, 10))
	assert.Equal(t, 5, DefaultIntClamp(v, "1", 1, 5, 10))
	assert.Equal(t, 0, DefaultIntClamp(v, "1", 1, 0, 0))
	assert.Equal(t, 5, DefaultIntClamp(v, "0", 1, 5, 10))
	assert.Equal(t, 5, DefaultIntClamp(v, "0", 1, 5, 10))
	assert.Equal(t, 5, DefaultIntClamp(v, "0", 1, 5, 10))
}

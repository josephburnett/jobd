package variables

import (
	"strconv"
)

type Variabler interface {
	Variable(string) string
}

// Default returns a job variable value by key if it is defined or
// the caller supplied default value if not.
func Default(v Variabler, key, defaultValue string) string {
	if v.Variable(key) == "" {
		return defaultValue
	}

	return v.Variable(key)
}

// DefaultBool returns a bool value parsed from a variable by it's
// key. If the key doesn't exist, the default value provided is returned.
func DefaultBool(v Variabler, key string, defaultValue bool) bool {
	if v.Variable(key) == "" {
		return defaultValue
	}

	val, err := strconv.ParseBool(v.Variable(key))
	if err != nil {
		return defaultValue
	}
	return val
}

// DefaultIntClamp clamps and returns an int value parsed from a
// variable by it's key. If the key doesn't exist, the default value is clamped
// and returned.
func DefaultIntClamp(v Variabler, key string, defaultValue, min, max int) int {
	var val int
	if v.Variable(key) == "" {
		val = defaultValue
	} else {
		var err error
		val, err = strconv.Atoi(v.Variable(key))
		if err != nil {
			return defaultValue
		}
	}

	if val < min {
		return min
	}
	if val > max {
		return max
	}
	return val
}

package variables

import (
	"os"

	"gitlab.com/jobd/jobd/pkg/ci/spec"
)

type variableProvider struct {
	indexed map[string]spec.Variable // index of keys to the associated variable

	env    map[string]string
	files  map[string]string
	masked []string
}

func NewVariableProvider(meta spec.Spec) *variableProvider {
	internalVariables := []spec.Variable{
		{Key: "CI_SERVER", Value: "yes"},
	}

	b := &variableProvider{
		indexed: make(map[string]spec.Variable),
		env:     make(map[string]string),
		files:   make(map[string]string),
	}

	process := func(variables []spec.Variable) {
		for _, variable := range variables {
			if !variable.Raw {
				variable.Value = os.Expand(variable.Value, b.mapping)
			}
			if variable.Masked {
				b.masked = append(b.masked, variable.Value)
			}

			b.indexed[variable.Key] = variable
			switch {
			case variable.File:
				b.files[variable.Key] = variable.Value
			default:
				b.env[variable.Key] = variable.Value
			}
		}
	}

	process(internalVariables)
	process(meta.Variables)

	return b
}

func (b *variableProvider) mapping(key string) string {
	switch key {
	case "$":
		return key
	case "*", "#", "@", "!", "?", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
		return ""
	}
	return b.indexed[key].Value
}

func (b *variableProvider) Variable(key string) string {
	return b.indexed[key].Value
}

func (b *variableProvider) Expand(value string) string {
	return os.Expand(value, b.mapping)
}

func (b *variableProvider) Env() map[string]string {
	return b.env
}

func (b *variableProvider) Files() map[string]string {
	return b.files
}

func (b *variableProvider) Masked() []string {
	return b.masked
}

package builder

import (
	"errors"
	"fmt"
	"path"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jobd/jobd/internal/taskrunner/taskscript/builder/internal/variables"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/taskrunner/taskscript"
	v1 "gitlab.com/jobd/jobd/pkg/taskrunner/taskscript/tasks/v1"
)

var (
	ErrCachePathTraversal = errors.New("cache path traversal")
)

func Build(meta spec.Spec, options ...Option) (taskscript.Script, error) {
	opts, err := newOptions(meta, options)
	if err != nil {
		return nil, err
	}

	b := builder{
		opts: opts,
		meta: meta,
	}

	creds := meta.JobCredentials()

	program := taskscript.Program{
		WorkingDir: opts.workingDir,
		CacheDir:   opts.cacheDir,
		Timeout:    time.Duration(b.meta.RunnerInfo.Timeout) * time.Second,
		ID:         creds.GetID(),
		Token:      creds.GetToken(),
		BaseURL:    creds.GetBaseURL(),
		Env:        opts.variables.Env(),
		Files:      opts.variables.Files(),
	}

	// get sources, cache extract, artifact extract
	program.Tasks = append(program.Tasks, b.prepare()...)

	// steps, after script
	program.Tasks = append(program.Tasks, b.steps()...)

	// cache archive, artifact archive
	program.Tasks = append(program.Tasks, b.finalize()...)

	return program.Marshal()
}

type builder struct {
	opts options
	meta spec.Spec
}

func (b *builder) cacheKey(name string) (string, error) {
	key := path.Join(b.meta.JobInfo.Name, b.meta.GitInfo.Ref)
	if name != "" {
		key = b.opts.variables.Expand(name)
	}

	base := path.Join("projects", b.opts.cacheSharedKey, strconv.FormatInt(b.meta.JobInfo.ProjectID, 10))
	key = path.Join(base, key)
	if !strings.HasPrefix(key, base+"/") {
		return "", ErrCachePathTraversal
	}

	return key, nil
}

func (b *builder) prepare() []taskscript.Item {
	var items []taskscript.Item

	// Setup environment basics
	items = append(items, taskscript.Item{
		When:      taskscript.WhenAlways,
		OnFailure: taskscript.OnFailureReturn,
		Task:      v1.SetupTask{},
	})

	// Fetching sources always executes, but execution stops entirely on failure.
	items = append(items, taskscript.Item{
		When:      taskscript.WhenAlways,
		OnFailure: taskscript.OnFailureReturn,
		Task: v1.FetchSourcesTask{
			AllowGitFetch: b.meta.AllowGitFetch,
			Checkout:      variables.DefaultBool(b.opts.variables, "GIT_CHECKOUT", true),
			MaxAttempts:   variables.DefaultIntClamp(b.opts.variables, "GET_SOURCES_ATTEMPTS", 5, 1, 10),
			Submodules:    variables.Default(b.opts.variables, "GIT_SUBMODULE_STRATEGY", "none"),
			LFSDisabled:   variables.DefaultBool(b.opts.variables, "GIT_LFS_SKIP_SMUDGE", false),
			Depth:         b.meta.GitInfo.Depth,
			RepoURL:       b.meta.GitInfo.RepoURL,
			Refspecs:      b.meta.GitInfo.Refspecs,
			SHA:           b.meta.GitInfo.Sha,
		},
	})

	// Cache extraction always executes, but on failure is skipped and execution continues
	// (caches are treated as nice to haves, not must haves).
	for _, cache := range b.meta.Cache {
		if cache.Policy == spec.CachePolicyPush {
			continue
		}
		if len(cache.Paths) == 0 && !cache.Untracked {
			continue
		}
		if b.opts.shellProvider == nil {
			items = append(items, taskscript.Item{
				When: taskscript.WhenAlways,
				Task: v1.InfoTask{
					Message: fmt.Sprintf("caching is disabled, skipping: %q\n", cache.Key),
				},
			})

			continue
		}

		key, err := b.cacheKey(cache.Key)
		if err != nil {
			// TODO: add error to logger

			items = append(items, taskscript.Item{
				When: taskscript.WhenAlways,
				Task: v1.InfoTask{
					Message: fmt.Sprintf("incorrect caching configuration, skipping: %q\n", cache.Key),
				},
			})

			continue
		}

		items = append(items, taskscript.Item{
			When:      taskscript.WhenAlways,
			OnFailure: taskscript.OnFailureContinue,
			Task: v1.CacheExtractTask{
				Key:            key,
				ArchiverFormat: b.opts.cacheArchiveFormat,
			},
		})
	}

	// Dependency extraction always executes, but execution stops entirely on failure.
	for _, dep := range b.meta.Dependencies {
		if len(dep.ArtifactsFile.Filename) == 0 {
			continue
		}

		items = append(items, taskscript.Item{
			When:      taskscript.WhenAlways,
			OnFailure: taskscript.OnFailureReturn,
			Task: v1.ArtifactsExtractTask{
				ID:           dep.ID,
				Token:        dep.Token,
				ArtifactName: dep.Name,
				Filename:     dep.ArtifactsFile.Filename,
			},
		})
	}

	return items
}

func (b *builder) steps() []taskscript.Item {
	var items []taskscript.Item
	var after []taskscript.Item

	// Step execution is configurable, based on whether the previous task/step succeeded.
	for _, step := range b.meta.Steps {
		// TODO: Implement suppport for GitLab to support specifying a shell on each step.
		s := b.opts.shellProvider(b.opts.shell)

		onFailure := taskscript.OnFailureBreak
		if step.AllowFailure {
			onFailure = taskscript.OnFailureContinue
		}

		item := taskscript.Item{
			When:      taskscript.When(step.When),
			OnFailure: onFailure,
			Task: v1.StepTask{
				Step:          string(step.Name),
				Script:        step.Script,
				CommandArgs:   s.Args(),
				CommandScript: s.Generate(step.Script),
				Extension:     s.Extension(),
			},
		}

		if step.Name == spec.StepNameAfterScript {
			after = append(after, item)
		} else {
			items = append(items, item)
		}
	}

	return append(items, after...)
}

func (b *builder) finalize() []taskscript.Item {
	var items []taskscript.Item

	// Cache archiving execution is configurable, but failures are effectively ignored.
	for _, cache := range b.meta.Cache {
		if cache.Policy == spec.CachePolicyUndefined || cache.Policy == spec.CachePolicyPull {
			continue
		}
		if len(cache.Paths) == 0 && !cache.Untracked {
			continue
		}

		key, err := b.cacheKey(cache.Key)
		if err != nil {
			// TODO: add error to logger

			items = append(items, taskscript.Item{
				When: taskscript.WhenAlways,
				Task: v1.InfoTask{
					Message: fmt.Sprintf("incorrect caching configuration, skipping: %q\n", cache.Key),
				},
			})

			continue
		}

		for idx := range cache.Paths {
			cache.Paths[idx] = b.opts.variables.Expand(cache.Paths[idx])
		}

		items = append(items, taskscript.Item{
			When:      taskscript.When(cache.When),
			OnFailure: taskscript.OnFailureContinue,
			Task: v1.CacheArchiveTask{
				Key:            key,
				Untracked:      cache.Untracked,
				Paths:          cache.Paths,
				ArchiverFormat: b.opts.cacheArchiveFormat,
			},
		})
	}

	// Artifact archiving is configurable, but execution stops entirely on failure.
	for _, artifact := range b.meta.Artifacts {
		if len(artifact.Paths) == 0 && !artifact.Untracked {
			continue
		}

		for idx := range artifact.Paths {
			artifact.Paths[idx] = b.opts.variables.Expand(artifact.Paths[idx])
		}
		for idx := range artifact.Exclude {
			artifact.Exclude[idx] = b.opts.variables.Expand(artifact.Exclude[idx])
		}

		items = append(items, taskscript.Item{
			When:      taskscript.When(artifact.When),
			OnFailure: taskscript.OnFailureReturn,
			Task: v1.ArtifactsArchiveTask{
				Untracked:    artifact.Untracked,
				Paths:        artifact.Paths,
				Exclude:      artifact.Exclude,
				ArtifactName: artifact.Name,
				ExpireIn:     artifact.ExpireIn,
				Format:       string(artifact.Format),
				Type:         artifact.Type,
			},
		})
	}

	return items
}

package builder

import (
	"gitlab.com/jobd/jobd/internal/taskrunner/taskscript/builder/internal/variables"
	"gitlab.com/jobd/jobd/pkg/ci/spec"
	"gitlab.com/jobd/jobd/pkg/taskrunner/shell"
)

type options struct {
	workingDir         string
	cacheDir           string
	variables          VariableProvider
	shellProvider      ShellProvider
	shell              string
	cacheArchiveFormat string
	cacheSharedKey     string
}

type Option func(*options) error

type VariableProvider interface {
	Variable(string) string
	Expand(string) string
	Env() map[string]string
	Files() map[string]string
}

type ShellProvider func(string) shell.Shell

func newOptions(meta spec.Spec, opts []Option) (options, error) {
	var options options

	options.cacheArchiveFormat = "zip"
	options.workingDir = "work"
	options.cacheDir = "cache"
	options.shell = "sh"

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.variables == nil {
		options.variables = variables.NewVariableProvider(meta)
	}

	if options.shellProvider == nil {
		options.shellProvider = shell.NewShellProvider()
	}

	return options, nil
}

func WithWorkingDir(dir string) Option {
	return func(o *options) error {
		o.workingDir = dir
		return nil
	}
}

func WithCacheDir(dir string) Option {
	return func(o *options) error {
		o.cacheDir = dir
		return nil
	}
}

func WithVariableProvider(provider VariableProvider) Option {
	return func(o *options) error {
		o.variables = provider
		return nil
	}
}

func WithShellProvider(provider ShellProvider) Option {
	return func(o *options) error {
		o.shellProvider = provider
		return nil
	}
}

func WithShell(shell string) Option {
	return func(o *options) error {
		o.shell = shell
		return nil
	}
}

func WithCacheSharedKey(key string) Option {
	return func(o *options) error {
		o.cacheSharedKey = key
		return nil
	}
}
